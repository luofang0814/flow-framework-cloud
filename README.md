# flow-framework-cloud

# flow-platform-support
该项目为基于spring-cloud-alibaba的脚手架，提供了诸多可实际提高研发效率的特性，如：在线API文档规范化、链路穿透、模块拆分后的相互调用支持、分布式分片调度、支持多个微服务业务数据的状态或类型统一等，详情请参考相应模块。注意，在实际业务部署时，需要在nginx的conf文件中透传域名和客户端ip，即`proxy_set_header Host $host`和`proxy_set_header X-Forwarded-For $remote_addr`;同时，需要删除系统自定义的header或将其值设置为空字符串，即：`proxy_set_header customization_trace_id ''`、`proxy_set_header customization_previous_application ''`以及`proxy_set_header customization_system_version ''`。

## flow-framework-api-doc
提供api文档支持，当前以swagger为基础进行适配，扩展后特性如下：  
1、只有使用了@Api注解的controller才会被解析到接口文档中  
2、controller中的响应不需要使用com.flow.framework.core.response.Response包装，接口文档和脚手架会自动包装，如果响应方法定义为void或者需要自定响应，则需要实现com.flow.framework.web.service.web.IResponseBodyAdviceService  
3、自动解析实现了com.flow.framework.common.enumeration.CodeEnum的枚举类，将相关的code值和其对应的含义说明添加到接口文档描述中  
4、自动解析请求入参的属性上添加了com.flow.framework.core.validation.annotation.ImmutableCustomization注解的属性，将相关的值和备注添加到接口文档描述中

## flow-framework-base
提供基础服务实现，相关特性如下：  
1、提供运维上下线微服务支持，包括提供REST接口  
2、com.flow.framework.core.response.Response中code国际化辅助支持，但未实现具体业务逻辑，详细业务逻辑需实现flow-framework-facade-i18n模块  
3、网络状态监控功能，包括指定网络和基于服务发现等场景  
4、运营告警服务辅助，但未实现具体业务逻辑，详细业务逻辑需实现flow-framework-facade-alarm模块  
5、操作系统监控检查服务实现，包括CPU、内存以及存储空间等  

## flow-framework-cache
提供缓存服务支持，包括redis的拓扑刷新、通用缓存接口封装和实现、缓存服务辅助类、pojo缓存管理以及缓存健康检查等

## flow-framework-cipher
提供通用算法服务，目前支持Bcrypt、RSA以及SM4等

## flow-framework-common
提供通用封装，如：时间相关功能、异常相关功能、响应封装、json工具以及各种工具类

## flow-framework-config
提供项目启动时获取配置中心数据的相关功能，主要用于研发本地启动时只需要配置统一配置文件便可启动

## flow-framework-core
提供总众多核心功能，如：系统初始化、系统配置获取、ID序列封、国际化上下文、安全上下文、系统版本上下文、线程池封装、任务超时检查、服务代理、自定义校验注解等

## flow-framework-facade
提供框架相关的接口定义

### flow-framework-facade-access-log
入访日志相关接口

### flow-framework-facade-alarm
告警相关接口

### flow-framework-facade-authority
鉴权相关接口

### flow-framework-facade-i18n
国际化相关接口

### flow-framework-facade-mq
消息队列相关接口（消费失败后处理）

### flow-framework-facade-system
系统相关接口（系统版本号服务）

## flow-framework-lock
提供分布式锁相关支持，目前使用redsson完成分布式锁封装，对外以提供com.flow.framework.lock.helper.LockHelper为主

## flow-framework-log
提供系统日志服务，如：打包、日志格式以及日志级别刷新等

## flow-framework-module-call
提供模块调用支持，包括如下功能：  
1、请求穿透，系统的trace_id会在整个请求链路中传递，同时，如果后置微服务出错，则相关的错误会进行封装最终可以呈现到最终响应（com.flow.framework.core.response.Response中的code与srcCode）  
2、同一个进程之间的不同模块相互调用不再使用HTTP，通过com.flow.framework.module.call.rpc.annotation.EnableCustomizationFeignClients可以直接将模块服务实现直接注入到需要使用的地方，以便直接调用，但模块服务的实现类必须要有@Validated注解，同时，相应的方法上也需要有@Valid注解，这样即使属于本地调用，spring也会对齐参数的合法性进行校验  
3、支持用户信息以及其他相关信息在调用链路中传递  
4、支持GET方法中传递body时，强制将body数据转换成URI拼接的数据  
5、支持@FeignClient注解中的配置动态刷新，该场景主要用于在线平滑升级

## flow-framework-mq
支持消息队列服务，主要包括如下功能：  
1、生产者客户端封装，规范项目中的消息发送  
2、消费者自动创建相关消息队列  
3、传递通用数据，如：trace_id、租户id、系统版本号、消息发送时间等  
4、封装消费者接口，规范项目中的消息消费  
5、消息消费失败后可以调用flow-framework-facade-mq进行相关业务处理，如：通知运营人员

## flow-framework-persistence
提供持久化服务，包括支持租户id和版本号入库、简化事务的开启和关闭（com.flow.framework.persistence.helper.TransactionHelper）以及规范持久化服务实现的使用和编写规范（com.flow.framework.persistence.persistence.service.impl.PersistenceServiceImpl）

## flow-framework-registry
提供注册支持，即基于nacos注册中心的上下线微服务支持以及健康检查

## flow-framework-schedule
提供调度相关服务，支持基于xxl-job的调度和基于配置中心任务配置的调度，其中，基于配置中心任务配置的调度为分布式分片调度，使用分页的方式进行分片

## flow-framework-web
提供web服务，包括对请求和响应的日志记录、全局异常处理、响应body统一封装等功能