package com.flow.framework.cache.helper;

import com.flow.framework.cache.manager.PojoCacheManager;
import com.flow.framework.cache.properties.FrameworkCacheConfigProperties;
import com.flow.framework.cache.properties.component.LocalCacheConfigProperties;
import com.flow.framework.cache.service.cache.common.ICommonCacheService;
import com.flow.framework.cache.service.cache.common.impl.CommonCacheServiceImpl;
import com.flow.framework.cache.util.LocalCacheConfigUtil;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import com.flow.framework.core.util.ProxyUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.util.ReflectionUtils;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 缓存服务辅助类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Slf4j
@RequiredArgsConstructor
public class CacheServiceHelper implements ISystemLifecycleListener {

    private static final Map<String, ICommonCacheService> CACHE_PREFIX_AND_CACHE_SERVICE_MAP = new ConcurrentHashMap<>();

    private static final ReentrantLock LOCK = new ReentrantLock();

    private static CacheServiceHelper cacheServiceHelper;

    private final LettuceConnectionFactory factory;

    private final ISystemConfigPropertiesService systemConfigPropertiesService;

    private final FrameworkCacheConfigProperties frameworkCacheConfigProperties;

    public static CacheServiceHelper getInstance() {
        return cacheServiceHelper;
    }

    /**
     * 系统启动时初始化
     */
    @Override
    public void onStartUp() {
        cacheServiceHelper = ProxyUtil.getProxied(CacheServiceHelper.class);
        Method initMethod = VerifyUtil.requireNotNull(ReflectionUtils.findMethod(PojoCacheManager.class, "init"),
                () -> {
                    log.error("can't find method. method name : init");
                    return new CheckedException(SystemErrorCode.SYSTEM_START_ERROR);
                });
        initMethod.setAccessible(true);
        ReflectionUtils.invokeMethod(initMethod, null);
        initMethod.setAccessible(false);
    }

    /**
     * 根据应用名称获取通用缓存服务
     *
     * @param applicationNamePlaceholder 应用名称占位符，如：${spring.application.name:flow-framework-service-test}
     * @return 通用缓存服务
     */
    public ICommonCacheService getCacheServiceByPlaceholder(String applicationNamePlaceholder) {
        String applicationName = systemConfigPropertiesService.resolvePlaceholders(applicationNamePlaceholder);
        return getCacheServiceByApplicationName(applicationName);
    }

    /**
     * 根据应用名称获取通用缓存服务
     *
     * @param applicationName 应用名称，如：flow-framework-service-test
     * @return 通用缓存服务
     */
    public ICommonCacheService getCacheServiceByApplicationName(String applicationName) {
        if (VerifyUtil.isEmpty(applicationName)) {
            log.error("cache prefix can't empty.");
            throw new CheckedException(SystemErrorCode.PARAMS_ERROR);
        }
        ICommonCacheService commonCacheService = CACHE_PREFIX_AND_CACHE_SERVICE_MAP.get(applicationName);
        if (null != commonCacheService) {
            return commonCacheService;
        }
        LOCK.lock();
        try {
            commonCacheService = CACHE_PREFIX_AND_CACHE_SERVICE_MAP.get(applicationName);
            if (null == commonCacheService) {
                commonCacheService = new CommonCacheServiceImpl(factory, systemConfigPropertiesService, frameworkCacheConfigProperties) {

                    @Override
                    public String getCacheKeyPrefix() {
                        return applicationName;
                    }

                    @Nullable
                    @Override
                    protected LocalCacheConfigProperties getLocalCacheConfigProperties() {
                        return LocalCacheConfigUtil.getLocalCacheConfigProperties(applicationName,
                                frameworkCacheConfigProperties.getLocalCacheConfigs());
                    }
                };
                ((CommonCacheServiceImpl) commonCacheService).afterPropertiesSet();
                registerCommonCacheService(commonCacheService);
            }
            return commonCacheService;
        } finally {
            LOCK.unlock();
        }
    }

    public static void registerCommonCacheService(ICommonCacheService commonCacheService) {
        CACHE_PREFIX_AND_CACHE_SERVICE_MAP.put(commonCacheService.getCacheKeyPrefix(), commonCacheService);
    }
}