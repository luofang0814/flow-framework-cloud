package com.flow.framework.cache.config;

import com.flow.framework.cache.helper.CacheServiceHelper;
import com.flow.framework.cache.properties.FrameworkCacheConfigProperties;
import com.flow.framework.cache.service.cache.common.ICommonCacheService;
import com.flow.framework.cache.service.cache.common.impl.CommonCacheServiceImpl;
import com.flow.framework.cache.service.system.health.impl.RedisHealthCheckServiceImpl;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;

import java.util.HashSet;

/**
 * 框架缓存配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class FrameworkCacheConfig {

    /**
     * 默认不支持读写分离模式，只有当缓存模式为集群模式和哨兵模式，并且缓存配置开启读写分离模式时才可支持读写分离
     */
    private boolean readClusterSlave = false;

    @Bean
    @ConditionalOnMissingBean
    @RefreshScope
    @ConfigurationProperties(prefix = "customization.framework.cache")
    FrameworkCacheConfigProperties frameworkCacheConfigProperties() {
        return new FrameworkCacheConfigProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    LettuceConnectionFactory lettuceConnectionFactory(RedisProperties properties, FrameworkCacheConfigProperties frameworkCacheConfigProperties) {
        RedisProperties.Pool pool = properties.getLettuce().getPool();
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMinIdle(pool.getMinIdle());
        genericObjectPoolConfig.setMaxIdle(pool.getMaxIdle());
        genericObjectPoolConfig.setMaxTotal(pool.getMaxActive());
        genericObjectPoolConfig.setMaxWaitMillis(pool.getMaxWait().toMillis());
        RedisConfiguration configuration;
        RedisProperties.Cluster cluster = properties.getCluster();
        RedisProperties.Sentinel sentinel = properties.getSentinel();
        LettucePoolingClientConfiguration lettucePoolingClientConfiguration;

        if (!VerifyUtil.isEmpty(cluster) && !VerifyUtil.isEmpty(cluster.getNodes())) {

            // 集群模式
            RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration(cluster.getNodes());
            redisClusterConfiguration.setMaxRedirects(cluster.getMaxRedirects());
            if (!VerifyUtil.isEmpty(properties.getPassword())) {
                redisClusterConfiguration.setPassword(properties.getPassword());
            }
            configuration = redisClusterConfiguration;
            readClusterSlave = frameworkCacheConfigProperties.getReadClusterSlave();
            lettucePoolingClientConfiguration = buildLettucePoolingClientConfiguration(properties, genericObjectPoolConfig, true);
        } else if (!VerifyUtil.isEmpty(sentinel) && !VerifyUtil.hasEmpty(sentinel.getMaster(), sentinel.getNodes())) {

            // 哨兵模式
            String master = sentinel.getMaster();
            configuration = new RedisSentinelConfiguration(master, new HashSet<>(sentinel.getNodes()));
            readClusterSlave = frameworkCacheConfigProperties.getReadClusterSlave();
            lettucePoolingClientConfiguration = buildLettucePoolingClientConfiguration(properties, genericObjectPoolConfig, true);
        } else {

            // 单机
            RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
            redisStandaloneConfiguration.setHostName(properties.getHost());
            if (!VerifyUtil.isEmpty(properties.getPassword())) {
                redisStandaloneConfiguration.setPassword(RedisPassword.of(properties.getPassword()));
            }
            redisStandaloneConfiguration.setPort(properties.getPort());
            redisStandaloneConfiguration.setDatabase(properties.getDatabase());
            configuration = redisStandaloneConfiguration;
            lettucePoolingClientConfiguration = buildLettucePoolingClientConfiguration(properties, genericObjectPoolConfig, false);
        }
        return new LettuceConnectionFactory(configuration, lettucePoolingClientConfiguration);
    }

    private LettucePoolingClientConfiguration buildLettucePoolingClientConfiguration(RedisProperties properties,
                                                                                     GenericObjectPoolConfig genericObjectPoolConfig,
                                                                                     boolean isCluster) {
        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder builder;
        if (isCluster) {
            // 开启自适应刷新和定时刷新
            ClusterTopologyRefreshOptions topologyRefreshOptions = ClusterTopologyRefreshOptions.builder()
                    .enableAllAdaptiveRefreshTriggers()
                    .enablePeriodicRefresh()
                    .build();

            builder = LettucePoolingClientConfiguration.builder().readFrom(readClusterSlave ? ReadFrom.REPLICA : ReadFrom.MASTER)
                    .poolConfig(genericObjectPoolConfig).clientOptions(ClusterClientOptions.builder()
                            .topologyRefreshOptions(topologyRefreshOptions).build()).commandTimeout(properties.getTimeout());
        } else {
            builder = LettucePoolingClientConfiguration.builder().poolConfig(genericObjectPoolConfig)
                    .clientOptions(ClusterClientOptions.builder().build()).commandTimeout(properties.getTimeout());
        }
        if (properties.isSsl()) {
            builder.useSsl();
        }
        return builder.build();
    }

    /**
     * 缓存使用相对较广泛，如果需要定制化使用，请参考com.flow.framework.cache.service.cache.common.impl.CommonCacheServiceImpl
     *
     * @param factory                        连接管理器
     * @param systemConfigPropertiesService  系统配置服务
     * @param frameworkCacheConfigProperties 缓存配置
     * @return 通用缓存服务
     */
    @Bean
    @ConditionalOnMissingBean
    ICommonCacheService commonCacheService(LettuceConnectionFactory factory,
                                           ISystemConfigPropertiesService systemConfigPropertiesService,
                                           FrameworkCacheConfigProperties frameworkCacheConfigProperties) {
        ICommonCacheService commonCacheService = new CommonCacheServiceImpl(factory, systemConfigPropertiesService,
                frameworkCacheConfigProperties);
        CacheServiceHelper.registerCommonCacheService(commonCacheService);
        return commonCacheService;
    }

    @Bean
    @ConditionalOnMissingBean
    CacheServiceHelper cacheServiceHelper(LettuceConnectionFactory factory,
                                          ISystemConfigPropertiesService systemConfigPropertiesService,
                                          FrameworkCacheConfigProperties frameworkCacheConfigProperties) {
        return new CacheServiceHelper(factory, systemConfigPropertiesService, frameworkCacheConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    RedisHealthCheckServiceImpl redisHealthCheckService(LettuceConnectionFactory factory) {
        return new RedisHealthCheckServiceImpl(factory);
    }
}
