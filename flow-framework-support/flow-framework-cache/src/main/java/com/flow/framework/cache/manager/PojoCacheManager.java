package com.flow.framework.cache.manager;

import com.flow.framework.cache.constant.FrameworkCacheConstant;
import com.flow.framework.cache.enumeration.FrameworkPojoCacheLockKeyEnum;
import com.flow.framework.cache.helper.CacheServiceHelper;
import com.flow.framework.cache.service.cache.common.ICommonCacheService;
import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.json.JsonObject;
import com.flow.framework.common.type.TypeReference;
import com.flow.framework.common.util.digest.SignatureUtil;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.lock.helper.LockHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

/**
 * pojo缓存管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/3/25
 */
@Slf4j
public class PojoCacheManager {

    private static final Map<String, ReentrantLock> LOCAL_CACHE_LOCK = new ConcurrentHashMap<>();

    private static final Map<String, PojoCacheManager> PLACEHOLDER_AND_MANAGER_MAP = new ConcurrentHashMap<>();

    private static final Map<String, PojoCacheManager> APPLICATION_NAME_AND_MANAGER_MAP = new ConcurrentHashMap<>();

    private static final long WAITE_CACHE_LOAD_TIME = 5000;

    private static final Object INIT_LOCK = new Object();

    private static volatile boolean alreadyInit = false;


    private ICommonCacheService commonCacheService;

    /**
     * 缓存前缀
     */
    private String cacheKeyPrefixAndColon;


    private void setCommonCacheService(ICommonCacheService commonCacheService) {
        this.commonCacheService = commonCacheService;
    }

    private void setCacheKeyPrefixAndColon(String cacheKeyPrefixAndColon) {
        this.cacheKeyPrefixAndColon = cacheKeyPrefixAndColon;
    }

    public static PojoCacheManager getByApplicationName(String applicationName) {
        return APPLICATION_NAME_AND_MANAGER_MAP.computeIfAbsent(applicationName,
                key -> new PojoCacheManager(applicationName, null));
    }

    public static PojoCacheManager getByPlaceholder(String applicationNamePlaceholder) {
        return PLACEHOLDER_AND_MANAGER_MAP.computeIfAbsent(applicationNamePlaceholder,
                key -> new PojoCacheManager(null, applicationNamePlaceholder));
    }

    private static void init() {
        synchronized (INIT_LOCK) {
            alreadyInit = true;
            CacheServiceHelper instance = CacheServiceHelper.getInstance();
            if (null == instance) {
                log.error("cache service helper is empty.");
                throw new CheckedException(SystemErrorCode.SYSTEM_START_ERROR);
            }
            PLACEHOLDER_AND_MANAGER_MAP.forEach((applicationNamePlaceholder, pojoCacheManager) -> {
                ICommonCacheService commonCacheService = instance.getCacheServiceByPlaceholder(applicationNamePlaceholder);
                pojoCacheManager.setCommonCacheService(commonCacheService);
                pojoCacheManager.setCacheKeyPrefixAndColon(commonCacheService.getCacheKeyPrefix()
                        + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING);
            });
            APPLICATION_NAME_AND_MANAGER_MAP.forEach((applicationName, pojoCacheManager) -> {
                ICommonCacheService commonCacheService = instance.getCacheServiceByApplicationName(applicationName);
                pojoCacheManager.setCommonCacheService(commonCacheService);
                pojoCacheManager.setCacheKeyPrefixAndColon(commonCacheService.getCacheKeyPrefix()
                        + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING);
            });
        }

    }

    private PojoCacheManager(String applicationName, String applicationNamePlaceholder) {
        synchronized (INIT_LOCK) {
            if (alreadyInit) {
                CacheServiceHelper instance = CacheServiceHelper.getInstance();
                if (null == instance) {
                    log.error("cache service helper is empty.");
                    throw new CheckedException(SystemErrorCode.SYSTEM_START_ERROR);
                }
                if (!VerifyUtil.isEmpty(applicationName)) {
                    this.commonCacheService = instance.getCacheServiceByApplicationName(applicationName);
                    this.cacheKeyPrefixAndColon = commonCacheService.getCacheKeyPrefix() + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING;
                    return;
                }
                if (!VerifyUtil.isEmpty(applicationNamePlaceholder)) {
                    this.commonCacheService = instance.getCacheServiceByApplicationName(applicationName);
                    this.cacheKeyPrefixAndColon = commonCacheService.getCacheKeyPrefix() + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING;
                }
            }
        }
    }

    /**
     * 可序列化的大对象获取
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param timeout       timeout
     * @param unit          unit
     * @param supplier      supplier
     * @param <T>
     * @return
     */
    public <T extends Serializable> T bigGet(int version, String key, TypeReference<T> typeReference,
                                             long timeout, TimeUnit unit, Supplier<T> supplier) {
        String md5 = commonCacheService.get(version, key, new TypeReference<String>() {
        });
        if (null != md5) {
            return getAndLoadValue(version, key, typeReference, md5, timeout, unit, supplier);
        }
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        return LockHelper.resultLockExecute(null, FrameworkPojoCacheLockKeyEnum.FRAMEWORK_POJO_CACHE_LOCK_KEY,
                Collections.singletonList(cacheKey), WAITE_CACHE_LOAD_TIME, () -> {
                    String newestMd5 = commonCacheService.get(version, key, new TypeReference<String>() {
                    });
                    if (null != newestMd5) {
                        return getAndLoadValue(version, key, typeReference, newestMd5, timeout, unit, supplier);
                    }

                    ReentrantLock lock = LOCAL_CACHE_LOCK.computeIfAbsent(key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                            lockKey -> new ReentrantLock());
                    lock.lock();
                    try {
                        T newestValue = supplier.get();
                        if (null != newestValue) {
                            String cacheValue = JsonObject.toString(newestValue);
                            newestMd5 = SignatureUtil.md5SignByContent(cacheValue);
                            commonCacheService.set(version, key, newestMd5, timeout, unit);
                            commonCacheService.bigSetLocal(version, key, newestValue, typeReference, timeout, unit);
                        } else {
                            commonCacheService.set(version, key, FrameworkCommonConstant.EMPTY_STRING, timeout, unit);
                        }
                        return newestValue;
                    } finally {
                        lock.unlock();
                    }
                });
    }

    private <T> T getAndLoadValue(int version, String key, TypeReference<T> typeReference, String md5, long timeout,
                                  TimeUnit unit, Supplier<T> supplier) {
        if (VerifyUtil.isEmpty(md5)) {
            return null;
        }
        T value = commonCacheService.bigGetLocal(version, key, md5, typeReference);
        if (null != value) {
            return value;
        }

        ReentrantLock lock = LOCAL_CACHE_LOCK.computeIfAbsent(key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                lockKey -> new ReentrantLock());
        lock.lock();
        try {
            value = supplier.get();
            if (null != value) {
                commonCacheService.bigSetLocal(version, key, value, typeReference, timeout, unit);
            }
            return value;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 缓存大对象删除
     *
     * @param version version
     * @param key     key
     */
    public void bigDelete(int version, String key) {
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        LockHelper.voidLockExecute(null, FrameworkPojoCacheLockKeyEnum.FRAMEWORK_POJO_CACHE_LOCK_KEY,
                Collections.singletonList(cacheKey), WAITE_CACHE_LOAD_TIME, () -> commonCacheService.bigDelete(version, key));
    }

    /**
     * pojo缓存获取
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param timeout       timeout
     * @param unit          unit
     * @param supplier      supplier
     * @param <T>
     * @return
     */
    public <T extends Serializable> T get(int version, String key, TypeReference<T> typeReference,
                                          long timeout, TimeUnit unit, Supplier<T> supplier) {
        CacheResult<T> cacheResult = get(version, key, typeReference);
        if (cacheResult.isSuccess()) {
            return cacheResult.getValue();
        }
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        return LockHelper.resultLockExecute(null, FrameworkPojoCacheLockKeyEnum.FRAMEWORK_POJO_CACHE_LOCK_KEY,
                Collections.singletonList(cacheKey), WAITE_CACHE_LOAD_TIME, () -> {
                    CacheResult<T> newestCacheResult = get(version, key, typeReference);
                    if (newestCacheResult.isSuccess()) {
                        return newestCacheResult.getValue();
                    }
                    T newestValue = supplier.get();
                    commonCacheService.set(version, key, newestValue, timeout, unit);
                    return newestValue;
                });
    }

    /**
     * 缓存对象删除
     *
     * @param version version
     * @param key     key
     */
    public void delete(int version, String key) {
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        LockHelper.voidLockExecute(null, FrameworkPojoCacheLockKeyEnum.FRAMEWORK_POJO_CACHE_LOCK_KEY,
                Collections.singletonList(cacheKey), WAITE_CACHE_LOAD_TIME, () -> commonCacheService.delete(version, key));
    }


    @SuppressWarnings("unchecked")
    private <T extends Serializable> CacheResult<T> get(int version, String key, TypeReference<T> typeReference) {
        String valueString = commonCacheService.get(version, key, new TypeReference<String>() {
        });
        if (null != valueString) {
            if (valueString.isEmpty()) {
                return new CacheResult(true, null);
            }
            Type type = typeReference.getType();
            if (type instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();

                // 如果泛型只有一个并且是string并且对象的类型是string，则直接强转为string
                if (actualTypeArguments.length == 1
                        && actualTypeArguments[0].equals(String.class)) {
                    return new CacheResult(true, (T) valueString);
                }
            }
            return new CacheResult(true, JsonObject.toBean(valueString, typeReference));
        }
        return new CacheResult(false, null);
    }

    @Data
    @AllArgsConstructor
    private static class CacheResult<T> {

        private boolean success;

        private T value;
    }
}