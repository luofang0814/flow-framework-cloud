package com.flow.framework.cache.service.system.health.impl;

import com.flow.framework.common.health.ServiceHealthCheckCode;
import com.flow.framework.common.pojo.vo.system.ServiceHealthVo;
import com.flow.framework.common.service.system.health.IHealthCheckService;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.system.helper.SystemHealthHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.ClusterInfo;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisConnectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * redis健康检查
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Slf4j
@RequiredArgsConstructor
public class RedisHealthCheckServiceImpl implements IHealthCheckService {

    private static final String OK_STATUS_STRING = "ok";

    private static final String REDIS_VERSION = "redis_version";

    private final RedisConnectionFactory redisConnectionFactory;

    /**
     * @inheritDoc
     */
    @Override
    public List<ServiceHealthVo> check() {
        try {
            RedisConnection connection = RedisConnectionUtils.getConnection(this.redisConnectionFactory);
            try {
                if (connection instanceof RedisClusterConnection) {
                    ClusterInfo clusterInfo = ((RedisClusterConnection) connection).clusterGetClusterInfo();
                    String state = clusterInfo.getState();
                    if (!OK_STATUS_STRING.equalsIgnoreCase(state)) {
                        log.error("redis cluster server status error. status : {}", state);
                        return Collections.singletonList(
                                SystemHealthHelper.unhealthy(ServiceHealthCheckCode.SERVICE_DISTRIBUTION_CACHE_CODE,
                                        "cache status " + state, null)
                        );
                    }
                } else {
                    Properties info = connection.info();
                    if (VerifyUtil.isEmpty(info)) {
                        log.error("redis server status error. redis info is null");
                        return Collections.singletonList(
                                SystemHealthHelper.unhealthy(ServiceHealthCheckCode.SERVICE_DISTRIBUTION_CACHE_CODE,
                                        "can't get connect info", null)
                        );
                    }
                    String version = info.getProperty(REDIS_VERSION);
                    if (VerifyUtil.isEmpty(version)) {
                        log.error("redis server status error. version : {}", version);
                        return Collections.singletonList(
                                SystemHealthHelper.unhealthy(ServiceHealthCheckCode.SERVICE_DISTRIBUTION_CACHE_CODE,
                                        "can't get cache version", null)
                        );
                    }
                }
                return Collections.singletonList(SystemHealthHelper.healthy(ServiceHealthCheckCode.SERVICE_DISTRIBUTION_CACHE_CODE));
            } finally {
                RedisConnectionUtils.releaseConnection(connection, this.redisConnectionFactory, false);
            }
        } catch (Exception e) {
            log.error("check redis error.", e);
            return Collections.singletonList(
                    SystemHealthHelper.unhealthy(ServiceHealthCheckCode.SERVICE_DISTRIBUTION_CACHE_CODE, e.getMessage(), null)
            );
        }
    }
}
