package com.flow.framework.cache.properties;

import com.flow.framework.cache.properties.component.LocalCacheConfigProperties;
import lombok.Data;

import java.util.List;

/**
 * 基础框架缓存配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Data
public class FrameworkCacheConfigProperties {

    /**
     * 是否开启读写分离，仅在缓存模式为集群模式和哨兵模式时才生效
     */
    private Boolean readClusterSlave = false;

    /**
     * 本地缓存配置
     */
    private List<LocalCacheConfigProperties> localCacheConfigs;
}
