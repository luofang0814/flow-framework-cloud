package com.flow.framework.cache.enumeration;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 框架获取分布式锁获健康检查的key的枚举类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@AllArgsConstructor
@Getter
public enum FrameworkPojoCacheLockKeyEnum implements ILockKeyEnum {

    /**
     * pojo缓存
     */
    FRAMEWORK_POJO_CACHE_LOCK_KEY(1, "pojo缓存"),
    ;

    private int paramsSize;

    private String remark;
}
