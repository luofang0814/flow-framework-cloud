package com.flow.framework.cache.properties.component;

import lombok.Data;

/**
 * 本地缓存配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Data
public class LocalCacheConfigProperties {

    /**
     * 缓存前缀
     */
    private String cacheKeyPrefix;

    /**
     * 缓存前缀对应的缓存个数
     */
    private int maxSize;
}