package com.flow.framework.cache.service.cache.common.impl;

import com.flow.framework.cache.constant.FrameworkCacheConstant;
import com.flow.framework.cache.properties.FrameworkCacheConfigProperties;
import com.flow.framework.cache.properties.component.LocalCacheConfigProperties;
import com.flow.framework.cache.service.cache.common.ICommonCacheService;
import com.flow.framework.cache.util.LocalCacheConfigUtil;
import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.json.JsonObject;
import com.flow.framework.common.json.ObjectMapperHolder;
import com.flow.framework.common.tuple.KeyValueTuple;
import com.flow.framework.common.type.TypeReference;
import com.flow.framework.common.util.digest.SignatureUtil;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.constant.FrameworkCoreConstant;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.ResolvableType;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 通用缓存服务，通用缓存服务可能会用到服务的facade里面，此时创建服务请使用
 * com.flow.framework.cache.helper.CacheServiceHelper#getCustomizationCacheService(java.lang.String)方法进行获取
 * 注意：在facade只能修改本地缓存中的值而不可以修改分布式缓存中的值，否则可能导致缓存数据与实际数据不一致问题
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
@Slf4j
@RequiredArgsConstructor
public class CommonCacheServiceImpl implements ICommonCacheService, InitializingBean {

    private static final int JSON_STRING_MIN_LEN = 2;

    private final LettuceConnectionFactory factory;

    private final ISystemConfigPropertiesService systemConfigPropertiesService;

    private final FrameworkCacheConfigProperties frameworkCacheConfigProperties;

    private Cache<String, BigElement> localCache;

    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 缓存前缀
     */
    private String cacheKeyPrefixAndColon;

    @Data
    @AllArgsConstructor
    private static final class BigElement {

        /**
         * content的MD5值
         */
        private String md5;

        /**
         * 类型处理器
         */
        private ResolvableType resolvableType;

        /**
         * 内容
         */
        private Object content;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void afterPropertiesSet() {
        cacheKeyPrefixAndColon = getCacheKeyPrefix()+ FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING;
        buildRedisTemplate();
        LocalCacheConfigProperties localCacheConfigProperties = getLocalCacheConfigProperties();
        if (VerifyUtil.isEmpty(localCacheConfigProperties)) {
            return;
        }
        localCache = Caffeine.newBuilder().softValues().maximumSize(localCacheConfigProperties.getMaxSize()).build();
    }

    @Override
    public String getCacheKeyPrefix() {
        return systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_NAME_KEY, "unknown");
    }

    @Nullable
    protected LocalCacheConfigProperties getLocalCacheConfigProperties() {
        return LocalCacheConfigUtil.getLocalCacheConfigProperties(getCacheKeyPrefix(),
                frameworkCacheConfigProperties.getLocalCacheConfigs());
    }

    private void buildRedisTemplate() {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer(StandardCharsets.UTF_8);
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);

        Jackson2JsonRedisSerializer<Object> jsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class) {

            /**
             * @inheritDoc
             */
            @Override
            public Object deserialize(byte[] bytes) throws SerializationException {
                if (bytes == null || bytes.length == 0) {
                    return null;
                }
                return new String(bytes, StandardCharsets.UTF_8);
            }
        };
        jsonRedisSerializer.setObjectMapper(ObjectMapperHolder.getInstance());
        redisTemplate.setValueSerializer(jsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jsonRedisSerializer);

        redisTemplate.afterPropertiesSet();
        this.redisTemplate = redisTemplate;
    }

    @SuppressWarnings("unchecked")
    private <T> T cast(Object o, TypeReference<T> typeReference) {
        if (null == o) {
            return null;
        }

        Type type = typeReference.getType();
        if (type instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();

            // 如果泛型只有一个并且是string并且对象的类型是string，则直接强转为string
            if (actualTypeArguments.length == 1
                    && actualTypeArguments[0].equals(String.class)
                    && o.getClass() == String.class) {
                return (T) o;
            }
        }

        try {
            if (o instanceof Collection) {
                Collection collection = (Collection) o;
                return JsonObject.toBean(getJsonArrayString(collection), typeReference);
            }
            if (o instanceof Map) {
                Map map = (Map) o;
                return JsonObject.toBean(getJsonMapString(map), typeReference);
            }
            return JsonObject.toBean((String) o, typeReference);
        } catch (Exception e) {
            log.error("redis service convert error, old type : {}, new type : {}",
                    o.getClass().getSimpleName(), type.getTypeName(), e);
            throw new CheckedException(SystemErrorCode.UNEXPECTED_ERROR, "cache convert type error.", e);
        }
    }

    /**
     * 将容器转换成字符串，该方法不可使用JsonObject.toString替换，原因是当前redis返回的数据为原生标准的json字符串，
     * 需要额外处理KeyValueTuple情况，因为redis返回的json字符串如果使用JsonObject.toString进行序列化，
     * 则该json字符串会被转义，导致后续使用typeReference进行反序列化失败，故该方法是保证KeyValueTuple中的json字符串不被转义
     *
     * @param collection collection
     * @return
     */
    @SuppressWarnings("unchecked")
    private String getJsonArrayString(Collection collection) {
        StringBuilder jsonArrayString = new StringBuilder("[");
        collection.forEach(s -> {
            if (s instanceof KeyValueTuple) {
                KeyValueTuple keyValueTuple = (KeyValueTuple) s;
                StringBuilder jsonString = new StringBuilder("{\"key\":");
                Object key = keyValueTuple.getKey();
                appendObjectToJsonString(jsonString, key);
                jsonString.append(",\"value\":");
                Object value = keyValueTuple.getValue();
                appendObjectToJsonString(jsonString, value);
                jsonString.append(FrameworkCommonConstant.RIGHT_BRACE);
                jsonArrayString.append(jsonString.toString());
            } else {
                jsonArrayString.append(s);
            }
            jsonArrayString.append(",");
        });
        if (jsonArrayString.length() >= JSON_STRING_MIN_LEN) {
            jsonArrayString.deleteCharAt(jsonArrayString.length() - 1);
        }
        jsonArrayString.append(FrameworkCommonConstant.RIGHT_BRACKET);
        return jsonArrayString.toString();
    }

    private void appendObjectToJsonString(StringBuilder jsonString, Object o) {
        if (o instanceof String) {
            String temp = (String) o;

            boolean isJsonString = (temp.startsWith(FrameworkCommonConstant.LEFT_BRACE)
                    && temp.endsWith(FrameworkCommonConstant.RIGHT_BRACE))
                    || (temp.startsWith(FrameworkCommonConstant.LEFT_BRACKET)
                    && temp.endsWith(FrameworkCommonConstant.RIGHT_BRACKET));

            // 如果是json字符串，则直接进行拼接
            if (isJsonString) {
                jsonString.append(o);
            } else {

                //如果不是json字符串，则需要添加引号
                jsonString.append("\"").append(o).append("\"");
            }
        } else {
            // 如果是非字符串则需要先转成json字符串
            jsonString.append(JsonObject.toString(o));
        }
    }

    /**
     * 将map转换成字符串，该方法不可使用JsonObject.toString替换，原因是当前redis返回的数据为原生标准的json字符串，
     * 需要额外处理map的key和value，因为redis返回的json字符串如果使用JsonObject.toString进行序列化，
     * 则该json字符串会被转义，导致后续使用typeReference进行反序列化失败，故该方法是保证map中的json字符串不被转义
     *
     * @param map map
     * @return
     */
    @SuppressWarnings("unchecked")
    private String getJsonMapString(Map map) {
        StringBuilder jsonString = new StringBuilder(FrameworkCommonConstant.LEFT_BRACE);
        map.forEach((key, value) -> {
            if (!(key instanceof String)) {
                log.error("cache data error. type name : {}", key.getClass().getSimpleName());
                throw new CheckedException(SystemErrorCode.DATA_TYPE_ERROR);
            }
            jsonString.append("\"").append(key).append("\":");
            appendObjectToJsonString(jsonString, value);
            jsonString.append(",");
        });

        if (jsonString.length() >= JSON_STRING_MIN_LEN) {
            jsonString.deleteCharAt(jsonString.length() - 1);
        }
        jsonString.append(FrameworkCommonConstant.RIGHT_BRACE);
        return jsonString.toString();
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> void bigSet(int version, String key, T value, TypeReference<T> typeReference, long timeout, TimeUnit timeUnit) {
        bigSet(version, key, value, typeReference, timeout, timeUnit, false);
    }

    private <T> void bigSet(int version, String key, T value, TypeReference<T> typeReference, long timeout, TimeUnit timeUnit, boolean isLocal) {
        if (VerifyUtil.isEmpty(localCache)) {
            log.error("local cache error, cache key prefix : {}.", cacheKeyPrefixAndColon);
            throw new CheckedException(SystemErrorCode.LOCAL_CACHE_ERROR);
        }
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        String cacheValue = JsonObject.toString(value);
        String md5 = SignatureUtil.md5SignByContent(cacheValue);
        BigElement bigElement = new BigElement(md5, ResolvableType.forType(typeReference.getType()), value);
        localCache.put(cacheKey, bigElement);
        if (!isLocal) {
            set(version, key, md5, timeout, timeUnit);
        }
    }

    /**
     * 可序列化的大对象缓存（只缓存到本地）
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param value         value
     * @param typeReference value对应的类型，主要用于缓存在获取的时候，如果类型相同则不再序列化
     * @param timeout       timeout
     * @param timeUnit      timeUnit
     */
    @Override
    public <T> void bigSetLocal(int version, String key, T value, TypeReference<T> typeReference, long timeout, TimeUnit timeUnit) {
        bigSet(version, key, value, typeReference, timeout, timeUnit, true);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T bigGet(int version, String key, TypeReference<T> typeReference) {
        if (VerifyUtil.isEmpty(localCache)) {
            log.error("local cache error, cache key prefix : {}.", cacheKeyPrefixAndColon);
            throw new CheckedException(SystemErrorCode.LOCAL_CACHE_ERROR);
        }
        String md5 = get(version, key, new TypeReference<String>() {
        });
        if (VerifyUtil.isEmpty(md5)) {
            return null;
        }
        return bigGetLocal(version, key, md5, typeReference);
    }

    /**
     * @inheritDoc
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T bigGetLocal(int version, String key, String sign, TypeReference<T> typeReference) {
        if (VerifyUtil.isEmpty(sign)){
            return null;
        }
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        BigElement bigElement = localCache.getIfPresent(cacheKey);
        if (null == bigElement) {
            return null;
        }
        if (sign.equals(bigElement.getMd5())) {
            Object content = bigElement.getContent();
            if (bigElement.getResolvableType().isAssignableFrom(ResolvableType.forType(typeReference.getType()))) {
                return (T) content;
            }
            return JsonObject.toBean(JsonObject.toString(content), typeReference);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void bigDelete(int version, String key) {
        if (VerifyUtil.isEmpty(localCache)) {
            log.error("local cache error, cache key prefix : {}.", cacheKeyPrefixAndColon);
            throw new CheckedException(SystemErrorCode.LOCAL_CACHE_ERROR);
        }
        delete(version, key);
        String cacheKey = cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version;
        localCache.invalidate(cacheKey);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long getExpire(String key) {
        return redisTemplate.getExpire(key);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean expire(int version, String key, long timeout, TimeUnit unit) {
        return redisTemplate.expire(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, timeout, unit);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean setIfAbsent(int version, String key, Serializable value, long timeout, TimeUnit unit) {
        return redisTemplate.opsForValue().setIfAbsent(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                value, timeout, unit);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void delete(int version, String key) {
        redisTemplate.delete(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T get(int version, String key, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForValue().get(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void set(int version, String key, Serializable value) {
        redisTemplate.opsForValue().set(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void set(int version, String key, Serializable value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value, timeout, unit);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long increment(int version, String key, long delta) {
        return redisTemplate.opsForValue().increment(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, delta);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Double increment(int version, String key, double delta) {
        return redisTemplate.opsForValue().increment(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, delta);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long hashDelete(int version, String key, String... hashKeys) {
        return redisTemplate.opsForHash().delete(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, hashKeys);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean hashHasKey(int version, String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, hashKey);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T hashGet(int version, String key, String hashKey, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForHash().get(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, hashKey);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends List> T hashMultiGet(int version, String key, Collection<String> hashKeys,
                                           TypeReference<T> typeReference) {
        List<Object> objects = redisTemplate.opsForHash().multiGet(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING
                + version, (Collection) hashKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long hashIncrement(int version, String key, String hashKey, long delta) {
        return redisTemplate.opsForHash().increment(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                hashKey, delta);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Double hashIncrement(int version, String key, String hashKey, double delta) {
        return redisTemplate.opsForHash().increment(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                hashKey, delta);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set<String>> T hashKeys(int version, String key, TypeReference<T> typeReference) {
        Set<Object> keys = redisTemplate.opsForHash().keys(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(keys, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long hashSize(int version, String key) {
        return redisTemplate.opsForHash().size(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void hashPutAll(int version, String key, Map<String, ? extends Serializable> map) {
        redisTemplate.opsForHash().putAll(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, map);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void hashPut(int version, String key, String hashKey, Serializable value) {
        redisTemplate.opsForHash().put(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, hashKey, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean hashPutIfAbsent(int version, String key, String hashKey, Serializable value) {
        return redisTemplate.opsForHash().putIfAbsent(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                hashKey, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends List> T hashValues(int version, String key, TypeReference<T> typeReference) {
        List<Object> values = redisTemplate.opsForHash().values(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(values, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <E extends Map<String, ? extends Serializable>> E hashEntries(int version, String key, TypeReference<E> mapTypeReference) {
        Map<Object, Object> entries = redisTemplate.opsForHash().entries(cacheKeyPrefixAndColon + key
                + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(entries, mapTypeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean zSetAdd(int version, String key, Serializable value, double score) {
        return redisTemplate.opsForZSet().add(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value, score);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetAdd(int version, String key, Collection<KeyValueTuple<Serializable, Double>> tuples) {
        Set<ZSetOperations.TypedTuple<Object>> tempTuples = new HashSet<>();
        tuples.forEach(keyValueTuple ->
                tempTuples.add(new DefaultTypedTuple<>(keyValueTuple.getKey(), keyValueTuple.getValue())));
        return redisTemplate.opsForZSet().add(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, tempTuples);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetRemove(int version, String key, Serializable... values) {
        return redisTemplate.opsForZSet().remove(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Double zSetIncrementScore(int version, String key, Serializable value, double delta) {
        return redisTemplate.opsForZSet().incrementScore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                value, delta);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetRank(int version, String key, Serializable value) {
        return redisTemplate.opsForZSet().rank(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetReverseRank(int version, String key, Serializable value) {
        return redisTemplate.opsForZSet().reverseRank(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T zSetRange(int version, String key, long start, long end, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForZSet().range(cacheKeyPrefixAndColon + key
                + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set<KeyValueTuple<K, Double>>, K> T zSetRangeWithScores(int version, String key, long start, long end,
                                                                              TypeReference<T> typeReference) {
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate
                .opsForZSet().rangeWithScores(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
        return castZSet(typedTuples, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T zSetRangeByScore(int version, String key, double min, double max, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForZSet().rangeByScore(cacheKeyPrefixAndColon + key
                + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, min, max);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set<KeyValueTuple<K, Double>>, K> T zSetRangeByScoreWithScores(int version, String key, double min,
                                                                                     double max, TypeReference<T> typeReference) {
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate
                .opsForZSet().rangeByScoreWithScores(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, min, max);
        return castZSet(typedTuples, typeReference);
    }

    private <T extends Set<KeyValueTuple<K, Double>>, K> T castZSet(Set<ZSetOperations.TypedTuple<Object>> typedTuples,
                                                                    TypeReference<T> typeReference) {
        if (VerifyUtil.isEmpty(typedTuples)) {
            return cast(Collections.emptySet(), typeReference);
        }
        Set<KeyValueTuple<Object, Double>> keyValueTuples = new LinkedHashSet<>();
        typedTuples.forEach(objectTypedTuple -> {
            Object value = objectTypedTuple.getValue();
            Double score = objectTypedTuple.getScore();
            KeyValueTuple<Object, Double> keyValueTuple = new KeyValueTuple<>(value, score);
            keyValueTuples.add(keyValueTuple);
        });
        return cast(keyValueTuples, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T zSetReverseRange(int version, String key, long start, long end, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForZSet().reverseRange(cacheKeyPrefixAndColon + key
                + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set<KeyValueTuple<K, Double>>, K> T zSetReverseRangeWithScores(int version, String key, long start,
                                                                                     long end, TypeReference<T> typeReference) {
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate
                .opsForZSet().reverseRangeWithScores(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
        return castZSet(typedTuples, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T zSetReverseRangeByScore(int version, String key, double min, double max,
                                                     TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForZSet().reverseRangeByScore(cacheKeyPrefixAndColon + key
                + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, min, max);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set<KeyValueTuple<K, Double>>, K> T zSetReverseRangeByScoreWithScores(int version, String key,
                                                                                            double min, double max,
                                                                                            TypeReference<T> typeReference) {
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate
                .opsForZSet().reverseRangeByScoreWithScores(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING
                        + version, min, max);
        return castZSet(typedTuples, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetCount(int version, String key, double min, double max) {
        return redisTemplate.opsForZSet().count(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, min, max);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetSize(int version, String key) {
        return redisTemplate.opsForZSet().size(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Double zSetScore(int version, String key, Serializable value) {
        return redisTemplate.opsForZSet().score(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetRemoveRange(int version, String key, long start, long end) {
        return redisTemplate.opsForZSet().removeRange(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                start, end);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetRemoveRangeByScore(int version, String key, double min, double max) {
        return redisTemplate.opsForZSet().removeRangeByScore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING
                + version, min, max);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetUnionAndStore(int version, String key, int otherVersion, String otherKey, int destVersion,
                                  String destKey) {
        return redisTemplate.opsForZSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey, cacheKeyPrefixAndColon + destKey);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                  String destKey) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForZSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                  String destKey, Aggregate aggregate) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForZSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion, aggregate.getAggregate());
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                  String destKey, Aggregate aggregate, List<Double> weights) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        int size = weights.size();
        double[] tempArray = new double[size];
        for (int i = 0; i < size; i++) {
            tempArray[i] = weights.get(i);
        }
        return redisTemplate.opsForZSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion, aggregate.getAggregate(),
                RedisZSetCommands.Weights.of(tempArray));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetIntersectAndStore(int version, String key, int otherVersion, String otherKey, int destVersion,
                                      String destKey) {
        return redisTemplate.opsForZSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                      String destKey) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForZSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                      String destKey, Aggregate aggregate) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForZSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion,
                aggregate.getAggregate());
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                      String destKey, Aggregate aggregate, List<Double> weights) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        int size = weights.size();
        double[] tempArray = new double[size];
        for (int i = 0; i < size; i++) {
            tempArray[i] = weights.get(i);
        }
        return redisTemplate.opsForZSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion,
                aggregate.getAggregate(), RedisZSetCommands.Weights.of(tempArray));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setAdd(int version, String key, Serializable... values) {
        return redisTemplate.opsForSet().add(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setRemove(int version, String key, Serializable... values) {
        return redisTemplate.opsForSet().remove(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T setPop(int version, String key, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForSet().pop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends List> T setPop(int version, String key, long count, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForSet().pop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, count);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean setMove(int version, String key, Serializable value, int destVersion, String destKey) {
        return redisTemplate.opsForSet().move(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setSize(int version, String key) {
        return redisTemplate.opsForSet().size(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Boolean setIsMember(int version, String key, Serializable value) {
        return redisTemplate.opsForSet().isMember(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setIntersect(int version, String key, int otherVersion, String otherKey, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForSet().intersect(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setIntersect(int version, String key, int otherVersion, Collection<String> otherKeys,
                                          TypeReference<T> typeReference) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet()
                .intersect(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setIntersect(int version, Collection<String> keys, TypeReference<T> typeReference) {
        Set<String> targetKeys = keys.stream().map(key -> cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet().intersect(targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setIntersectAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey) {
        return redisTemplate.opsForSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys,
                                     int destVersion, String destKey) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().intersectAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setIntersectAndStore(int version, Collection<String> keys, int destVersion, String destKey) {
        Set<String> targetKeys = keys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().intersectAndStore(targetKeys,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setUnion(int version, String key, int otherVersion, String otherKey, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForSet().union(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setUnion(int version, String key, int otherVersion, Collection<String> otherKeys,
                                      TypeReference<T> typeReference) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet()
                .union(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setUnion(int version, Collection<String> keys, TypeReference<T> typeReference) {
        Set<String> targetKeys = keys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet().union(targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setUnionAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey) {
        return redisTemplate.opsForSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                 String destKey) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().unionAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, targetKeys,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setUnionAndStore(int version, Collection<String> keys, int destVersion, String destKey) {
        Set<String> targetKeys = keys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().unionAndStore(targetKeys,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setDifference(int version, String key, int otherVersion, String otherKey,
                                           TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForSet()
                .difference(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                        cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setDifference(int version, String key, int otherVersion, Collection<String> otherKeys,
                                           TypeReference<T> typeReference) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet()
                .difference(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setDifference(int version, Collection<String> keys, TypeReference<T> typeReference) {
        Set<String> targetKeys = keys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        Set<Object> objects = redisTemplate.opsForSet().difference(targetKeys);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setDifferenceAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey) {
        return redisTemplate.opsForSet().differenceAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                cacheKeyPrefixAndColon + otherKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setDifferenceAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                                      String destKey) {
        Set<String> targetKeys = otherKeys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + otherVersion)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().differenceAndStore(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                targetKeys, cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long setDifferenceAndStore(int version, Collection<String> keys, int destVersion, String destKey) {
        Set<String> targetKeys = keys.stream()
                .map(tempKey -> cacheKeyPrefixAndColon + tempKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version)
                .collect(Collectors.toSet());
        return redisTemplate.opsForSet().differenceAndStore(targetKeys,
                cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends Set> T setMembers(int version, String key, TypeReference<T> typeReference) {
        Set<Object> objects = redisTemplate.opsForSet().members(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T extends List> T listRange(int version, String key, long start, long end, TypeReference<T> typeReference) {
        List<Object> objects = redisTemplate.opsForList()
                .range(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
        return cast(objects, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void listTrim(int version, String key, long start, long end) {
        redisTemplate.opsForList().trim(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, start, end);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listSize(int version, String key) {
        return redisTemplate.opsForList().size(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listLeftPush(int version, String key, Serializable value) {
        return redisTemplate.opsForList().leftPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listLeftPushAll(int version, String key, Serializable... values) {
        return redisTemplate.opsForList().leftPushAll(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listLeftPushAll(int version, String key, Collection<Serializable> values) {
        return redisTemplate.opsForList().leftPushAll(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listLeftPushIfPresent(int version, String key, Serializable value) {
        return redisTemplate.opsForList().leftPushIfPresent(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listLeftPush(int version, String key, Serializable pivot, Serializable value) {
        return redisTemplate.opsForList().leftPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, pivot, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRightPush(int version, String key, Serializable value) {
        return redisTemplate.opsForList().rightPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRightPushAll(int version, String key, Serializable... values) {
        return redisTemplate.opsForList().rightPushAll(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRightPushAll(int version, String key, Collection<Serializable> values) {
        return redisTemplate.opsForList().rightPushAll(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, values);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRightPushIfPresent(int version, String key, Serializable value) {
        return redisTemplate.opsForList().rightPushIfPresent(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRightPush(int version, String key, Serializable pivot, Serializable value) {
        return redisTemplate.opsForList().rightPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, pivot, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void listSet(int version, String key, long index, Serializable value) {
        redisTemplate.opsForList().set(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, index, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public Long listRemove(int version, String key, long count, Serializable value) {
        return redisTemplate.opsForList().remove(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, count, value);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listIndex(int version, String key, long index, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList().index(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version, index);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listLeftPop(int version, String key, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList().leftPop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listLeftPop(int version, String key, long timeout, TimeUnit unit, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList().leftPop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                timeout, unit);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listRightPop(int version, String key, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList().rightPop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listRightPop(int version, String key, long timeout, TimeUnit unit, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList().rightPop(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                timeout, unit);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listRightPopAndLeftPush(int version, String key, int destVersion, String destKey, TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList()
                .rightPopAndLeftPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                        cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion);
        return cast(o, typeReference);
    }

    /**
     * @inheritDoc
     */
    @Override
    public <T> T listRightPopAndLeftPush(int version, String key, int destVersion, String destKey, long timeout, TimeUnit unit,
                                         TypeReference<T> typeReference) {
        Object o = redisTemplate.opsForList()
                .rightPopAndLeftPush(cacheKeyPrefixAndColon + key + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + version,
                        cacheKeyPrefixAndColon + destKey + FrameworkCacheConstant.CACHE_KEY_SPLIT_STRING + destVersion, timeout, unit);
        return cast(o, typeReference);
    }
}
