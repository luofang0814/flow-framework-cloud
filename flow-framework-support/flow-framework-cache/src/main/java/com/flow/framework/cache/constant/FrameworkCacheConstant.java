package com.flow.framework.cache.constant;

/**
 * cache常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
public final class FrameworkCacheConstant {

    /**
     * 缓存中的key的分隔符
     */
    public static final String CACHE_KEY_SPLIT_STRING = ":";

    /**
     * 利用ID缓存值的key
     */
    public static final String CACHE_VALUE_ID_KEY = ":ID:";
}
