package com.flow.framework.cache.service.cache.common;

import com.flow.framework.common.tuple.KeyValueTuple;
import com.flow.framework.common.type.TypeReference;
import org.springframework.data.redis.connection.RedisZSetCommands;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 通用缓存服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
public interface ICommonCacheService {

    /**
     * 获取缓存前缀
     *
     * @return 缓存前缀
     */
    String getCacheKeyPrefix();

    /**
     * 可序列化的大对象缓存
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param value         value
     * @param typeReference value对应的类型，主要用于缓存在获取的时候，如果类型相同则不再序列化
     * @param timeout       timeout
     * @param timeUnit      timeUnit
     */
    <T> void bigSet(int version, String key, T value, TypeReference<T> typeReference, long timeout, TimeUnit timeUnit);

    /**
     * 可序列化的大对象缓存（只缓存到本地）
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param value         value
     * @param typeReference value对应的类型，主要用于缓存在获取的时候，如果类型相同则不再序列化
     * @param timeout       timeout
     * @param timeUnit      timeUnit
     */
    <T> void bigSetLocal(int version, String key, T value, TypeReference<T> typeReference, long timeout, TimeUnit timeUnit);

    /**
     * 可序列化的大对象获取
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>
     * @return
     */
    <T> T bigGet(int version, String key, TypeReference<T> typeReference);

    /**
     * 可序列化的大对象获取（只从缓存获取）
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param sign          数据签名
     * @param typeReference typeReference
     * @param <T>
     * @return
     */
    <T> T bigGetLocal(int version, String key, String sign, TypeReference<T> typeReference);

    /**
     * 可序列化大对象删除
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     */
    void bigDelete(int version, String key);

    /**
     * 判断key是否存在
     *
     * @param key key
     * @return
     */
    Boolean hasKey(String key);

    /**
     * 获取key的过期时间
     *
     * @param key key
     * @return
     */
    Long getExpire(String key);

    /**
     * 设置缓存超时时间，如果过期时间设置为-1，则永不过期
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param timeout timeout
     * @param unit    unit
     * @return
     */
    Boolean expire(int version, String key, long timeout, TimeUnit unit);

    /**
     * 相对于redis的setnx，即如果没有就设置值，如果有就失败
     *
     * @param version 版本
     * @param key     key
     * @param value   value
     * @param timeout timeout
     * @param unit    unit
     * @return
     */
    Boolean setIfAbsent(int version, String key, Serializable value, long timeout, TimeUnit unit);

    /**
     * 删除缓存对象
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     */
    void delete(int version, String key);

    /**
     * 获取缓存对象
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>
     * @return
     */
    <T> T get(int version, String key, TypeReference<T> typeReference);

    /**
     * 缓存缓存对象
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     */
    void set(int version, String key, Serializable value);

    /**
     * 缓存缓存对象
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @param timeout timeout
     * @param unit    unit
     */
    void set(int version, String key, Serializable value, long timeout, TimeUnit unit);

    /**
     * 保证原子操作的情况下给key加上一个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param delta   delta
     * @return
     */
    Long increment(int version, String key, long delta);

    /**
     * 保证原子操作的情况下给key加上一个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param delta   delta
     * @return
     */
    Double increment(int version, String key, double delta);


    /**
     * 删除hash中的指定hash key
     *
     * @param version  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key      key
     * @param hashKeys hashKeys
     * @return
     */
    Long hashDelete(int version, String key, String... hashKeys);

    /**
     * 判断hash中是否存在指定hash key
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param hashKey hashKey
     * @return
     */
    Boolean hashHasKey(int version, String key, String hashKey);

    /**
     * 获取hash中是否存在指定hash key
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param hashKey       hashKey
     * @param typeReference typeReference
     * @return
     */
    <T> T hashGet(int version, String key, String hashKey, TypeReference<T> typeReference);

    /**
     * 批量获取hash中是否存在指定hash key
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param hashKeys      hashKeys
     * @param typeReference typeReference
     * @return
     */
    <T extends List> T hashMultiGet(int version, String key, Collection<String> hashKeys, TypeReference<T> typeReference);

    /**
     * 保证原子操作的情况下对hash的的key加上指定值
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param hashKey hashKey
     * @param delta   delta
     * @return
     */
    Long hashIncrement(int version, String key, String hashKey, long delta);

    /**
     * 保证原子操作的情况下对hash的的key加上指定值
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param hashKey hashKey
     * @param delta   delta
     * @return
     */
    Double hashIncrement(int version, String key, String hashKey, double delta);

    /**
     * 获取hash的所有key
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @return
     */
    <T extends Set<String>> T hashKeys(int version, String key, TypeReference<T> typeReference);

    /**
     * 获取hash的大小
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @return
     */
    Long hashSize(int version, String key);

    /**
     * 批量往hash中存放键值对
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param map     map
     */
    void hashPutAll(int version, String key, Map<String, ? extends Serializable> map);

    /**
     * 往hash中存放键值对
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param hashKey hashKey
     * @param value   value
     */
    void hashPut(int version, String key, String hashKey, Serializable value);

    /**
     * 当hash中不存在hash key时，往hash中存放键值对
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param hashKey hashKey
     * @param value   value
     * @return
     */
    Boolean hashPutIfAbsent(int version, String key, String hashKey, Serializable value);

    /**
     * 获取hash中全部值
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends List> T hashValues(int version, String key, TypeReference<T> typeReference);

    /**
     * 获取hash中的全部键值对
     *
     * @param version          版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key              key
     * @param mapTypeReference mapTypeReference
     * @param <E>              反序列化泛型
     * @return
     */
    <E extends Map<String, ? extends Serializable>> E hashEntries(int version, String key, TypeReference<E> mapTypeReference);

    /**
     * 往zSet中添加元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @param score   score
     * @return
     */
    Boolean zSetAdd(int version, String key, Serializable value, double score);

    /**
     * 批量往zSet中添加元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param tuples  tuples
     * @return
     */
    Long zSetAdd(int version, String key, Collection<KeyValueTuple<Serializable, Double>> tuples);

    /**
     * 从zSet中删除指定元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long zSetRemove(int version, String key, Serializable... values);

    /**
     * 保证原子性操作的情况下对元素的分数进行加操作
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @param delta   delta
     * @return
     */
    Double zSetIncrementScore(int version, String key, Serializable value, double delta);

    /**
     * 获取zSet中指定元素的排名
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long zSetRank(int version, String key, Serializable value);

    /**
     * 获取zSet中指定元素的反向排名
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long zSetReverseRank(int version, String key, Serializable value);

    /**
     * 获取zSet中指定排名范围的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param start         start
     * @param end           end
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T zSetRange(int version, String key, long start, long end, TypeReference<T> typeReference);

    /**
     * 获取zSet中指定排名范围的元素和分数
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param start         start
     * @param end           end
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set<KeyValueTuple<K, Double>>, K> T zSetRangeWithScores(int version, String key, long start, long end,
                                                                       TypeReference<T> typeReference);


    /**
     * 获取zSet中指定分数范围内的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param min           min
     * @param max           max
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T zSetRangeByScore(int version, String key, double min, double max, TypeReference<T> typeReference);

    /**
     * 获取zSet中指定分数范围内的元素和分数
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param min           min
     * @param max           max
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set<KeyValueTuple<K, Double>>, K> T zSetRangeByScoreWithScores(int version, String key, double min,
                                                                              double max, TypeReference<T> typeReference);

    /**
     * 获取指定反向排名范围内的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param start         start
     * @param end           end
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T zSetReverseRange(int version, String key, long start, long end, TypeReference<T> typeReference);

    /**
     * 获取指定反向排名范围内的元素和分数
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param start         start
     * @param end           end
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set<KeyValueTuple<K, Double>>, K> T zSetReverseRangeWithScores(int version, String key, long start,
                                                                              long end, TypeReference<T> typeReference);

    /**
     * 获取指定分数范围内的反向排名后的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param min           min
     * @param max           max
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T zSetReverseRangeByScore(int version, String key, double min, double max, TypeReference<T> typeReference);

    /**
     * 获取指定分数范围内的反向排名后的元素和分数
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param min           min
     * @param max           max
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set<KeyValueTuple<K, Double>>, K> T zSetReverseRangeByScoreWithScores(int version, String key,
                                                                                     double min, double max,
                                                                                     TypeReference<T> typeReference);

    /**
     * 获取指定分数范围内的元素个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param min     min
     * @param max     max
     * @return
     */
    Long zSetCount(int version, String key, double min, double max);

    /**
     * 获取zSet的元素个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @return
     */
    Long zSetSize(int version, String key);

    /**
     * 获取指定元素的分数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Double zSetScore(int version, String key, Serializable value);

    /**
     * 移除指定排名范围的元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param start   start
     * @param end     end
     * @return
     */
    Long zSetRemoveRange(int version, String key, long start, long end);

    /**
     * 移除指定分数范围的元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param min     min
     * @param max     max
     * @return
     */
    Long zSetRemoveRangeByScore(int version, String key, double min, double max);

    /**
     * 两个zSet取并集后存到目标zSet中
     * 默认情况下，一个元素的结果分数是它在它存在的排序集中的分数的总和。
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey     otherKey
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long zSetUnionAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey);

    /**
     * 指定zSet与多个个zSet取并集后存到目标zSet中
     * 默认情况下，一个元素的结果分数是它在它存在的排序集中的分数的总和。
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                           String destKey);

    /**
     * 合并方式
     */
    enum Aggregate {
        /**
         * 相加
         */
        SUM(RedisZSetCommands.Aggregate.SUM),

        /**
         * 取最小值
         */
        MIN(RedisZSetCommands.Aggregate.MIN),

        /**
         * 取最大值
         */
        MAX(RedisZSetCommands.Aggregate.MAX);

        private RedisZSetCommands.Aggregate aggregate;

        Aggregate(RedisZSetCommands.Aggregate aggregate) {
            this.aggregate = aggregate;
        }

        public RedisZSetCommands.Aggregate getAggregate() {
            return aggregate;
        }
    }

    /**
     * 指定分数合并方式的zSet与多个个zSet取并集后存到目标zSet中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @param aggregate    aggregate
     * @return
     */
    Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                           String destKey, Aggregate aggregate);

    /**
     * 指定分数合并方式和乘积因子的的zSet与多个个zSet取并集后存到目标zSet中
     * 一个元素的结果分数是它在它存在的排序集中的分数分别乘以权重后再根据合并类型进行计算
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @param aggregate    aggregate
     * @param weights      weights
     * @return
     */
    Long zSetUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                           String destKey, Aggregate aggregate, List<Double> weights);

    /**
     * 两个zSet取交集后存到目标zSet中
     * 默认情况下，一个元素的结果分数是它在它存在的排序集中的分数的总和。
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey     otherKey
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long zSetIntersectAndStore(int version, String key, int otherVersion, String otherKey, int destVersion,
                               String destKey);

    /**
     * 指定zSet与多个个zSet取交集后存到目标zSet中
     * 默认情况下，一个元素的结果分数是它在它存在的排序集中的分数的总和。
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                               String destKey);

    /**
     * 指定分数合并方式的zSet与多个个zSet取交集后存到目标zSet中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @param aggregate    aggregate
     * @return
     */
    Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                               String destKey, Aggregate aggregate);

    /**
     * 指定分数合并方式和乘积因子的的zSet与多个个zSet取交集后存到目标zSet中
     * 一个元素的结果分数是它在它存在的排序集中的分数分别乘以权重后再根据合并类型进行计算
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @param aggregate    aggregate
     * @param weights      weights
     * @return
     */
    Long zSetIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                               String destKey, Aggregate aggregate, List<Double> weights);


    /**
     * 往set中添加元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long setAdd(int version, String key, Serializable... values);

    /**
     * 从set中删除元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long setRemove(int version, String key, Serializable... values);

    /**
     * 随机移除一个元素并返回该元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T setPop(int version, String key, TypeReference<T> typeReference);

    /**
     * 随机移除多个元素并返回元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param count         count
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends List> T setPop(int version, String key, long count, TypeReference<T> typeReference);

    /**
     * 将指定元素移动到另外一个set中
     *
     * @param version     版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key         key
     * @param value       value
     * @param destVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey     destKey
     * @return
     */
    Boolean setMove(int version, String key, Serializable value, int destVersion, String destKey);

    /**
     * 获取set的元素个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key
     * @return
     */
    Long setSize(int version, String key);

    /**
     * 判断某元素是否是set的成员
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Boolean setIsMember(int version, String key, Serializable value);

    /**
     * 指定set分别和给定set取交集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey      otherKey
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setIntersect(int version, String key, int otherVersion, String otherKey, TypeReference<T> typeReference);

    /**
     * 指定set分别和给定set取交集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys     otherKeys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setIntersect(int version, String key, int otherVersion, Collection<String> otherKeys,
                                   TypeReference<T> typeReference);

    /**
     * 给定set取交集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys          keys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setIntersect(int version, Collection<String> keys, TypeReference<T> typeReference);

    /**
     * 指定set分别和给定set取交集并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey     otherKey
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setIntersectAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey);

    /**
     * 指定set分别和给定set取交集并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setIntersectAndStore(int version, String key, int otherVersion, Collection<String> otherKeys,
                              int destVersion, String destKey);

    /**
     * 给定set取交集并存储到另外的set中
     *
     * @param version     版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys        keys
     * @param destVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey     destKey
     * @return
     */
    Long setIntersectAndStore(int version, Collection<String> keys, int destVersion, String destKey);

    /**
     * 指定set分别和给定set取并集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey      otherKey
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setUnion(int version, String key, int otherVersion, String otherKey, TypeReference<T> typeReference);

    /**
     * 指定set分别和给定set取并集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys     otherKeys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setUnion(int version, String key, int otherVersion, Collection<String> otherKeys,
                               TypeReference<T> typeReference);

    /**
     * 给定set取并集
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys          keys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setUnion(int version, Collection<String> keys, TypeReference<T> typeReference);

    /**
     * 指定set分别和给定set取并集并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey     otherKey
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setUnionAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey);

    /**
     * 指定set分别和给定set取并集并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setUnionAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                          String destKey);

    /**
     * 给定set取并集并存储到另外的set中
     *
     * @param version     版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys        keys
     * @param destVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey     destKey
     * @return
     */
    Long setUnionAndStore(int version, Collection<String> keys, int destVersion, String destKey);

    /**
     * 获取给定的两个set中所有不相同的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey      otherKey
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setDifference(int version, String key, int otherVersion, String otherKey,
                                    TypeReference<T> typeReference);

    /**
     * 指定set分别和给定set取不相同的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param otherVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys     otherKeys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setDifference(int version, String key, int otherVersion, Collection<String> otherKeys,
                                    TypeReference<T> typeReference);

    /**
     * 给定set取不相同的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys          keys
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setDifference(int version, Collection<String> keys, TypeReference<T> typeReference);

    /**
     * 指定set给定set取不相同的元素并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKey     otherKey
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setDifferenceAndStore(int version, String key, int otherVersion, String otherKey, int destVersion, String destKey);

    /**
     * 指定set分别和给定set取不相同的元素并存储到另外的set中
     *
     * @param version      版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key          key
     * @param otherVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param otherKeys    otherKeys
     * @param destVersion  版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey      destKey
     * @return
     */
    Long setDifferenceAndStore(int version, String key, int otherVersion, Collection<String> otherKeys, int destVersion,
                               String destKey);

    /**
     * 给定set取不相同的元素并存储到另外的set中
     *
     * @param version     版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param keys        keys
     * @param destVersion 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey     destKey
     * @return
     */
    Long setDifferenceAndStore(int version, Collection<String> keys, int destVersion, String destKey);

    /**
     * 获取set中所有元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends Set> T setMembers(int version, String key, TypeReference<T> typeReference);

    /**
     * 获取给定排序范围内的所有元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param start         start
     * @param end           end
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T extends List> T listRange(int version, String key, long start, long end, TypeReference<T> typeReference);

    /**
     * 移除给定排序范围外的所有元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param start   start
     * @param end     end
     */
    void listTrim(int version, String key, long start, long end);

    /**
     * 获取list的元素个数
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key
     * @return
     */
    Long listSize(int version, String key);

    /**
     * 从左边添加一个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long listLeftPush(int version, String key, Serializable value);

    /**
     * 从左边添加多个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long listLeftPushAll(int version, String key, Serializable... values);

    /**
     * 从左边添加多个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long listLeftPushAll(int version, String key, Collection<Serializable> values);

    /**
     * 当给定元素不存在时从左边添加一个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long listLeftPushIfPresent(int version, String key, Serializable value);

    /**
     * 从给定元素的左边添加元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param pivot   pivot
     * @param value   value
     * @return
     */
    Long listLeftPush(int version, String key, Serializable pivot, Serializable value);

    /**
     * 从右边添加一个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long listRightPush(int version, String key, Serializable value);

    /**
     * 从右边添加多个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long listRightPushAll(int version, String key, Serializable... values);

    /**
     * 从右边添加多个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param values  values
     * @return
     */
    Long listRightPushAll(int version, String key, Collection<Serializable> values);

    /**
     * 当给定元素不存在时从右边添加一个元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param value   value
     * @return
     */
    Long listRightPushIfPresent(int version, String key, Serializable value);

    /**
     * 从给定元素的右边添加元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param pivot   pivot
     * @param value   value
     * @return
     */
    Long listRightPush(int version, String key, Serializable pivot, Serializable value);

    /**
     * 将指定元素插入指定位置
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param index   index
     * @param value   value
     */
    void listSet(int version, String key, long index, Serializable value);

    /**
     * 删除指定元素前的给定个数的元素
     *
     * @param version 版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key     key
     * @param count   count
     * @param value   value
     * @return
     */
    Long listRemove(int version, String key, long count, Serializable value);

    /**
     * 获取指定序号的元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param index         index
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listIndex(int version, String key, long index, TypeReference<T> typeReference);

    /**
     * 移除并返回最左边元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listLeftPop(int version, String key, TypeReference<T> typeReference);

    /**
     * 阻塞移除并返回最左边元素直到超时
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param timeout       timeout
     * @param unit          unit
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listLeftPop(int version, String key, long timeout, TimeUnit unit, TypeReference<T> typeReference);

    /**
     * 移除并返回最右边元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listRightPop(int version, String key, TypeReference<T> typeReference);

    /**
     * 阻塞移除并返回最右边元素直到超时
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param timeout       timeout
     * @param unit          unit
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listRightPop(int version, String key, long timeout, TimeUnit unit, TypeReference<T> typeReference);

    /**
     * 从最右边移除并添加到目标list中，最后返回该元素
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param destVersion   版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey       destKey
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listRightPopAndLeftPush(int version, String key, int destVersion, String destKey, TypeReference<T> typeReference);

    /**
     * 阻塞并从最右边移除，添加到目标list中，最后返回该元素，直到超时
     *
     * @param version       版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param key           key
     * @param destVersion   版本号，用于灰度发布场景，即只要新版本的缓存内容和旧版本不一致，则需要升级版本号，通常在原有的基础上加1
     * @param destKey       destKey
     * @param timeout       timeout
     * @param unit          unit
     * @param typeReference typeReference
     * @param <T>           反序列化泛型
     * @return
     */
    <T> T listRightPopAndLeftPush(int version, String key, int destVersion, String destKey, long timeout, TimeUnit unit,
                                  TypeReference<T> typeReference);
}