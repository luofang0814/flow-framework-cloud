package com.flow.framework.cache.util;

import com.flow.framework.cache.properties.component.LocalCacheConfigProperties;
import com.flow.framework.common.util.verify.VerifyUtil;

import java.util.List;

/**
 * 本地缓存工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/12
 */
public class LocalCacheConfigUtil {

    public static LocalCacheConfigProperties getLocalCacheConfigProperties(String cachePrefix, List<LocalCacheConfigProperties> localCacheConfigs){
        if (VerifyUtil.isEmpty(localCacheConfigs)){
            return null;
        }
        for (LocalCacheConfigProperties localCacheConfigProperties : localCacheConfigs) {
            String tempCacheKeyPrefix = localCacheConfigProperties.getCacheKeyPrefix();
            if (cachePrefix.equals(tempCacheKeyPrefix)) {
                return localCacheConfigProperties;
            }
        }
        return null;
    }
}