package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.core.validation.annotation.AssertTrueCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * AssertTrue 校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class AssertTrueValidator extends BaseValidator<AssertTrueCustomization, Boolean> {

    private static final String ERROR_MSG = "expect value is true";

    private AssertTrueCustomization assertTrueCustomization;

    @Override
    public void initialize(AssertTrueCustomization assertTrueCustomization) {
        this.assertTrueCustomization = assertTrueCustomization;
    }

    @Override
    public boolean isValid(Boolean value, ConstraintValidatorContext context) {
        if (null == value) {
            return true;
        }

        if (!value) {
            super.logError(context, false, ERROR_MSG);
            throw new ParamCheckedException(assertTrueCustomization.errorCode());
        }
        return true;
    }
}
