package com.flow.framework.core.properties;

import lombok.Data;

/**
 * 框架核心配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Data
public class FrameworkCoreConfigProperties {

    /**
     * 是否开启租户支持
     */
    private boolean enableTenantSupport = true;

    /**
     * 是否开启环境编码支持
     */
    private boolean enableEnvCodeSupport = true;

    /**
     * 环境编码，可用于区分不同环境，系统自动调起异步任务时，需要使用该环境变量去查询当前系统版本，用于对特定系统版本的数据进行处理
     */
    private String envCode;

    /**
     * 文件缓存路径，参考FileHelper
     */
    private String fileCachePath;
}
