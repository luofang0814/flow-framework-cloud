package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.core.validation.annotation.MinCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * Min 校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class MinValidator extends BaseValidator<MinCustomization, Number> {

    private MinCustomization minCustomization;

    private String errorMsg;

    @Override
    public void initialize(MinCustomization minCustomization) {
        this.minCustomization = minCustomization;
        this.errorMsg = "value less than" + minCustomization.value();
    }

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {

        if (null == minCustomization) {
            return true;
        }

        if (minCustomization.containsEquals()) {
            if (value.longValue() < minCustomization.value()) {
                super.logError(context, value, errorMsg);
                throw new ParamCheckedException(minCustomization.errorCode());
            }
        } else {
            if (value.longValue() <= minCustomization.value()) {
                super.logError(context, value, errorMsg);
                throw new ParamCheckedException(minCustomization.errorCode());
            }
        }
        return true;
    }
}
