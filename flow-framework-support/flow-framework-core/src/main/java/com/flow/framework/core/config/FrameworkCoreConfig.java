package com.flow.framework.core.config;

import com.flow.framework.core.helper.file.FileHelper;
import com.flow.framework.core.helper.response.ResponseHelper;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.service.properties.impl.SystemConfigPropertiesServiceImpl;
import com.flow.framework.core.toolkit.IdentifierGeneratorSequence;
import com.flow.framework.core.service.system.health.IServiceWorkerNameInitService;
import com.flow.framework.core.service.system.health.impl.ServiceStatusHealthCheckServiceImpl;
import com.flow.framework.core.system.helper.AsyncHelper;
import com.flow.framework.core.system.helper.ServiceWorkerHelper;
import com.flow.framework.core.system.helper.SystemHealthHelper;
import com.flow.framework.core.system.initialization.SystemInitialization;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * 框架核心配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class FrameworkCoreConfig {

    @Bean
    @ConditionalOnMissingBean
    SystemInitialization systemInitialization() {
        return new SystemInitialization();
    }

    @Bean
    @ConditionalOnMissingBean
    SystemConfigPropertiesServiceImpl systemConfigPropertiesService(Environment environment) {
        return new SystemConfigPropertiesServiceImpl(environment);
    }

    @Bean
    @ConditionalOnMissingBean
    ServiceWorkerHelper serviceWorkerHelper(ISystemConfigPropertiesService systemConfigPropertiesService) {
        return new ServiceWorkerHelper(systemConfigPropertiesService);
    }

    @Bean
    @ConditionalOnMissingBean
    IdentifierGeneratorSequence identifierGeneratorSequence(ServiceWorkerHelper serviceWorkerHelper) {
        return new IdentifierGeneratorSequence(serviceWorkerHelper.getWorkerId(), serviceWorkerHelper.getDateCenterId());
    }

    @Bean
    @ConditionalOnMissingBean
    ResponseHelper responseHelper() {
        return new ResponseHelper();
    }

    @Bean
    @ConditionalOnMissingBean
    ServiceStatusHealthCheckServiceImpl serviceStatusHealthCheckService() {
        return new ServiceStatusHealthCheckServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    AsyncHelper asyncHelper(ISystemConfigPropertiesService systemConfigPropertiesService) {
        return new AsyncHelper(systemConfigPropertiesService);
    }

    @Bean
    @ConditionalOnMissingBean
    SystemHealthHelper systemHealthHelper(ISystemConfigPropertiesService systemConfigPropertiesService,
                                          @Autowired(required = false) IServiceWorkerNameInitService serviceWorkerNameInitService,
                                          ServiceWorkerHelper serviceWorkerHelper) {
        return new SystemHealthHelper(systemConfigPropertiesService, serviceWorkerNameInitService, serviceWorkerHelper);
    }

    @Bean
    @ConditionalOnMissingBean
    FileHelper fileHelper() {
        return new FileHelper();
    }
}
