package com.flow.framework.core.util;

import com.flow.framework.common.util.verify.VerifyUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * http content type工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
public class HttpContentTypeUtil {

    private static final List<String> TEXT_CONTENT_TYPE = new ArrayList<String>() {{
        add("application/atom+xml");
        add("application/x-www-form-urlencoded");
        add("application/json");
        add("application/problem+json");
        add("application/problem+xml");
        add("application/rss+xml");
        add("application/soap+xml");
        add("application/svg+xml");
        add("application/xhtml+xml");
        add("application/xml");
        add("text/html");
        add("text/plain");
        add("text/markdown");
        add("text/xml");
    }};

    /**
     * contentType是否为文本型body
     *
     * @param contentType contentType
     * @return 是否为文本型body
     */
    public static boolean isTextBody(String contentType) {
        if (VerifyUtil.isEmpty(contentType)) {
            return false;
        }
        for (String textContentType : TEXT_CONTENT_TYPE) {
            if (contentType.contains(textContentType)) {
                return true;
            }
        }
        return false;
    }
}