package com.flow.framework.core.util;

import com.flow.framework.common.util.collection.CollectionUtil;
import com.flow.framework.core.pojo.vo.PageModuleVo;
import com.flow.framework.core.pojo.vo.PageVo;

import java.util.function.Function;

/**
 * page vo工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public class PageVoUtil {

    /**
     * 将分页数据转化为远程调用value object的分页对象
     *
     * @param pageVo      pageVo
     * @param voConverter voConverter
     * @param <T>         T
     * @param <E>         E
     * @return
     */
    public static <T, E> PageModuleVo<T> convertToPageModuleVo(PageVo<E> pageVo, Function<E, T> voConverter) {
        PageModuleVo<T> pageModuleVo = new PageModuleVo<>();
        pageModuleVo.setCurrent(pageVo.getCurrent());
        pageModuleVo.setTotal(pageVo.getTotal());
        pageModuleVo.setSize(pageVo.getSize());
        pageModuleVo.setPages(pageVo.getPages());
        pageModuleVo.setRecords(CollectionUtil.convertToList(pageVo.getRecords(), voConverter));
        return pageModuleVo;
    }

    /**
     * 将远程调用的分页数据转化为value object的分页对象
     *
     * @param pageModuleVo pageModuleVo
     * @param voConverter  voConverter
     * @param <T>          T
     * @param <E>          E
     * @return
     */
    public static <T, E> PageVo<T> convertToPageVo(PageModuleVo<E> pageModuleVo, Function<E, T> voConverter) {
        PageVo<T> pageVo = new PageVo<>();
        pageVo.setCurrent(pageModuleVo.getCurrent());
        pageVo.setTotal(pageModuleVo.getTotal());
        pageVo.setSize(pageModuleVo.getSize());
        pageVo.setPages(pageModuleVo.getPages());
        pageVo.setRecords(CollectionUtil.convertToList(pageModuleVo.getRecords(), voConverter));
        return pageVo;
    }
}