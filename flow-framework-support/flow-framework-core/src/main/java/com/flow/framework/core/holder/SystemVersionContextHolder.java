package com.flow.framework.core.holder;

import lombok.extern.slf4j.Slf4j;

/**
 * 环境版本上下文持有者
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
@Slf4j
public class SystemVersionContextHolder {

    private static final ThreadLocal<Long> CONTEXT_HOLDER_CONTENT_CACHE_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 获取当前系统版本号
     *
     * @return 当前系统版本号
     */
    public static Long getCurrentSystemVersion() {
        return CONTEXT_HOLDER_CONTENT_CACHE_THREAD_LOCAL.get();
    }

    /**
     * 设置当前系统版本号
     *
     * @param systemVersion 当前系统版本号
     */
    public static void setCurrentSystemVersion(Long systemVersion) {
        CONTEXT_HOLDER_CONTENT_CACHE_THREAD_LOCAL.set(systemVersion);
    }

    /**
     * 清空系统版本号
     */
    public static void clear() {
        CONTEXT_HOLDER_CONTENT_CACHE_THREAD_LOCAL.remove();
    }
}
