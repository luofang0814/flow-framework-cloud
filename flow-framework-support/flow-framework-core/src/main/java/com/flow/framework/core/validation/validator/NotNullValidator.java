package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.core.validation.annotation.NotNullCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * not null校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class NotNullValidator extends BaseValidator<NotNullCustomization, Object> {

    private static final String ERROR_MSG = "expect value not null";

    private NotNullCustomization notNullCustomization;

    @Override
    public void initialize(NotNullCustomization notNullCustomization) {
        this.notNullCustomization = notNullCustomization;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (null == value) {
            super.logError(context, null, ERROR_MSG);
            throw new ParamCheckedException(notNullCustomization.errorCode());
        }
        return true;
    }
}
