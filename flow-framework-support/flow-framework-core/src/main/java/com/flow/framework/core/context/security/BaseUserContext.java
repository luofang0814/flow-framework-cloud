package com.flow.framework.core.context.security;

import lombok.Data;

/**
 * 用户基础上下文
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Data
public class BaseUserContext {

    /**
     * 是否为管理端客户
     */
    private boolean isManageUser;

    /**
     * 主租户，当前暂未使用，后续如果需要切换成物理隔离的多租户模式，则可以使用该字段，使用动态DynamicRoutingDataSource
     */
    private String tenantMainId;

    /**
     * 租户
     */
    private String tenantId;

    /**
     * 用户来源id，如果为0则为当前系统用户，否则可能来源于其他系统，该id需要在关键业务中进行冗余，方便快速数据统计
     */
    private String userSrcId;

    /**
     * 用户主账号id
     */
    private String userMainId;

    /**
     * 用户主账号名称
     */
    private String userMainName;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 是否国际化
     */
    private boolean i18Required;
}
