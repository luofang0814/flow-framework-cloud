package com.flow.framework.core.pojo.dto.base.module;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 模块查询调用基础请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseQueryModuleDto extends BaseModuleDto {
}
