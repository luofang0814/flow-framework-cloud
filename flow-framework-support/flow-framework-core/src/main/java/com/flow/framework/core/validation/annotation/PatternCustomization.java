package com.flow.framework.core.validation.annotation;

import com.flow.framework.core.validation.validator.PatternValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 正则校验注解
 * 校验目标： String
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {PatternValidator.class})
public @interface PatternCustomization {

    long errorCode();

    String regex();

    /**
     * 此参数是代表匹配标志的整数，它是一个位掩码，可以包括Pattern下的CASE_INSENSITIVE，MULTILINE，DOTALL，UNICODE_CASE，
     * CANON_EQ，UNIX_LINES，LITERAL，UNICODE_CHARACTER_CLASS和COMMENTS。
     *
     * @return flag
     */
    int flag() default 0;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String message() default "参数错误";
}
