package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.core.validation.annotation.NullCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * null校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class NullValidator extends BaseValidator<NullCustomization, Object> {

    private static final String ERROR_MSG = "expect value is null";

    private NullCustomization nullCustomizationAnnotation;

    @Override
    public void initialize(NullCustomization nullCustomizationAnnotation) {
        this.nullCustomizationAnnotation = nullCustomizationAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (null != value) {
            super.logError(context, value, ERROR_MSG);
            throw new ParamCheckedException(nullCustomizationAnnotation.errorCode());
        }
        return true;
    }
}
