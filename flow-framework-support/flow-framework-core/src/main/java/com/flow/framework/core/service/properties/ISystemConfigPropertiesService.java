package com.flow.framework.core.service.properties;

/**
 * 系统配置服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
public interface ISystemConfigPropertiesService {
    /**
     * 获取配置值
     *
     * @param key          配置key
     * @param defaultValue 默认值
     * @return
     */
    String getConfigValue(String key, String defaultValue);

    /**
     * 获取配置值
     *
     * @param key 配置key
     * @return
     */
    String getConfigValue(String key);

    /**
     * 获取placeHolder值
     *
     * @param placeHolder 替换placeHolder
     * @return
     */
    String resolvePlaceholders(String placeHolder);
}
