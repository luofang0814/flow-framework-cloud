package com.flow.framework.core.service.system.health;

/**
 * 微服务节点名称初始化服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public interface IServiceWorkerNameInitService {

    /**
     * 获取节点名称，即在健康检查过程中，微服务可以自定义微服务节点名称
     *
     * @param dateCenterId 数据中心id
     * @param workerId     节点中心id
     * @return 节点名称
     */
    String getWorkerName(long dateCenterId, long workerId);
}