package com.flow.framework.core.pojo.dto.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基础分页查询请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BasePageQueryDto extends BaseQueryDto {

    /**
     * 当前查询页
     */
    @ApiModelProperty(value = "当前页", required = true, example = "1")
    private int current = 1;

    /**
     * 分页大小
     */
    @ApiModelProperty(value = "分页大小", required = true, example = "10")
    private int size = 10;
}
