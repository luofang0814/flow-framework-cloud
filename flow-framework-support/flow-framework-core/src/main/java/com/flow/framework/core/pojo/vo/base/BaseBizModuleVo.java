package com.flow.framework.core.pojo.vo.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 远程调用基础view Object对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public abstract class BaseBizModuleVo extends BaseSystemVersionModuleVo {
    /**
     * 租户id，由脚手架自动设置值，不可在业务代码中设置相关的值
     */
    private String tenantId;
}
