package com.flow.framework.core.helper.response;

import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.pojo.bo.InternationalBo;
import com.flow.framework.core.service.response.IResponseI18nService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import com.flow.framework.core.util.ProxyUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 响应辅助类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Slf4j
public class ResponseHelper implements ISystemLifecycleListener {

    private static IResponseI18nService responseService;

    /**
     * 根据国际化编码和参数格式化描述
     *
     * @param code   国际化编码
     * @param params 国际化参数
     * @return
     */
    public static InternationalBo i18nResponse(String code, List<String> params) {
        if (VerifyUtil.isEmpty(responseService)) {
            log.warn("international error, response service can't found");
            return new InternationalBo(null, code, null);
        }
        return responseService.i18n(code, params);
    }

    /**
     * 获取当前微服务编码
     *
     * @return
     */
    public static String getCurrentSystemCode() {
        if (VerifyUtil.isEmpty(responseService)) {
            log.error("can't found self system code");
            return "0";
        }
        return responseService.getCurrentSystemCode();
    }

    @Override
    public void onStartUp() {
        IResponseI18nService bean = ProxyUtil.getProxiedQuietly(IResponseI18nService.class);
        if (null == bean) {
            log.warn("response service can't found.");
            return;
        }
        responseService = bean;
    }
}
