package com.flow.framework.core.helper.file;

import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.constant.FrameworkCoreConstant;
import com.flow.framework.core.properties.FrameworkCoreConfigProperties;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import com.flow.framework.core.util.ProxyUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 文件辅助类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Slf4j
public class FileHelper implements ISystemLifecycleListener {

    private static final String LINUX_TEMP_PATH = "/opt/flow/file/temp";

    private static final String WINDOWS_TEMP_PATH = "C:/opt/flow/file/temp";

    private static FrameworkCoreConfigProperties frameworkCoreConfigProperties;

    /**
     * 获取文件缓存路径
     *
     * @return 文件缓存路径
     */
    public static String getFileCachePath() {
        if (!VerifyUtil.isEmpty(frameworkCoreConfigProperties)) {
            String fileCachePath = frameworkCoreConfigProperties.getFileCachePath();
            if (!VerifyUtil.isEmpty(fileCachePath)) {
                return fileCachePath;
            }
        }
        String osName = System.getProperty("os.name");
        if (VerifyUtil.isEmpty(osName)) {
            throw new CheckedException(SystemErrorCode.PARAMS_ERROR, "can't find operator system name.");
        }
        if (osName.toUpperCase().contains(FrameworkCoreConstant.WINDOWS_OS)) {
            return WINDOWS_TEMP_PATH;
        }
        return LINUX_TEMP_PATH;
    }

    @Override
    public void onStartUp() {
        FrameworkCoreConfigProperties bean = ProxyUtil.getProxiedQuietly(FrameworkCoreConfigProperties.class);
        if (null == bean) {
            log.warn("framework core config can't found.");
            return;
        }
        frameworkCoreConfigProperties = bean;
    }
}
