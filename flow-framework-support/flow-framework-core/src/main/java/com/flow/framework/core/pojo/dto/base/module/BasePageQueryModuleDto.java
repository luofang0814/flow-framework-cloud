package com.flow.framework.core.pojo.dto.base.module;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 模块调用分页查询请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public abstract class BasePageQueryModuleDto extends BaseQueryModuleDto {

    /**
     * 当前查询页
     */
    private int current = 1;

    /**
     * 分页大小
     */
    private int size = 10;
}
