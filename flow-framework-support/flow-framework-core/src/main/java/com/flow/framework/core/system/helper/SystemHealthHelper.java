package com.flow.framework.core.system.helper;

import com.flow.framework.common.pojo.vo.system.ServiceHealthVo;
import com.flow.framework.core.constant.FrameworkCoreConstant;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.service.system.health.IServiceWorkerNameInitService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 系统健康检查帮助类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
@Slf4j
public class SystemHealthHelper implements ISystemLifecycleListener {

    private final ISystemConfigPropertiesService systemConfigPropertiesService;

    private final IServiceWorkerNameInitService serviceWorkerNameInitService;

    private final ServiceWorkerHelper serviceWorkerHelper;

    public SystemHealthHelper(ISystemConfigPropertiesService systemConfigPropertiesService,
                              @Autowired(required = false) IServiceWorkerNameInitService serviceWorkerNameInitService,
                              ServiceWorkerHelper serviceWorkerHelper) {
        this.systemConfigPropertiesService = systemConfigPropertiesService;
        this.serviceWorkerNameInitService = serviceWorkerNameInitService;
        this.serviceWorkerHelper = serviceWorkerHelper;
    }

    /**
     * 微服务名称
     */
    private static String serviceName;

    /**
     * 微服务端口
     */
    private static String servicePort;

    /**
     * 微服务节点名称
     */
    private static String workerName;

    /**
     * 微服务节点ip
     */
    private static String workerIp;

    /**
     * @inheritDoc
     */
    @Override
    public void onStartUp() {
        serviceName = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_NAME_KEY);
        servicePort = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_CLIENT_PORT_KEY);
        workerIp = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_CLIENT_IP_KEY);

        // 如果微服务节点名称初始化服务不存在，则使用微服务的数据中心ID加节点ID
        if (null == serviceWorkerNameInitService) {
            workerName = serviceName + ":" + serviceWorkerHelper.getDateCenterId() + ":" + serviceWorkerHelper.getWorkerId();
        } else {
            workerName = serviceName + ":" + serviceWorkerNameInitService.getWorkerName(serviceWorkerHelper.getDateCenterId(), serviceWorkerHelper.getWorkerId());
        }
    }

    /**
     * 构造服务不健康对象
     *
     * @param systemHealthCode 健康检查编码
     * @param extMsg           描述
     * @param params           健康检查
     * @return 健康检查VO
     */
    public static ServiceHealthVo unhealthy(int systemHealthCode, String extMsg, List<String> params) {
        return new ServiceHealthVo(serviceName, servicePort, workerName, workerIp, String.valueOf(systemHealthCode),
                false, null, extMsg, params);
    }

    /**
     * 构造服务健康对象
     *
     * @param systemHealthCode 健康检查编码
     * @return 健康检查VO
     */
    public static ServiceHealthVo healthy(int systemHealthCode) {
        return new ServiceHealthVo(serviceName, servicePort, workerName, workerIp, String.valueOf(systemHealthCode),
                true, null, null, null);
    }
}
