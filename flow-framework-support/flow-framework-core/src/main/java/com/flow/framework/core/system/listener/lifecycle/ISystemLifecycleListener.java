package com.flow.framework.core.system.listener.lifecycle;

/**
 * 系统生命周期监听器 但需要将实现类交给spring管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public interface ISystemLifecycleListener {

    /**
     * 系统启动时初始化
     */
    default void onStartUp() {

    }

    /**
     * 系统启动后手动上线
     */
    default void manualOnline() {

    }

    /**
     * 系统刷新，对应使用配置中心场景
     */
    default void onRefresh() {

    }

    /**
     * 关闭系统前
     */
    default void beforeShutdown() {

    }

    /**
     * 获取初始化时的顺序，越小越优先
     *
     * @return 排序优先级，默认优先级最小
     */
    default int getOrder() {
        return Integer.MAX_VALUE;
    }
}
