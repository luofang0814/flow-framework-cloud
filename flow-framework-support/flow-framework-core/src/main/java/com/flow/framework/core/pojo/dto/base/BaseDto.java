package com.flow.framework.core.pojo.dto.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 基础调用请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/25
 */
@Data
@EqualsAndHashCode
public abstract class BaseDto implements Serializable {
}
