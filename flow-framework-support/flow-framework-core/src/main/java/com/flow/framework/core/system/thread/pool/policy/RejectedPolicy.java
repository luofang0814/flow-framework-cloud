package com.flow.framework.core.system.thread.pool.policy;

import lombok.extern.slf4j.Slf4j;

/**
 * 线程池满载时提交任务的处理策略
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
@Slf4j
public enum RejectedPolicy {

    /**
     * 丢弃最老的任务，有可能丢弃很多个任务
     */
    DISCARD_OLDEST_POLICY("A handler for rejected tasks that discards the oldest."),

    /**
     * 丢弃提交的任务
     */
    DISCARD_POLICY(" A handler for rejected tasks that silently discards the rejected task."),

    /**
     * 抛出异常
     */
    THROW_EXCEPTION_POLICY("A handler for rejected tasks that throws a CheckedException(SystemErrorCode.SUBMIT_TASK_ERROR)."),

    /**
     * 阻塞调用线程直到任务提交成功
     */
    BLOCK_QUEUE_POLICY("block until submit task success."),

    /**
     * 如果任务队列已经满了，则由调用者线程自己执行提交的任务
     */
    CALLER_RUN_POLICY("caller run submit task."),
    ;

    RejectedPolicy(String description) {
        this.description = description;

    }

    private String description;

    public String getDescription() {
        return description;
    }
}
