package com.flow.framework.core.system.proxy;

import org.springframework.stereotype.Service;

/**
 * 标记需要被代理的对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
@Service
public interface IProxy<T> {

    /**
     * 获取代理的类型，即实际使用时会根据该类型获取被代理的对象
     *
     * @return 类型
     */
    T[] getTypes();
}