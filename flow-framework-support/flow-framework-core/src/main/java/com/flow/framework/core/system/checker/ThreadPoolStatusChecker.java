package com.flow.framework.core.system.checker;

import com.flow.framework.core.system.thread.pool.task.BaseCallable;
import com.flow.framework.core.system.thread.pool.task.BaseRunnable;

import java.util.concurrent.Future;

/**
 * 线程池监听接口
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public interface ThreadPoolStatusChecker extends SystemStatusChecker {

    /**
     * 提交任务后会触发该方法
     *
     * @param future       baseRunnable提交后对应的future
     * @param baseRunnable baseRunnable
     */
    void onSubmit(Future<?> future, BaseRunnable baseRunnable);

    /**
     * 提交任务后会触发该方法
     *
     * @param future       baseCallable提交后对应的future
     * @param baseCallable baseCallable
     * @param <V>          baseCallable返回的对象
     */
    <V> void onSubmit(Future<?> future, BaseCallable<V> baseCallable);
}
