package com.flow.framework.core.constant;

/**
 * 核心常量定义
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public final class FrameworkCoreConstant {

    /**
     * windows操作系统字符大写
     */
    public static final String WINDOWS_OS = "WINDOWS";

    /**
     * 成功
     */
    public static final String SUCCESS_CODE = "0";

    /**
     * 内部RPC传递用户信息的header
     */
    public static final String USER_CONTEXT_HEADER_KEY = "customize_user_context";

    /**
     * 内部RPC传递租户id的header，同时在MQ进行数据交互时也通过该标识传递租户id
     */
    public static final String BIZ_TENANT_ID_HEADER_KEY = "customize_biz_tenant_id";

    /**
     * 请求传递鉴权的header
     */
    public static final String AUTHORIZATION_HEADER_KEY = "customize_authorization";

    /**
     * 微服务的feign调用链中，在header中保存当前微服务名称传递到下一个微服务中
     */
    public static final String GLOBAL_PREVIOUS_APP_KEY = "customization_previous_application";

    /**
     * 微服务名称
     */
    public static final String SERVICE_NAME_KEY = "spring.application.name";

    /**
     * 微服务注册到注册中心时选择的ip
     */
    public static final String SERVICE_CLIENT_IP_KEY = "spring.cloud.client.ip-address";

    /**
     * 微服务绑定的ip，微服务不一定是spring cloud，所以SERVICE_CLIENT_IP_KEY不一定有值
     */
    public static final String SERVICE_BIND_IP_KEY = "server.address";

    /**
     * 微服务激活的变量名称
     */
    public static final String SERVICE_ACTIVE_PROFILES_KEY = "spring.profiles.active";

    /**
     * 微服务运行时的端口
     */
    public static final String SERVICE_CLIENT_PORT_KEY = "server.port";

    /**
     * 微服务响应的框架响应，即RPC调用时在header中的响应的key和请求异常时在请求中的attribute的key
     */
    public static final String FRAMEWORK_RESPONSE_KEY = "customization-framework-response-key";

    /**
     * 项目启动时，需要将数据中心id以运行参数的形式传进来，如：-Dcustomization.framework.data-center-id=0
     * 或将该变量设置成操作系统环境变量，注意：不同节点使用数据中心id加节点id进行区分！！！
     */
    public static final String DATA_CENTER_ID_KEY = "customization.framework.data-center-id";

    /**
     * 项目启动时，需要将节点id以运行参数的形式传进来，如：-Dcustomization.framework.worker-id=0
     * 或将该变量设置成操作系统环境变量，注意：不同节点使用数据中心id加节点id进行区分！！！
     */
    public static final String WORKER_ID_KEY = "customization.framework.worker-id";

    /**
     * 数据中心ID最大值为31
     */
    public static final long DATA_CENTER_ID_MAX_VALUE = 31;

    /**
     * 节点ID最大值为31
     */
    public static final long WORKER_ID_MAX_VALUE = 31;
}
