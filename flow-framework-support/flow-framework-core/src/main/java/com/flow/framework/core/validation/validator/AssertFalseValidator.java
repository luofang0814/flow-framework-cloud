package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.core.validation.annotation.AssertFalseCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * AssertFalse 校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class AssertFalseValidator extends BaseValidator<AssertFalseCustomization, Boolean> {

    private static final String ERROR_MSG = "expect value is false";

    private AssertFalseCustomization assertFalseCustomization;

    @Override
    public void initialize(AssertFalseCustomization assertFalseCustomization) {
        this.assertFalseCustomization = assertFalseCustomization;
    }

    @Override
    public boolean isValid(Boolean value, ConstraintValidatorContext context) {
        if (null == value) {
            return true;
        }

        if (value) {
            super.logError(context, true, ERROR_MSG);
            throw new ParamCheckedException(assertFalseCustomization.errorCode());
        }
        return true;
    }
}
