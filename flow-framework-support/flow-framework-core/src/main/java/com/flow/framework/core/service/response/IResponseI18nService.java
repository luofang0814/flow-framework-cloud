package com.flow.framework.core.service.response;

import com.flow.framework.core.pojo.bo.InternationalBo;

import java.util.List;

/**
 * 提供响应i18n相关的服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
public interface IResponseI18nService {

    /**
     * 国际化
     *
     * @param code   code
     * @param params 国际化参数
     * @return
     */
    InternationalBo i18n(String code, List<String> params);

    /**
     * 获取当前系统编码
     *
     * @return
     */
    String getCurrentSystemCode();
}
