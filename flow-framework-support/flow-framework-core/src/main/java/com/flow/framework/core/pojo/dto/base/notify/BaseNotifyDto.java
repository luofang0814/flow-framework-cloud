package com.flow.framework.core.pojo.dto.base.notify;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 基础通知对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/26
 */
@Data
@EqualsAndHashCode
public abstract class BaseNotifyDto implements Serializable {
}
