package com.flow.framework.core.validation.validator;

import com.flow.framework.common.exception.ParamCheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.framework.core.validation.validator.base.BaseValidator;

import javax.validation.ConstraintValidatorContext;

/**
 * NotEmpty校验器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public class NotEmptyValidator extends BaseValidator<NotEmptyCustomization, Object> {

    private static final String ERROR_MSG = "expect value not empty";

    private NotEmptyCustomization notEmptyCustomization;

    @Override
    public void initialize(NotEmptyCustomization notEmptyCustomization) {
        this.notEmptyCustomization = notEmptyCustomization;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (VerifyUtil.isEmpty(value)) {
            super.logError(context, value, ERROR_MSG);
            throw new ParamCheckedException(notEmptyCustomization.errorCode());
        }
        return true;
    }
}
