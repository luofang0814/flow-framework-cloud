package com.flow.framework.core.validation.annotation;

import com.flow.framework.core.validation.validator.SizeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 容器大小校验注解
 * 校验目标： Map、Collection
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {SizeValidator.class})
public @interface SizeCustomization {

    long errorCode();

    int min() default 0;

    int max();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String message() default "参数错误";
}
