package com.flow.framework.core.validation.annotation;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.core.validation.validator.ImmutableValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 常量约束校验注解
 * 校验目标： Immutable
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Documented
@Constraint(validatedBy = {ImmutableValidator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
public @interface ImmutableCustomization {

    Class<? extends Immutable> immutableInterface();

    long errorCode();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String message() default "参数错误";
}
