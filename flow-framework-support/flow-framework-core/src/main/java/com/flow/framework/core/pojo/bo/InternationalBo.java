package com.flow.framework.core.pojo.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 国际化 biz object
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InternationalBo {

    /**
     * 系统编号
     */
    private String system;

    /**
     * 国际化编码
     */
    private String code;

    /**
     * 国际化结果
     */
    private String international;
}
