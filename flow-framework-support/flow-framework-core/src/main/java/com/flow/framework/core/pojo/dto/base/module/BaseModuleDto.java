package com.flow.framework.core.pojo.dto.base.module;

import lombok.Data;

import java.io.Serializable;

/**
 * 模块调用基础请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
@Data
public abstract class BaseModuleDto implements Serializable {
}
