package com.flow.framework.core.system.thread.pool.bo;

import java.util.Map;
import java.util.concurrent.Future;

/**
 * 监听任务超时缓存的biz object
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public class TaskTimeoutBo {

    /**
     * 任务提交后返回的future
     */
    private Future<?> future;

    /**
     * 提交时间
     */
    private Long startTime;

    /**
     * 超时时间
     */
    private Long timeout;

    /**
     * 任务task对应的clazz
     */
    private Class<?> taskClazz;

    /**
     * 用于存储日志的MDC中的map
     */
    private Map<String, String> mdcContext;

    /**
     * 监听任务超时缓存的biz object构造器
     *
     * @param future     任务提交后返回的future
     * @param startTime  提交时间
     * @param timeout    超时时间
     * @param taskClazz  任务task对应的clazz
     * @param mdcContext 用于存储日志的MDC中的map
     */
    public TaskTimeoutBo(Future<?> future, Long startTime, Long timeout,
                         Class<?> taskClazz, Map<String, String> mdcContext) {
        this.future = future;
        this.startTime = startTime;
        this.timeout = timeout;
        this.taskClazz = taskClazz;
        this.mdcContext = mdcContext;
    }

    public Future<?> getFuture() {
        return future;
    }

    public void setFuture(Future<?> future) {
        this.future = future;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public Class<?> getTaskClazz() {
        return taskClazz;
    }

    public void setTaskClazz(Class<?> taskClazz) {
        this.taskClazz = taskClazz;
    }

    public Map<String, String> getMdcContext() {
        return mdcContext;
    }

    public void setMdcContext(Map<String, String> mdcContext) {
        this.mdcContext = mdcContext;
    }
}
