package com.flow.framework.core.system.checker;

import java.util.Set;

/**
 * 系统健康检查器接口
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public interface SystemStatusChecker {

    /**
     * 异步检查健康检查
     *
     * @return 不健康标签
     */
    Set<String> asyncHealthCheck();

    /**
     * 获取健康检查项对应的编码
     *
     * @return 健康编码
     */
    int getServiceHealthCheckCode();

    /**
     * 系统启动后延迟多少毫秒执行
     *
     * @return 系统启动后延迟多少毫秒执行
     */
    long getInitialDelay();

    /**
     * 间隔多少毫秒之后执行一次
     *
     * @return 间隔多少毫秒之后执行一次
     */
    long getPeriod();
}
