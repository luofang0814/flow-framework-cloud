package com.flow.framework.core.system.thread.pool.task;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.holder.SecurityContextHolder;
import com.flow.framework.core.holder.SystemVersionContextHolder;
import com.flow.framework.core.system.helper.AsyncHelper;
import org.slf4j.MDC;

import java.util.HashMap;
import java.util.Map;

/**
 * 基础任务类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
abstract class BaseTask {

    /**
     * 日志MDC数据缓存
     */
    private Map<String, String> mdcContext;

    /**
     * 任务超时时间
     */
    private long timeout;

    /**
     * 系统版本号
     */
    private Long systemVersion;

    /**
     * 租户id
     */
    private String tenantId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 创建任务
     *
     * @param timeout 任务超时时间
     */
    BaseTask(long timeout) {
        this.timeout = timeout;
        this.mdcContext = MDC.getCopyOfContextMap();
        this.systemVersion = SystemVersionContextHolder.getCurrentSystemVersion();
        this.tenantId = SecurityContextHolder.getTenantIdQuietly();
        this.userId = SecurityContextHolder.getUserIdQuietly();
    }

    /**
     * 任务执行前需要处理的事项
     */
    final void preprocess() {
        MDC.clear();
        if (!VerifyUtil.isEmpty(mdcContext)) {
            MDC.setContextMap(mdcContext);
        } else {
            MDC.put(FrameworkCommonConstant.GLOBAL_LOG_TRACE_KEY, AsyncHelper.randomAsyncTraceId());
            mdcContext = MDC.getCopyOfContextMap();
        }
        SecurityContextHolder.clearAll();
        SystemVersionContextHolder.setCurrentSystemVersion(systemVersion);
        SecurityContextHolder.setTenantId(tenantId);
        SecurityContextHolder.setUserId(userId);
    }

    /**
     * 任务结束后需要处理的事项
     */
    final void postprocess() {
        MDC.clear();
        SecurityContextHolder.clearAll();
        SystemVersionContextHolder.clear();
    }

    /**
     * 返回日志MDC数据副本
     *
     * @return 日志MDC数据副本
     */
    Map<String, String> getMdcContext() {
        Map<String, String> temp = new HashMap<>(16);
        if (null != mdcContext) {
            temp.putAll(mdcContext);
        }
        return temp;
    }

    /**
     * 获取超时时间
     *
     * @return 超时时间
     */
    long getTimeout() {
        return timeout;
    }
}