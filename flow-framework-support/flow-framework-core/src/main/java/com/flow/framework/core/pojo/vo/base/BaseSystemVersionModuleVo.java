package com.flow.framework.core.pojo.vo.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 远程调用基础view Object对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/5
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public abstract class BaseSystemVersionModuleVo extends BaseModuleVo {
    /**
     * 系统版本，使用场景：灰度发布、预期内的高发业务可以使用多套环境分担压力等
     * 由脚手架自动设置值，不可在业务代码中设置相关的值
     */
    private Long systemVersion;
}
