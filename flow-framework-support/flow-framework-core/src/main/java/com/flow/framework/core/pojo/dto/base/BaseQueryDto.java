package com.flow.framework.core.pojo.dto.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基础查询请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseQueryDto extends BaseDto {

}
