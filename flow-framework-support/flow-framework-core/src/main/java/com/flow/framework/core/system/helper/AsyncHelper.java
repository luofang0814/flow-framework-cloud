package com.flow.framework.core.system.helper;

import com.flow.framework.common.util.random.RandomUtil;
import com.flow.framework.core.constant.FrameworkCoreConstant;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import lombok.RequiredArgsConstructor;

/**
 * 异步调度辅助类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
@RequiredArgsConstructor
public class AsyncHelper implements ISystemLifecycleListener {

    private final ISystemConfigPropertiesService systemConfigPropertiesService;

    private static String serverPort = "00000";

    /**
     * 随机获取异步trace id
     *
     * @return trace id
     */
    public static String randomAsyncTraceId() {
        return serverPort + "-" + RandomUtil.random14LenId();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onStartUp() {
        serverPort = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_CLIENT_PORT_KEY);
    }
}
