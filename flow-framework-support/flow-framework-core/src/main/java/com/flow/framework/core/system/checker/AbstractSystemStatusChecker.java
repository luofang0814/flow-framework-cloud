package com.flow.framework.core.system.checker;

import java.util.Set;

/**
 * 系统健康检查器抽象实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
public abstract class AbstractSystemStatusChecker implements SystemStatusChecker {

    /**
     * @inheritDoc
     */
    @Override
    public final Set<String> asyncHealthCheck() {
        return executeAsyncHealthCheck();
    }

    /**
     * 异步检查健康检查
     *
     * @return 不健康标签
     */
    protected abstract Set<String> executeAsyncHealthCheck();
}
