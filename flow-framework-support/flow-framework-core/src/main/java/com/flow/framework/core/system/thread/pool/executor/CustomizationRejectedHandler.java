package com.flow.framework.core.system.thread.pool.executor;

import com.flow.framework.core.system.checker.ThreadPoolStatusChecker;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 自定义拒绝策略处理器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/23
 */
interface CustomizationRejectedHandler {

    /**
     * 拒绝策略具体处理方法
     *
     * @param r                        任务
     * @param e                        线程池
     * @param threadPoolStatusListener 线程池监听器
     * @param customizationRejectedHandler 自定义拒绝策略
     */
    void rejectedExecution(Runnable r, ThreadPoolExecutor e, ThreadPoolStatusChecker threadPoolStatusListener,
                           CustomizationRejectedHandler customizationRejectedHandler);
}