package com.flow.framework.persistence.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flow.framework.persistence.pojo.po.base.BaseSystemVersionPo;

/**
 * 不可使用，仅仅为了让mybatis-plus能解析BaseSystemVersionPo的方法的lambda表达式
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/10
 */
@Deprecated
public interface BaseSystemVersionPoMapper extends BaseMapper<BaseSystemVersionPo> {

}
