package com.flow.framework.persistence.properties;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 框架持久化配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
public class FrameworkPersistenceConfigProperties {

    private String secret;

    /**
     * 忽略租户id的表名
     */
    private List<String> ignoreTenantTableNames = new ArrayList<>();

    /**
     * 忽略环境编号的表
     */
    private List<String> ignoreSystemVersionTableNames = new ArrayList<>();
}
