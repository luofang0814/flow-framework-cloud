package com.flow.framework.persistence.pojo.po.base;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.flow.framework.persistence.pojo.po.base.BaseSystemVersionPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基础业务实体类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseBizPo extends BaseSystemVersionPo {

    /**
     * 租户id，主要用于数据要求逻辑隔离，由脚手架自动设置值，不可在业务代码中设置相关的值
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private String tenantId;
}
