package com.flow.framework.persistence.persistence.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 该类主要用于使用扩展mybatis-plus的com.baomidou.mybatisplus.extension.service.impl.ServiceImpl类，即将baseMapper设置到父类中
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/16
 */
public class BasePersistenceServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements IService<T> {

    BasePersistenceServiceImpl(M baseMapper) {
        super();
        super.baseMapper = baseMapper;
    }
}
