package com.flow.framework.persistence.system.listener.lifecycle;

import com.flow.framework.persistence.manager.LocalDataSourceTransactionManager;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * transaction manager holder
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
public class DataSourceTransactionMgrHolder {

    private static LocalDataSourceTransactionManager localTransactionManager;

    static void setLocalDataSourceTransactionManager(LocalDataSourceTransactionManager localTransactionManager) {
        DataSourceTransactionMgrHolder.localTransactionManager = localTransactionManager;
    }

    public static DataSourceTransactionManager getLocalTransactionManager() {
        return localTransactionManager;
    }
}
