package com.flow.framework.persistence.system.listener.lifecycle;

import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import com.flow.framework.persistence.manager.LocalDataSourceTransactionManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 事务管理初始化
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
@Slf4j
@RequiredArgsConstructor
public class PersistenceHolderLifecycleListener implements ISystemLifecycleListener {

    private final LocalDataSourceTransactionManager localTransactionManager;

    /**
     * @inheritDoc
     */
    @Override
    public void onStartUp() {
        DataSourceTransactionMgrHolder.setLocalDataSourceTransactionManager(localTransactionManager);
    }
}
