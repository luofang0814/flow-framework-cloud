package com.flow.framework.persistence.persistence.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("ALL")
/**
 * 该类主要用于使用mybatis-plus的com.baomidou.mybatisplus.extension.service.impl.ServiceImpl查询数据库相关数据，即使用者需要
 * 继承该类，然后自定义方法进行数据库查询，如：
 *
 * public interface ITestService extends IPersistenceService<Test> {
 *
 *     Test get(TestBo testBo);
 * }
 *
 * @Service
 * public class TestServiceImpl extends PersistenceServiceImpl<TestMapper, Test> implements ITestService {
 *     @Override
 *     public Test get(TestBo testBo) {
 *         return CollectionUtil.getFirstElement(service().lambdaQuery().eq(Test::getName, testBo.getName())).list());
 *     }
 * }
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/16
 */
public class PersistenceServiceImpl<M extends BaseMapper<T>, T> {

    private BasePersistenceServiceImpl<M, T> service;

    private M mapper;

    @Autowired(required = true)
    private void setBaseMapper(M mapper) {
        this.mapper = mapper;
        this.service = new BasePersistenceServiceImpl<M, T>(mapper);
    }

    protected final BasePersistenceServiceImpl<M, T> service() {
        return service;
    }

    protected final M mapper() {
        return mapper;
    }
}
