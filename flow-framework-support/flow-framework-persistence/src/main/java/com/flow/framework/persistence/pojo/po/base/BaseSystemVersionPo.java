package com.flow.framework.persistence.pojo.po.base;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基础系统版本实体类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class BaseSystemVersionPo extends BasePo {

    /**
     * 系统版本，使用场景：灰度发布、预期内的高发业务可以使用多套环境分担压力等
     * 由脚手架自动设置值，不可在业务代码中设置相关的值
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private Long systemVersion;
}
