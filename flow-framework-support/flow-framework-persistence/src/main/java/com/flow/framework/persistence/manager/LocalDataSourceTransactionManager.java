package com.flow.framework.persistence.manager;

import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.lang.Nullable;

import javax.sql.DataSource;

/**
 * 本地事务管理器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
public class LocalDataSourceTransactionManager extends DataSourceTransactionManager {

    public LocalDataSourceTransactionManager(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * 适配分布式事务，即：将分布式事务代理中的本地数据源剥离出来
     *
     * @param dataSource dataSource
     */
    @Override
    public void setDataSource(@Nullable DataSource dataSource) {
        if (dataSource instanceof DataSourceProxy) {
            super.setDataSource(((DataSourceProxy) dataSource).getTargetDataSource());
        }
        super.setDataSource(dataSource);
    }
}