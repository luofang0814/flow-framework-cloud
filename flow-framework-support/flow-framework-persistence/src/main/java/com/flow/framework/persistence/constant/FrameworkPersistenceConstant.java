package com.flow.framework.persistence.constant;

/**
 * 持久化常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
public final class FrameworkPersistenceConstant {

    /**
     * 系统版本号对应的列的名称
     */
    public static final String SYSTEM_VERSION_COLUMN_NAME = "system_version";
}