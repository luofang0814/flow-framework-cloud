package com.flow.framework.persistence.pojo.po.base;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础实体类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
@EqualsAndHashCode
public abstract class BasePo implements Serializable {

    /**
     * po id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 创建时间，由数据库自动设置值，不可在业务代码中设置相关的值
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime createTime;

    /**
     * 更新时间，由数据库自动设置值，不可在业务代码中设置相关的值
     */
    @TableField(insertStrategy = FieldStrategy.NEVER, updateStrategy = FieldStrategy.NEVER)
    private LocalDateTime updateTime;

    /**
     * 逻辑删除标记，0-（false）未删除，1-（true）已删除
     */
    @TableLogic(value = "0", delval = "1")
    private Boolean isDeleted = false;
}
