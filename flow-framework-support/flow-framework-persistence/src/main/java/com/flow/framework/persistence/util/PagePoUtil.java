package com.flow.framework.persistence.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.flow.framework.common.util.collection.CollectionUtil;
import com.flow.framework.core.pojo.vo.PageModuleVo;
import com.flow.framework.core.pojo.vo.PageVo;

import java.util.function.Function;

/**
 * 分页数据工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
public class PagePoUtil {

    /**
     * 将持久化分页数据转化为指定value object的分页对象
     *
     * @param page        page
     * @param voConverter voConverter
     * @param <T>         T
     * @param <E>         E
     * @return
     */
    public static <T, E> PageVo<T> convertToPageVo(IPage<E> page, Function<E, T> voConverter) {
        PageVo<T> pageVo = new PageVo<>();
        pageVo.setPages(page.getPages());
        pageVo.setSize(page.getSize());
        pageVo.setTotal(page.getTotal());
        pageVo.setCurrent(page.getCurrent());
        pageVo.setRecords(CollectionUtil.convertToList(page.getRecords(), voConverter));
        return pageVo;
    }

    /**
     * 将分页数据转化为value object的分页对象
     *
     * @param page page
     * @param <T>  T
     * @return
     */
    public static <T> PageVo<T> convertToPageVo(IPage<T> page) {
        PageVo<T> pageVo = new PageVo<>();
        pageVo.setRecords(page.getRecords());
        pageVo.setPages(page.getPages());
        pageVo.setSize(page.getSize());
        pageVo.setTotal(page.getTotal());
        pageVo.setCurrent(page.getCurrent());
        return pageVo;
    }

    /**
     * 将持久化分页数据转化为指定的远程调用value object的分页对象
     *
     * @param page        page
     * @param voConverter voConverter
     * @param <T>         T
     * @param <E>         E
     * @return
     */
    public static <T, E> PageModuleVo<T> convertToPageModuleVo(IPage<E> page, Function<E, T> voConverter) {
        PageModuleVo<T> pageModuleVo = new PageModuleVo<>();
        pageModuleVo.setPages(page.getPages());
        pageModuleVo.setSize(page.getSize());
        pageModuleVo.setTotal(page.getTotal());
        pageModuleVo.setCurrent(page.getCurrent());
        pageModuleVo.setRecords(CollectionUtil.convertToList(page.getRecords(), voConverter));
        return pageModuleVo;
    }

    /**
     * 将分页数据转化为远程调用value object的分页对象
     *
     * @param page page
     * @param <T>  T
     * @return
     */
    public static <T> PageModuleVo<T> convertToPageModuleVo(IPage<T> page) {
        PageModuleVo<T> pageModuleVo = new PageModuleVo<>();
        pageModuleVo.setRecords(page.getRecords());
        pageModuleVo.setPages(page.getPages());
        pageModuleVo.setSize(page.getSize());
        pageModuleVo.setTotal(page.getTotal());
        pageModuleVo.setCurrent(page.getCurrent());
        return pageModuleVo;
    }
}