package com.flow.framework.persistence.persistence.page;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.flow.framework.core.pojo.dto.base.BasePageQueryDto;
import com.flow.framework.core.pojo.dto.base.module.BasePageQueryModuleDto;

import java.util.Collections;
import java.util.List;

/**
 * 分页对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/16
 */
public class Page<T> implements IPage<T> {

    /**
     * 当前页数
     */
    private long current;

    /**
     * 分页大小
     */
    private long size;

    /**
     * 数据总数
     */
    private long total;

    /**
     * 分页数据
     */
    private List<T> records = Collections.emptyList();

    public Page() {
    }

    public Page(long current, long size) {
        this.current = current;
        this.size = size;
    }

    public Page(long current, long size, long total, List<T> records) {
        this.current = current;
        this.size = size;
        this.total = total;
        this.records = records;
    }

    public static <T> Page<T> of(BasePageQueryModuleDto dto) {
        return new Page<>(dto.getCurrent(), dto.getSize());
    }

    public static <T> Page<T> of(BasePageQueryDto dto) {
        return new Page<>(dto.getCurrent(), dto.getSize());
    }

    /**
     * 获取排序信息，排序的字段和正反序
     *
     * @return 排序信息
     */
    @Override
    public List<OrderItem> orders() {
        return Collections.emptyList();
    }

    /**
     * 分页记录列表
     *
     * @return 分页对象记录列表
     */
    @Override
    public List<T> getRecords() {
        return this.records;
    }

    /**
     * 设置分页记录列表
     *
     * @param records records
     */
    @Override
    public IPage<T> setRecords(List<T> records) {
        this.records = records;
        return this;
    }

    /**
     * 当前满足条件总行数
     *
     * @return 总条数
     */
    @Override
    public long getTotal() {
        return this.total;
    }

    /**
     * 设置当前满足条件总行数
     *
     * @param total total
     */
    @Override
    public IPage<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    /**
     * 获取每页显示条数
     *
     * @return 每页显示条数
     */
    @Override
    public long getSize() {
        return this.size;
    }

    /**
     * 设置每页显示条数
     *
     * @param size size
     */
    @Override
    public IPage<T> setSize(long size) {
        this.size = size;
        return this;
    }

    /**
     * 当前页，默认 1
     *
     * @return 当前页
     */
    @Override
    public long getCurrent() {
        return this.current;
    }

    /**
     * 设置当前页
     *
     * @param current current
     */
    @Override
    public IPage<T> setCurrent(long current) {
        this.current = current;
        return this;
    }

    @Override
    public long getPages() {
        if (getSize() == 0) {
            return 0L;
        }
        long pages = getTotal() / getSize();
        if (getTotal() % getSize() != 0) {
            pages++;
        }
        return pages;
    }
}