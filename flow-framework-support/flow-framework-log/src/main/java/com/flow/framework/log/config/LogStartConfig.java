package com.flow.framework.log.config;

import com.flow.framework.log.helper.LogConfigHelper;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.logging.LoggingApplicationListener;
import org.springframework.context.ApplicationEvent;

/**
 * 启动时日志配置监听
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/15
 */
public class LogStartConfig extends LoggingApplicationListener {

    private static boolean alreadyConfig = false;

    /**
     * @inheritDoc
     */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (alreadyConfig) {
            return;
        }
        if (event instanceof ApplicationEnvironmentPreparedEvent) {
            LogConfigHelper.config((ApplicationEnvironmentPreparedEvent) event);
            alreadyConfig = true;
        }
        super.onApplicationEvent(event);
    }
}