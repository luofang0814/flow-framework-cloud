package com.flow.framework.log.definer;

import ch.qos.logback.core.PropertyDefinerBase;

/**
 * 定义日志级别
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/15
 */
public class LogLevelDefiner extends PropertyDefinerBase {

    private static String loggingLevel = "DEBUG";

    @Override
    public String getPropertyValue() {
        return loggingLevel;
    }

    public static void setSpringLoggingLevel(String loggingLevel) {
        LogLevelDefiner.loggingLevel = loggingLevel;
    }
}
