package com.flow.framework.log.config;

import com.flow.framework.log.helper.LogConfigHelper;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;

/**
 * 配置中心更新配置时提供默认日志配置和定制化日志配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/15
 */
public class LogRefreshConfig implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    /**
     * @inheritDoc
     */
    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {

        // 重新加载日志配置
        LogConfigHelper.config(event);
    }
}
