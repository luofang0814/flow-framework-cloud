package com.flow.framework.log.definer;

import ch.qos.logback.core.PropertyDefinerBase;
import com.flow.framework.common.util.verify.VerifyUtil;

import java.util.Locale;

/**
 * 日志路径定义，Windows路径为C:/opt/logs，linux路径为/opt/logs
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/15
 */
public class LogPathDefiner extends PropertyDefinerBase {

    private static final String LOG_PATH_LINUX = "/opt/flow/logs";

    private static final String LOG_PATH_WINDOWS = "C:/opt/flow/logs";

    private static final String SYS_OS_WINDOWS = "WINDOWS";

    private static String CUSTOMIZE_LOG_PATH = null;

    public static void setCustomizeLogPath(String customizeLogPath) {
        CUSTOMIZE_LOG_PATH = customizeLogPath;
    }

    @Override
    public String getPropertyValue() {
        if (null != CUSTOMIZE_LOG_PATH) {
            return CUSTOMIZE_LOG_PATH;
        }
        String osName = System.getProperty("os.name");
        if (!VerifyUtil.isEmpty(osName) && osName.toUpperCase(Locale.ENGLISH).contains(SYS_OS_WINDOWS)) {
            return LOG_PATH_WINDOWS;
        } else {
            return LOG_PATH_LINUX;
        }
    }
}
