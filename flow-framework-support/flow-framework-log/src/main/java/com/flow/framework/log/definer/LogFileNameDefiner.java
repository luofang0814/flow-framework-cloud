package com.flow.framework.log.definer;

import ch.qos.logback.core.PropertyDefinerBase;
import com.flow.framework.common.util.verify.VerifyUtil;

/**
 * 定义日志名称类，设计思路为在项目启动时获取spring.application.name作为日志名称，如果没有获取到，则名称为default
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/15
 */
public class LogFileNameDefiner extends PropertyDefinerBase {

    private static String logFileName = "default";

    @Override
    public String getPropertyValue() {
        return logFileName;
    }

    public static void setLogFileName(String logFileName) {
        if (VerifyUtil.isEmpty(logFileName)) {
            return;
        }
        LogFileNameDefiner.logFileName = logFileName;
    }
}
