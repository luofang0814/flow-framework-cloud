package com.flow.framework.schedule.enumeration;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 异步调度分布式锁
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@AllArgsConstructor
@Getter
public enum FrameworkScheduleLockKeyEnum implements ILockKeyEnum {

    /**
     * 异步调度实际执行前的分布式锁
     */
    FRAMEWORK_SCHEDULE_EXEC_LOCK_KEY(1, "异步调度实际执行前的分布式锁"),
    ;

    private int paramsSize;

    private String remark;
}
