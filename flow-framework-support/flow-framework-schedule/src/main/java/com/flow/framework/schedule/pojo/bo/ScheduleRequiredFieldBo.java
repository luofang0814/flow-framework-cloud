package com.flow.framework.schedule.pojo.bo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flow.framework.cache.service.cache.common.ICommonCacheService;
import com.flow.framework.core.properties.FrameworkCoreConfigProperties;
import com.flow.framework.facade.authority.module.service.IAuthorityFrameworkModuleService;
import com.flow.framework.facade.system.module.service.ISystemFrameworkModuleService;
import com.flow.framework.persistence.pojo.po.base.BaseSystemVersionPo;
import com.flow.framework.schedule.job.IPagePoScheduleHandler;
import lombok.Data;

/**
 * 调度所需的属性
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/10
 */
@Data
public class ScheduleRequiredFieldBo<E extends BaseSystemVersionPo> {

    private IPagePoScheduleHandler<E> pagePoScheduleHandler;

    private FrameworkCoreConfigProperties frameworkCoreConfigProperties;

    private ISystemFrameworkModuleService systemFrameworkModuleService;

    private IAuthorityFrameworkModuleService authorityFrameworkModuleService;

    private ICommonCacheService commonCacheService;

    private BaseMapper<E> mapper;

    private String params;
}