package com.flow.framework.schedule.properties.rpc;

import lombok.Data;

/**
 * 远程调度配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@Data
public class RpcConfigProperties {

    /**
     * 管理端地址
     */
    private String adminAddresses;

    /**
     * 客户端端口
     */
    private int clientPort = 9999;

    /**
     * 客户端和调度服务端交互token
     */
    private String accessToken;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 客户端日志地址
     */
    private String clientLogPath;

    /**
     * 客户端日志保存时间
     */
    private int clientLogRetentionDays = 90;

}