package com.flow.framework.schedule.properties.local;

import com.flow.framework.schedule.properties.local.sub.SubLocalConfigProperties;
import lombok.Data;

import java.util.List;

/**
 * 本地调度配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@Data
public class LocalConfigProperties {

    /**
     * spring的bean名称
     */
    private String beanName;

    /**
     * cron表达式
     */
    private String cron;

    /**
     * 参数
     */
    private String params;

    /**
     * 负责人
     */
    private String author;

    /**
     * 备注
     */
    private String remark;

    /**
     * 子任务配置
     */
    private List<SubLocalConfigProperties> subs;
}