package com.flow.framework.schedule.holder;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

/**
 * 本地调度器持有者
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@Slf4j
public class LocalSchedulerHolder {

    public static Scheduler getInstance() {
        return Holder.instance;
    }

    private static class Holder {

        private static Scheduler instance;

        static {
            try {
                instance = StdSchedulerFactory.getDefaultScheduler();
                instance.start();
            } catch (Exception e) {
                e.printStackTrace();
                log.error("init local scheduler error.", e);
            }
        }
    }
}