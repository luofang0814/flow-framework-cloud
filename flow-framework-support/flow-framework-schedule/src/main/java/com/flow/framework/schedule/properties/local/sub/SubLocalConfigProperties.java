package com.flow.framework.schedule.properties.local.sub;

import lombok.Data;

/**
 * 本地调度的子任务配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@Data
public class SubLocalConfigProperties {

    /**
     * spring的bean名称
     */
    private String beanName;

    /**
     * 参数
     */
    private String params;

    /**
     * 负责人
     */
    private String author;

    /**
     * 备注
     */
    private String remark;
}