package com.flow.framework.schedule.config.condition;

import com.flow.framework.common.util.verify.VerifyUtil;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 是否加载 rpc schedule配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
public class RpcScheduleJobExecutorInitCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String adminAddresses = context.getEnvironment()
                .resolvePlaceholders("${customization.framework.schedule.rpc.admin-addresses:}");
        return !VerifyUtil.isEmpty(adminAddresses);
    }
}