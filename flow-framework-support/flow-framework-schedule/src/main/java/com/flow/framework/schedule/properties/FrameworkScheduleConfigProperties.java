package com.flow.framework.schedule.properties;

import com.flow.framework.schedule.properties.local.LocalConfigProperties;
import com.flow.framework.schedule.properties.rpc.RpcConfigProperties;
import lombok.Data;

import java.util.List;

/**
 * 调度相关配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@Data
public class FrameworkScheduleConfigProperties {

    /**
     * 远程调度配置
     */
    private RpcConfigProperties rpc;

    /**
     * 本地调度配置
     */
    private List<LocalConfigProperties> locals;
}
