package com.flow.framework.schedule.job;

import com.flow.framework.core.response.Response;
import com.flow.framework.core.util.ProxyUtil;
import com.flow.framework.facade.authority.module.service.IAuthorityFrameworkModuleService;
import com.flow.framework.persistence.pojo.po.base.BaseBizPo;
import com.flow.framework.schedule.pojo.bo.ScheduleRequiredFieldBo;
import lombok.extern.slf4j.Slf4j;

/**
 * 调度base biz po分页处理任务抽象实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/10
 */
@Slf4j
public abstract class BaseBizScheduleJob<E extends BaseBizPo> extends BaseSystemVersionScheduleJob<E> implements IPagePoScheduleHandler<E> {

    /**
     * @inheritDoc
     */
    @Override
    public void onStartUp() {
        super.onStartUp();
        getScheduleRequiredFieldBo().setAuthorityFrameworkModuleService(ProxyUtil.getProxied(IAuthorityFrameworkModuleService.class));
    }

    /**
     * @inheritDoc
     */
    @Override
    public Response<String> handle(String params) {
        ScheduleRequiredFieldBo<E> scheduleRequiredFieldBo = getScheduleRequiredFieldBo();
        scheduleRequiredFieldBo.setParams(params);
        long dataCount = ScheduleHelper.bizPoPageSchedule(scheduleRequiredFieldBo);
        log.info("process data count {}, page size {}", dataCount, getPageSize(params));
        return Response.success(String.valueOf(dataCount));
    }
}