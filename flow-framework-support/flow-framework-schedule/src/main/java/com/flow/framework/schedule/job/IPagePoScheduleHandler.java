package com.flow.framework.schedule.job;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flow.framework.persistence.pojo.po.base.BaseSystemVersionPo;

import java.util.List;

/**
 * base system version po分页处理器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/10
 */
public interface IPagePoScheduleHandler<E extends BaseSystemVersionPo> {

    /**
     * 默认分页最大值
     */
    int DEFAULT_PAGE_SIZE = 500;

    /**
     * 默认开始查询的最大ID
     */
    String START_GREATER_ID = "0";

    /**
     * 默认查询超时时间30分钟
     */
    long DEFAULT_QUERY_TIMEOUT = 3600000;

    /**
     * 获取任务名称（主任务）
     *
     * @return 任务名称
     */
    String getJobName();

    /**
     * 构造查询条件
     *
     * @param params      调度参数
     * @param lambdaQuery lambda查询
     */
    void buildLambdaQuery(String params, LambdaQueryWrapper<E> lambdaQuery);

    /**
     * 分页处理
     *
     * @param record      record
     * @param currentPage 当前页数
     * @param pageSize    每页数据量
     * @param totalCount  数据总条数
     * @param params      调度参数
     */
    void process(List<E> record, long currentPage, long pageSize, long totalCount, String params);

    /**
     * 获取分页大小
     *
     * @param params 调度参数
     * @return 分页大小
     */
    default int getPageSize(String params) {
        return DEFAULT_PAGE_SIZE;
    }

    /**
     * 获取开始查询的ID，不包含该ID，即：只查询大于该ID的数据
     *
     * @param params 调度参数
     * @return 开始查询的最大id
     */
    default String getStartGreaterId(String params) {
        return START_GREATER_ID;
    }

    /**
     * 获取数据库查询超时时间
     *
     * @param params 调度参数
     * @return 查询超时时间
     */
    default long getQueryTimeout(String params) {
        return DEFAULT_QUERY_TIMEOUT;
    }
}