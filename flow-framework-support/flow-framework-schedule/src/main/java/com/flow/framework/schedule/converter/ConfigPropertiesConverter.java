package com.flow.framework.schedule.converter;

import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.schedule.properties.local.LocalConfigProperties;
import com.flow.framework.schedule.properties.local.sub.SubLocalConfigProperties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 配置转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
public class ConfigPropertiesConverter {

    /**
     * 将调度本地配置转换成新的本地调度配置
     *
     * @param locals 本地调度配置
     * @return 全新的本地调度配置
     */
    public static List<LocalConfigProperties> toNewLocalConfigPropertiesList(List<LocalConfigProperties> locals) {
        if (VerifyUtil.isEmpty(locals)) {
            return Collections.emptyList();
        }
        List<LocalConfigProperties> localConfigPropertiesList = new ArrayList<>();
        locals.forEach(localConfigProperties -> {
            LocalConfigProperties configProperties = new LocalConfigProperties();
            configProperties.setBeanName(localConfigProperties.getBeanName());
            configProperties.setCron(localConfigProperties.getCron());
            configProperties.setParams(localConfigProperties.getParams());
            configProperties.setAuthor(localConfigProperties.getAuthor());
            configProperties.setRemark(localConfigProperties.getRemark());
            List<SubLocalConfigProperties> subs = localConfigProperties.getSubs();
            if (!VerifyUtil.isEmpty(subs)) {
                List<SubLocalConfigProperties> subLocalConfigPropertiesList = new ArrayList<>();
                subs.forEach(subLocalConfigProperties -> {
                    SubLocalConfigProperties subLocalConfigProp = new SubLocalConfigProperties();
                    subLocalConfigProp.setBeanName(subLocalConfigProperties.getBeanName());
                    subLocalConfigProp.setParams(subLocalConfigProperties.getParams());
                    subLocalConfigProp.setAuthor(subLocalConfigProperties.getAuthor());
                    subLocalConfigProp.setRemark(subLocalConfigProperties.getRemark());
                    subLocalConfigPropertiesList.add(subLocalConfigProp);
                });
                configProperties.setSubs(subLocalConfigPropertiesList);
            }
            localConfigPropertiesList.add(configProperties);
        });
        return localConfigPropertiesList;
    }
}