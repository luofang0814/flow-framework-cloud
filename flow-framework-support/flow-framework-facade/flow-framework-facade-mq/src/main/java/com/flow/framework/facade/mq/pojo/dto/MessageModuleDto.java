package com.flow.framework.facade.mq.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * MQ消息的对象传输
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MessageModuleDto extends BaseModuleDto {

    /**
     * 系统版本号
     */
    private Long systemVersion;

    /**
     * 租户id
     */
    private String tenantId;

    /**
     * 入访id
     */
    private String traceId;

    /**
     * 元数据信息
     */
    private Map<String, String> metadata;

    /**
     * 业务日期
     */
    private LocalDateTime bizDateTime;

    /**
     * 消息ID
     */
    private String messageId;

    /**
     * 消息headers
     */
    private Map<String, Object> messageHeaders;

    /**
     * 消息体
     */
    private String messageBody;
}
