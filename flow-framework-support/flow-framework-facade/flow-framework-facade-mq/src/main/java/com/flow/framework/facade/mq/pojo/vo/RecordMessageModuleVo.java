package com.flow.framework.facade.mq.pojo.vo;

import com.flow.framework.core.pojo.vo.base.BaseModuleVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 消息持久化响应
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RecordMessageModuleVo extends BaseModuleVo {

    /**
     * 是否达到重试上限
     */
    private boolean reachedMaxRetry;
}
