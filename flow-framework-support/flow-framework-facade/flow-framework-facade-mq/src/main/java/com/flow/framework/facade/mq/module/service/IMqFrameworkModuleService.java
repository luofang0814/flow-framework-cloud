package com.flow.framework.facade.mq.module.service;

import com.flow.framework.facade.mq.pojo.dto.MessageModuleDto;
import com.flow.framework.facade.mq.constant.MessageQueueFacadeConstant;
import com.flow.framework.facade.mq.pojo.vo.RecordMessageModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 框架消息模块服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
@FeignClient(
        contextId = "framework-facade-mq-mqFrameworkModuleService",
        url = "${customization.framework.facade.mq.url:}",
        name = "${customization.framework.facade.mq.application-name:}",
        path = "${customization.framework.facade.mq.rpc-prefix:}")
public interface IMqFrameworkModuleService {

    /**
     * 记录处理失败的消息
     *
     * @param messageModuleDto recordHandleFailedMsgModuleDto
     * @return
     */
    @PostMapping(path = MessageQueueFacadeConstant.RECORD_HANDLE_FAILED_MSG_URI)
    RecordMessageModuleVo recordHandleFailedMsg(@RequestBody MessageModuleDto messageModuleDto);
}
