package com.flow.framework.facade.mq.constant;

/**
 * 消息队列facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
public final class MessageQueueFacadeConstant {

    /**
     * 记录处理失败的消息
     */
    public static final String RECORD_HANDLE_FAILED_MSG_URI = "/recordHandleFailedMsg";
}