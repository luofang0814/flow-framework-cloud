package com.flow.framework.facade.i18n.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Locale;

/**
 * 记录未国际化请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RecordUnInternationalModuleDto extends BaseModuleDto {

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 国际化key
     */
    private String key;

    /**
     * 语言类型
     */
    private Locale locale;
}
