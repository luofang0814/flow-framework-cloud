package com.flow.framework.facade.i18n.module.service;

import com.flow.framework.facade.i18n.constant.I18nFacadeConstant;
import com.flow.framework.facade.i18n.pojo.dto.RecordUnInternationalModuleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

/**
 * 国际化服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
@FeignClient(
        contextId = "framework-facade-i18n-i18nFrameworkModuleService",
        url = "${customization.framework.facade.i18n.url:}",
        name = "${customization.framework.facade.i18n.application-name:}",
        path = "${customization.framework.facade.i18n.rpc-prefix:}")
public interface Ii18nFrameworkModuleService {

    /**
     * 根据地区和国际化key展示语言
     *
     * @param serviceName serviceName
     * @param key         key
     * @param locale      locale
     * @return 国际化值
     */
    @GetMapping(path = I18nFacadeConstant.GET_INTERNATIONAL_VALUE_URI)
    String getInternationalValue(@RequestParam(name = "serviceName") String serviceName,
                                 @RequestParam(name = "key") String key,
                                 @RequestParam(name = "locale") Locale locale);

    /**
     * 记录未被国际化的key
     *
     * @param recordUnInternationalModuleDto recordUnInternationalModuleDto
     */
    @PostMapping(path = I18nFacadeConstant.RECORD_UN_INTERNATIONAL_KEY_URI)
    void recordUnInternationalKey(@RequestBody RecordUnInternationalModuleDto recordUnInternationalModuleDto);
}
