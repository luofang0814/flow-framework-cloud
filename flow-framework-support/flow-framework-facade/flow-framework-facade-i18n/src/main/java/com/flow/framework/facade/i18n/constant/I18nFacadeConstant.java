package com.flow.framework.facade.i18n.constant;

/**
 * 国际化facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
public final class I18nFacadeConstant {

    /**
     * 获取国际化结果
     */
    public static final String GET_INTERNATIONAL_VALUE_URI = "/getInternationalValue";

    /**
     * 记录未国际化的key
     */
    public static final String RECORD_UN_INTERNATIONAL_KEY_URI = "/recordUnInternationalKey";
}