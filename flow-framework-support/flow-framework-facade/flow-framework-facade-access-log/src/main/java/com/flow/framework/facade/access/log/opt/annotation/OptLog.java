package com.flow.framework.facade.access.log.opt.annotation;

import java.lang.annotation.*;

/**
 * 操作日志记录注解
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface OptLog {

    /**
     * 菜单编码，如：flow-service-test:taskManage:orderManage:refuse
     * 表示任务管理下的订单管理下的拒绝按钮
     */
    String menuCode();

    /**
     * 版本号
     *
     * @return 版本号
     */
    int version() default 1;

    /**
     * 备注
     *
     * @return 备注
     */
    String remark();
}
