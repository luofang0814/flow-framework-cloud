package com.flow.framework.facade.access.log.constant;

/**
 * 入访日志facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
public final class AccessLogFacadeConstant {

    /**
     * 记录入访日志
     */
    public static final String TRACE_LOG_RECORD_URI = "/recordTraceLog";

    /**
     * 记录操作日志
     */
    public static final String OPT_LOG_RECORD_URI = "/recordOptLog";
}