package com.flow.framework.facade.access.log.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 请求入访日志
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TraceLogModuleDto extends BaseModuleDto {

    /**
     * 请求IP
     */
    private String requestIp;

    /**
     * 请求方法
     */
    private String requestMethod;

    /**
     * 请求URI
     */
    private String requestUri;

    /**
     * 请求时间
     */
    private Long requestTime;

    /**
     * 响应时间
     */
    private Long responseTime;

    /**
     * 请求消耗的时间数
     */
    private Integer duration;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * 请求内容类型
     */
    private String requestContentType;

    /**
     * 请求参数
     */
    private String requestBody;

    /**
     * 响应的body类型
     */
    private String responseBodyType;

    /**
     * 响应参数
     */
    private String responseBody;

    /**
     * HTTP状态码
     */
    private Integer httpStatus;
}
