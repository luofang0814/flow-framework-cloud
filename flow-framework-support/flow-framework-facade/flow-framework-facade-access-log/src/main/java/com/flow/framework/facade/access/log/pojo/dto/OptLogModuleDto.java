package com.flow.framework.facade.access.log.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 操作日志
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OptLogModuleDto extends BaseModuleDto {

    /**
     * 请求入访日志
     */
    private TraceLogModuleDto traceLogModuleDto;

    /**
     * 菜单编码
     */
    private String menuCode;

    /**
     * 菜单编码
     */
    private Boolean isSuccess;

    /**
     * 版本
     */
    private Integer version;

    /**
     * 备注
     */
    private String remark;

    /**
     * 国际化参数，对应国际化文件的参数取值
     */
    private String i18nParams;
}
