package com.flow.framework.facade.access.log.module.service;

import com.flow.framework.facade.access.log.constant.AccessLogFacadeConstant;
import com.flow.framework.facade.access.log.pojo.dto.OptLogModuleDto;
import com.flow.framework.facade.access.log.pojo.dto.TraceLogModuleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 入访日志记录模块服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@FeignClient(
        contextId = "framework-facade-access-log-accessLogFrameworkModuleService",
        url = "${customization.framework.facade.access-log.url:}",
        name = "${customization.framework.facade.access-log.application-name:}",
        path = "${customization.framework.facade.access-log.rpc-prefix:}")
public interface IAccessLogFrameworkModuleService {

    /**
     * 记录请求入访日志
     *
     * @param traceLogModuleDto 请求入访日志记录请求
     */
    @PostMapping(path = AccessLogFacadeConstant.TRACE_LOG_RECORD_URI)
    void recordTraceLog(@RequestBody TraceLogModuleDto traceLogModuleDto);

    /**
     * 记录操作日志
     *
     * @param optLogModuleDto 操作日志记录请求
     */
    @PostMapping(path = AccessLogFacadeConstant.OPT_LOG_RECORD_URI)
    void recordOptLog(@RequestBody OptLogModuleDto optLogModuleDto);
}
