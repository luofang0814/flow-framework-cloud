package com.flow.framework.facade.system.pojo.vo;

import com.flow.framework.core.pojo.vo.base.BaseModuleVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 属于当前环境的版本号的VO
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SystemVersionModuleVo extends BaseModuleVo {

    /**
     * 当前版本号
     */
    private Long currentVersion;

    /**
     * 开始版本号，格式为年月日时分秒结构，如：20201015221511
     */
    private Long startVersion;

    /**
     * 结束版本号，格式为年月日时分秒结构，如：20201015221555
     */
    private Long endVersion;
}