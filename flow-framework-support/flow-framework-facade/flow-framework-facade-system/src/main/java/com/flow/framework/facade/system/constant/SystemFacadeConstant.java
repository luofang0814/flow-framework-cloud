package com.flow.framework.facade.system.constant;

/**
 * 系统facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
public final class SystemFacadeConstant {

    /**
     * 获取系统版本信息
     */
    public static final String GET_SYSTEM_VERSION_URI = "/getSystemVersion";
}