package com.flow.framework.facade.system.module.service;

import com.flow.framework.facade.system.constant.SystemFacadeConstant;
import com.flow.framework.facade.system.pojo.vo.SystemVersionModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 系统服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/4
 */
@FeignClient(
        contextId = "framework-facade-system-systemFrameworkModuleService",
        url = "${customization.framework.facade.system.url:}",
        name = "${customization.framework.facade.system.application-name:}",
        path = "${customization.framework.facade.system.rpc-prefix:}")
public interface ISystemFrameworkModuleService {


    /**
     * 根据环境编码获取当前系统版本号
     *
     * @param envCode 环境编码
     * @return 当前环境编码的系统版本号
     */
    @GetMapping(path = SystemFacadeConstant.GET_SYSTEM_VERSION_URI)
    SystemVersionModuleVo getSystemVersion(@RequestParam("envCode") String envCode);
}
