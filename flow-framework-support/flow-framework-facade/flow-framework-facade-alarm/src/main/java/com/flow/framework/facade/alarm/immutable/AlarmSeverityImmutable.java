package com.flow.framework.facade.alarm.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 告警级别常量限制类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/3/19
 */
public interface AlarmSeverityImmutable extends Immutable {

    /**
     * 提示告警
     */
    @ImmutableRemark("提示告警")
    String WARNING = "1";

    /**
     * 一般告警
     */
    @ImmutableRemark("一般告警")
    String MINOR = "2";

    /**
     * 严重告警
     */
    @ImmutableRemark("严重告警")
    String MAJOR = "3";

    /**
     * 紧急告警
     */
    @ImmutableRemark("紧急告警")
    String CRITICAL = "4";
}