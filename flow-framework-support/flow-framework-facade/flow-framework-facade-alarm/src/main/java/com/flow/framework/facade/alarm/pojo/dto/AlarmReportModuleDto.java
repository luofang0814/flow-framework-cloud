package com.flow.framework.facade.alarm.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.facade.alarm.error.AlarmErrorCode;
import com.flow.framework.facade.alarm.immutable.AlarmSeverityImmutable;
import com.flow.framework.facade.alarm.immutable.AlarmTypeImmutable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.annotation.Nullable;
import java.time.LocalDateTime;

/**
 * 告警上报请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AlarmReportModuleDto extends BaseModuleDto {

    /**
     * 告警来源的模块名称
     */
    private String moduleName;

    /**
     * 告警来源类型，如：swap空间过小、xxx业务短信通知失败等（由业务侧自己定义）
     */
    private String sourceType;

    /**
     * 如果有实际对象，则为对象的id，否则为空
     */
    @Nullable
    private String sourceId;

    /**
     * 告警类型
     */
    @ImmutableCustomization(errorCode = AlarmErrorCode.ALARM_TYPE_ERROR, immutableInterface = AlarmTypeImmutable.class)
    private String alarmType;

    /**
     * 告警级别
     */
    @ImmutableCustomization(errorCode = AlarmErrorCode.ALARM_SEVERITY_TYPE_ERROR, immutableInterface = AlarmSeverityImmutable.class)
    private String alarmSeverity;

    /**
     * 告警时间
     */
    private LocalDateTime alarmTime;

    /**
     * 告警版本，如果告警参数有调整，则可以修改该版本号，用于区分同一种告警不同版本
     */
    private Integer alarmVersion;

    /**
     * 告警来源类型对应的国际化所需要的的参数，格式为jsonArray，如：[{"id":"123", "name":"xx"}]
     */
    private String alarmParams;
}
