package com.flow.framework.facade.alarm.constant;

/**
 * 告警facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
public final class AlarmFacadeConstant {

    /**
     * 上报告警
     */
    public static final String ALARM_REPORT_URI = "/report";

    /**
     * 清除告警
     */
    public static final String ALARM_CLEAR_URI = "/clear";
}