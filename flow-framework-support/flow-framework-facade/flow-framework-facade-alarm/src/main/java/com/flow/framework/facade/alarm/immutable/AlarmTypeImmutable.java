package com.flow.framework.facade.alarm.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 告警类型常量限制类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/3/19
 */
public interface AlarmTypeImmutable extends Immutable {

    /**
     * 系统交互（包括外部系统和内部系统）
     */
    @ImmutableRemark("系统交互")
    String COMMUNICATION = "1";

    /**
     * 环境信息（如：swap空间过小、内存占用过多等）
     */
    @ImmutableRemark("环境信息")
    String ENVIRONMENT = "2";

    /**
     * 业务信息
     */
    @ImmutableRemark("业务信息")
    String BUSINESS = "3";

    /**
     * 安全信息，如：登陆密码错误次数过多等
     */
    @ImmutableRemark("安全信息")
    String SECURITY = "4";
}