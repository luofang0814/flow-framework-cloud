package com.flow.framework.facade.alarm.module.service;

import com.flow.framework.facade.alarm.constant.AlarmFacadeConstant;
import com.flow.framework.facade.alarm.pojo.dto.AlarmClearModuleDto;
import com.flow.framework.facade.alarm.pojo.dto.AlarmReportModuleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 告警服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@FeignClient(
        contextId = "framework-facade-alarm-alarmFrameworkModuleService",
        url = "${customization.framework.facade.alarm.url:}",
        name = "${customization.framework.facade.alarm.application-name:}",
        path = "${customization.framework.facade.alarm.rpc-prefix:}")
public interface IAlarmFrameworkModuleService {

    /**
     * 上报告警
     *
     * @param alarmReportModuleDto reportAlarmModuleDto
     */
    @GetMapping(path = AlarmFacadeConstant.ALARM_REPORT_URI)
    void reportAlarm(@RequestBody AlarmReportModuleDto alarmReportModuleDto);

    /**
     * 清除告警
     *
     * @param alarmClearModuleDto clearAlarmModuleDto
     */
    @PostMapping(path = AlarmFacadeConstant.ALARM_CLEAR_URI)
    void clearAlarm(@RequestBody AlarmClearModuleDto alarmClearModuleDto);
}
