package com.flow.framework.facade.alarm.enumeration;

import com.flow.framework.common.enumeration.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 告警类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Getter
@AllArgsConstructor
public enum AlarmType implements CodeEnum {

    /**
     * 系统交互
     */
    COMMUNICATION("1", "系统交互", "系统交互（包括外部系统和内部系统）"),

    /**
     * 环境信息
     */
    ENVIRONMENT("2", "环境信息", "环境信息（如：swap空间过小、内存占用过多等）"),

    /**
     * 业务信息
     */
    BUSINESS("3", "业务信息", "业务信息"),

    /**
     * 安全信息
     */
    SECURITY("4", "安全信息", "安全信息，如：登陆密码错误次数过多等");

    private String code;

    private String name;

    private String remark;
}
