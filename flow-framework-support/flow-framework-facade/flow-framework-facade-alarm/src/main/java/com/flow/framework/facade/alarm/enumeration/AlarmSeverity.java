package com.flow.framework.facade.alarm.enumeration;

import com.flow.framework.common.enumeration.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 告警级别
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Getter
@AllArgsConstructor
public enum AlarmSeverity implements CodeEnum {

    /**
     * 提示性告警
     */
    WARNING("1", "提示告警", "提示性告警"),

    /**
     * 一般性告警
     */
    MINOR("2", " 一般告警", " 一般性告警"),

    /**
     * 严重性告警
     */
    MAJOR("3", "严重告警", "一般性告警"),

    /**
     * 紧急性告警
     */
    CRITICAL("4", "紧急告警", "紧急性告警"),
    ;

    private String code;

    private String name;

    private String remark;
}
