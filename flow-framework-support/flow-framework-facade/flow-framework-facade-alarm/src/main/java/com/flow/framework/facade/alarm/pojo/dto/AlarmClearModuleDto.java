package com.flow.framework.facade.alarm.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 清除告警请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AlarmClearModuleDto extends BaseModuleDto {

    /**
     * 告警来源的模块名称
     */
    private String srcModuleName;

    /**
     * 告警来源类型，如：swap空间过小、xxx业务短信通知失败等（由业务侧自己定义）
     */
    private String sourceType;

    /**
     * 如果有实际对象，则为对象的id，否则为空
     */
    private String sourceId;

    /**
     * 告警清除事件
     */
    private LocalDateTime clearTime;
}
