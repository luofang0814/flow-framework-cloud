package com.flow.framework.facade.alarm.error;

/**
 * 告警模块错误码
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/3/19
 */
public interface AlarmErrorCode {

    /**
     * 告警级别类型错误
     */
    long ALARM_SEVERITY_TYPE_ERROR = 20001;

    /**
     * 告警类型错误
     */
    long ALARM_TYPE_ERROR = 20002;
}