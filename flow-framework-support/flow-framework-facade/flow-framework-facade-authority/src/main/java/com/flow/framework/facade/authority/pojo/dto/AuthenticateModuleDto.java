package com.flow.framework.facade.authority.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * 鉴权请求
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AuthenticateModuleDto extends BaseModuleDto {

    /**
     * 客户端主机ip
     */
    private List<String> hosts;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 请求的uri
     */
    private String uri;

    /**
     * 鉴权需要使用的header
     */
    private Map<String, List<String>> authorityHeaders;

    /**
     * 鉴权需要的uri中的params
     */
    private Map<String, List<String>> authorityParams;
}
