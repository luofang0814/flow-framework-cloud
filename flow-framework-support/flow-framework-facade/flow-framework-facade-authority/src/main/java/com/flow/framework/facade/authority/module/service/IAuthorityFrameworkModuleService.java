package com.flow.framework.facade.authority.module.service;

import com.flow.framework.core.pojo.vo.PageModuleVo;
import com.flow.framework.facade.authority.constant.AuthorityFacadeConstant;
import com.flow.framework.facade.authority.pojo.dto.AuthenticateModuleDto;
import com.flow.framework.facade.authority.pojo.vo.AuthenticateModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 鉴权服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@FeignClient(
        contextId = "framework-facade-authority-authorityFrameworkModuleService",
        url = "${customization.framework.facade.authority.url:}",
        name = "${customization.framework.facade.authority.application-name:}",
        path = "${customization.framework.facade.authority.rpc-prefix:}")
public interface IAuthorityFrameworkModuleService {

    /**
     * 鉴权
     *
     * @param authenticateModuleDto authenticateModuleDto
     * @return 鉴权结果
     */
    @PutMapping(path = AuthorityFacadeConstant.AUTHORITY_AUTHORITY_URI)
    AuthenticateModuleVo authenticate(@RequestBody AuthenticateModuleDto authenticateModuleDto);

    /**
     * 分页查询租户id
     *
     * @param currentPage 当前页数
     * @return 分页的租户id
     */
    @GetMapping(path = AuthorityFacadeConstant.PAGE_TENANT_IDS_URI)
    PageModuleVo<String> paginationTenantIds(@RequestParam("currentPage") Long currentPage);
}
