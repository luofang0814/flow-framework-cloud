package com.flow.framework.facade.authority.pojo.vo;

import com.flow.framework.core.pojo.vo.base.BaseModuleVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.annotation.Nullable;

/**
 * 鉴权结果
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AuthenticateModuleVo extends BaseModuleVo {

    /**
     * 是否允许访问
     */
    private boolean allowAccess;

    /**
     * 当前系统版本号
     */
    private Long systemVersion;

    /**
     * 需要路由到的目标主机ip或者域名
     */
    @Nullable
    private String routeHost;

    /**
     * 是否需要国际化
     */
    private boolean i18nRequired;

    /**
     * 用户上下文信息
     */
    private String userContext;

    /**
     * 证书信息
     */
    @Nullable
    private CertificationModuleVo certificationModuleVo;
}
