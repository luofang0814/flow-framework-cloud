package com.flow.framework.facade.authority.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 证书信息
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
@Data
@EqualsAndHashCode()
public class CertificationModuleVo implements Serializable {

    /**
     * 加签算法编码，当前支持：RV1，RV2，HMV1
     */
    private String signCipherCode;

    /**
     * 签名key，只有当签名code不是RV开始时，该值才生效
     */
    private String signKey;

    /**
     * 加解密算法编码，当前支持：RV1，RV2，SV1
     */
    private String contentCipherCode;

    /**
     * 对称加密的秘钥
     */
    private String symmetricKey;

    /**
     * 非对称加密的私钥
     */
    private String asymmetricPrivateKey;

    /**
     * 非对称加密的公钥
     */
    private String asymmetricPublicKey;
}