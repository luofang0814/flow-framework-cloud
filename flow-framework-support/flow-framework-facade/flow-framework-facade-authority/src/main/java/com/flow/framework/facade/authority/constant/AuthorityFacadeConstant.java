package com.flow.framework.facade.authority.constant;

/**
 * 鉴权facade常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/3
 */
public final class AuthorityFacadeConstant {

    /**
     * 鉴权
     */
    public static final String AUTHORITY_AUTHORITY_URI = "/authenticate";

    /**
     * 分页查询租户id
     */
    public static final String PAGE_TENANT_IDS_URI = "/pageTenantIds";
}