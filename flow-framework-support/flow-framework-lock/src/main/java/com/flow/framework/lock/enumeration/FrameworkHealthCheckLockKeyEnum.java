package com.flow.framework.lock.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 框架获取分布式锁获健康检查的key的枚举类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@AllArgsConstructor
@Getter
public enum FrameworkHealthCheckLockKeyEnum implements ILockKeyEnum {

    /**
     * Redisson健康检查
     */
    @SuppressWarnings("SpellCheckingInspection")
    FRAMEWORK_HEALTH_CHECK_LOCK_KEY(0, "redisson健康检查"),
    ;

    private int paramsSize;

    private String remark;
}
