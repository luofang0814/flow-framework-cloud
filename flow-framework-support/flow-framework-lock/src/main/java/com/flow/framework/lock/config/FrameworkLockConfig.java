package com.flow.framework.lock.config;

import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.lock.helper.LockHelperConfig;
import com.flow.framework.lock.properties.FrameworkLockConfigProperties;
import com.flow.framework.lock.service.system.health.impl.RedissonHealthCheckServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 框架锁模块配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@Configuration
public class FrameworkLockConfig {

    @Bean
    @ConditionalOnMissingBean
    @RefreshScope
    @ConfigurationProperties(prefix = "customization.framework.lock")
    FrameworkLockConfigProperties frameworkLockConfigProperties() {
        return new FrameworkLockConfigProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    LockHelperConfig lockHelperConfig(ISystemConfigPropertiesService systemConfigPropertiesService, FrameworkLockConfigProperties frameworkLockConfigProperties) {
        return new LockHelperConfig(systemConfigPropertiesService, frameworkLockConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    RedissonHealthCheckServiceImpl redissonHealthCheckService(FrameworkLockConfigProperties frameworkLockConfigProperties) {
        return new RedissonHealthCheckServiceImpl(frameworkLockConfigProperties);
    }
}
