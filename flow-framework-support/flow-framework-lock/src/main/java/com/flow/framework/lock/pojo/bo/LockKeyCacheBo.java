package com.flow.framework.lock.pojo.bo;

import lombok.Data;

/**
 * 分布式锁的key的缓存
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@Data
public class LockKeyCacheBo {

    /**
     * 上一个版本的锁的key
     */
    private String previousVersionLockKey;

    /**
     * 上一个版本的锁获取是否成功
     */
    private boolean previousVersionLockSuccess;

    /**
     * 当前版本的锁的key
     */
    private String currentVersionLockKey;

    /**
     * 当前版本的锁获取是否成功
     */
    private boolean currentVersionLockSuccess;
}
