package com.flow.framework.lock.service.lock;

/**
 * Redisson分布式锁接口，不要直接使用该接口对应的服务实现，建议使用com.flow.framework.lock.helper.LockHelper
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
public interface ILockService {

    /**
     * 根据时间和时间单位和等待时间加锁
     *
     * @param lockKey   lockKey
     * @param waitTime  waitTime
     * @param leaseTime 加锁时间，单位为毫秒
     * @return 获取锁是否成功
     */
    boolean tryLock(String lockKey, long waitTime, long leaseTime);

    /**
     * 释放锁
     *
     * @param lockKey lockKey
     */
    void unlock(String lockKey);
}
