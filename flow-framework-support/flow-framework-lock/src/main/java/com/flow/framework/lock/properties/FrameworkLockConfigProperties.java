package com.flow.framework.lock.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;

/**
 * 分布式锁配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@Data
public class FrameworkLockConfigProperties {

    /**
     * 是否启用锁
     */
    private Boolean enable;

    /**
     * redis配置
     */
    private RedisProperties redis;
}
