package com.flow.framework.lock.pojo.bo;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * 上一版本的锁的信息，该场景主要用于灰度发布，避免锁调整后和之前的锁没有关联导致正式环境和灰度环境各取各的锁
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
@Data
@AllArgsConstructor
public class PreviousVersionLockBo {

    /**
     * 微服务名称
     */
    private String appName;

    /**
     * 上一版本的锁的key的枚举
     */
    private ILockKeyEnum lockKeyEnum;

    /**
     * 上一版本的锁的参数
     */
    private List<String> lockParams;
}
