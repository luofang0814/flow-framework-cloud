package com.flow.framework.lock.enumeration;

/**
 * 分布式锁的key
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/13
 */
public interface ILockKeyEnum {

    /**
     * 获取锁的参数个数，如果为小于等于0，则表示没有参数
     *
     * @return 参数个数
     */
    int getParamsSize();

    /**
     * 获取备注
     *
     * @return 备注
     */
    String getRemark();
}
