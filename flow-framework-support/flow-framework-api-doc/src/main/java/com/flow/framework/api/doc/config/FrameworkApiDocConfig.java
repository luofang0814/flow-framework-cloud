package com.flow.framework.api.doc.config;

import com.fasterxml.classmate.TypeResolver;
import com.flow.framework.api.doc.manager.CustomizationSchemaPluginsManager;
import com.flow.framework.api.doc.plugins.CustomizationApiParamParameterBuilder;
import com.flow.framework.api.doc.plugins.CustomizationModelsProvider;
import com.flow.framework.api.doc.plugins.CustomizationResponseMessageReader;
import com.flow.framework.api.doc.properties.FrameworkApiDocConfigProperties;
import com.flow.framework.api.doc.service.web.impl.SwaggerResponseBodyAdviceServiceImpl;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.EnumTypeDeterminer;
import springfox.documentation.spi.schema.ModelBuilderPlugin;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.SyntheticModelProviderPlugin;
import springfox.documentation.spi.schema.contexts.ModelContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 框架Swagger配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/2/25
 */
@RequiredArgsConstructor
@Configuration
@EnableSwagger2
public class FrameworkApiDocConfig implements WebMvcConfigurer {

    @Bean
    @ConditionalOnMissingBean
    @RefreshScope
    @ConfigurationProperties(prefix = "customization.framework.api.doc")
    FrameworkApiDocConfigProperties frameworkWebConfigProperties() {
        return new FrameworkApiDocConfigProperties();
    }

    /**
     * 添加对应的资源处理器
     *
     * @param registry 资源注册器
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
        registry.addResourceHandler("/js/**").addResourceLocations("/js/");
        registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 跨域支持配置
     *
     * @param registry 跨域信息注册器
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowCredentials(true).allowedOrigins("*").allowedMethods("GET", "PUT", "DELETE", "POST", "OPTIONS").maxAge(3600);
    }

    @Bean
    @ConditionalOnMissingBean
    CustomizationModelsProvider customizationModelsProvider() {
        return new CustomizationModelsProvider();
    }

    @Bean
    @ConditionalOnMissingBean
    CustomizationResponseMessageReader customizationResponseMessageReader(TypeNameExtractor typeNameExtractor, TypeResolver typeResolver) {
        return new CustomizationResponseMessageReader(typeNameExtractor, typeResolver);
    }

    @Bean
    @ConditionalOnMissingBean
    SwaggerResponseBodyAdviceServiceImpl swaggerResponseBodyAdviceService(FrameworkApiDocConfigProperties frameworkApiDocConfigProperties) {
        return new SwaggerResponseBodyAdviceServiceImpl(frameworkApiDocConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    CustomizationApiParamParameterBuilder customizationApiParamParameterBuilder(DescriptionResolver descriptions,
                                                                                EnumTypeDeterminer enumTypeDeterminer) {
        return new CustomizationApiParamParameterBuilder(descriptions, enumTypeDeterminer);
    }

    @Bean
    @Primary
    @ConditionalOnMissingBean
    CustomizationSchemaPluginsManager customizationSchemaPluginsManager(
            @Qualifier("modelPropertyBuilderPluginRegistry")
                    PluginRegistry<ModelPropertyBuilderPlugin, DocumentationType> propertyEnrichers,
            @Qualifier("modelBuilderPluginRegistry")
                    PluginRegistry<ModelBuilderPlugin, DocumentationType> modelEnrichers,
            @Qualifier("syntheticModelProviderPluginRegistry")
                    PluginRegistry<SyntheticModelProviderPlugin, ModelContext> syntheticModelProviders,
            DescriptionResolver descriptions) {
        return new CustomizationSchemaPluginsManager(propertyEnrichers, modelEnrichers, syntheticModelProviders, descriptions);
    }

    @Bean
    @ConditionalOnMissingBean
    public Docket createRestApi(ISystemConfigPropertiesService systemConfigPropertiesService) {
        String profiles = systemConfigPropertiesService.getConfigValue("spring.profiles.active",
                "default");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build().groupName(profiles);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 标题
                .title("flow-framework-cloud")
                // 描述
                .description("api doc")
                // 作者信息
                .contact(new Contact("flow", "https://gitee.com/flow-projects/flow-framework-cloud", "528304020@qq.com"))
                // 服务网址
                .termsOfServiceUrl("https://gitee.com/flow-projects/flow-framework-cloud")
                // 版本
                .version("1.0.0")
                .build();
    }
}
