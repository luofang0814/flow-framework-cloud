package com.flow.framework.api.doc.util;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.enumeration.CodeEnum;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.google.common.base.Optional;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;


/**
 * 常量约束工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/3/19
 */
@Deprecated
@Slf4j
public class ImmutableUtil {

    /**
     * 获取需要额外添加的描述
     *
     * @param immutableCustomizationOptional immutableCustomizationOptional
     * @return
     */
    public static String getAdditionalDescription(Optional<ImmutableCustomization> immutableCustomizationOptional) {
        if (immutableCustomizationOptional.isPresent()) {
            ImmutableCustomization immutableCustomization = immutableCustomizationOptional.get();
            Class<? extends Immutable> immutableInterface = immutableCustomization.immutableInterface();
            if (!immutableInterface.isInterface()) {
                log.error("immutable interface isn't interface type. clazz name : {}", immutableInterface.getSimpleName());
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
            StringBuilder additionalDescription = new StringBuilder("（枚举信息：");
            Field[] fields = immutableInterface.getFields();
            try {
                if (VerifyUtil.isEmpty(fields)) {
                    log.error("immutable interface field is empty. clazz name : {}", immutableInterface.getSimpleName());
                    throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
                }
                for (Field field : fields) {
                    Object o = field.get(null);
                    additionalDescription.append(o);
                    ImmutableRemark immutableRemark = field.getAnnotation(ImmutableRemark.class);
                    if (null != immutableRemark) {
                        additionalDescription.append("-").append(immutableRemark.value()).append(";");
                    } else {
                        log.error("immutable interface field need immutable remark annotation. clazz name : {}", immutableInterface.getSimpleName());
                        throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
                    }
                }
                additionalDescription.deleteCharAt(additionalDescription.length() - 1).append("）");
                return additionalDescription.toString();
            } catch (Exception e) {
                log.error("construct immutable interface field error. clazz name : {}", immutableInterface.getSimpleName());
                throw new CheckedException(SystemErrorCode.UNEXPECTED_ERROR);
            }
        }
        return FrameworkCommonConstant.EMPTY_STRING;
    }

    /**
     * 获取样例值
     *
     * @param immutableCustomizationOptional immutableCustomizationOptional
     * @return
     */
    public static String getExample(Optional<ImmutableCustomization> immutableCustomizationOptional) {
        if (immutableCustomizationOptional.isPresent()) {
            ImmutableCustomization immutableCustomization = immutableCustomizationOptional.get();
            Class<? extends Immutable> immutableInterface = immutableCustomization.immutableInterface();
            if (!immutableInterface.isInterface()) {
                log.error("immutable interface isn't interface type. clazz name : {}", immutableInterface.getSimpleName());
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
            Field[] fields = immutableInterface.getFields();
            try {
                if (VerifyUtil.isEmpty(fields)) {
                    log.error("immutable interface field is empty. clazz name : {}", immutableInterface.getSimpleName());
                    throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
                }
                for (Field field : fields) {
                    Object o = field.get(null);
                    ImmutableRemark immutableRemark = field.getAnnotation(ImmutableRemark.class);
                    if (null != immutableRemark) {
                        return String.valueOf(o);
                    } else {
                        log.error("immutable interface field need immutable remark annotation. clazz name : {}", immutableInterface.getSimpleName());
                        throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
                    }
                }
            } catch (Exception e) {
                log.error("construct immutable interface field error. clazz name : {}", immutableInterface.getSimpleName());
                throw new CheckedException(SystemErrorCode.UNEXPECTED_ERROR);
            }
        }
        return FrameworkCommonConstant.EMPTY_STRING;
    }

    /**
     * 获取枚举类额外添加的描述信息
     *
     * @param clazz clazz
     * @return
     */
    public static String getAdditionalDescription(Class<?> clazz) {
        if (clazz.isEnum()) {
            StringBuilder additionalDescription = new StringBuilder("（枚举信息：");
            Object[] enums = clazz.getEnumConstants();
            if (CodeEnum.class.isAssignableFrom(clazz)) {
                for (Object singleEnum : enums) {
                    CodeEnum codeEnum = (CodeEnum) singleEnum;
                    additionalDescription.append(codeEnum.getCode()).append("-").append(codeEnum.getName()).append(";");
                }
            } else {
                for (Object singleEnum : enums) {
                    additionalDescription.append(singleEnum).append(";");
                }
            }
            additionalDescription.deleteCharAt(additionalDescription.length() - 1).append("）");
            return additionalDescription.toString();
        }
        return FrameworkCommonConstant.EMPTY_STRING;
    }

    /**
     * 获取样例值
     *
     * @param clazz clazz
     * @return
     */
    public static String getExample(Class<?> clazz) {
        if (clazz.isEnum()) {
            Object[] enums = clazz.getEnumConstants();
            if (CodeEnum.class.isAssignableFrom(clazz)) {
                for (Object singleEnum : enums) {
                    CodeEnum codeEnum = (CodeEnum) singleEnum;
                    return "\"" + codeEnum.getCode() + "\"";
                }
            } else {
                for (Object singleEnum : enums) {
                    return "\"" + singleEnum.toString() + "\"";
                }
            }
        }
        return FrameworkCommonConstant.EMPTY_STRING;
    }
}