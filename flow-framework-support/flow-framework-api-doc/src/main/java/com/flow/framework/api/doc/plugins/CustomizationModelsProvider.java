package com.flow.framework.api.doc.plugins;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.flow.framework.core.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationModelsProviderPlugin;
import springfox.documentation.spi.service.contexts.RequestMappingContext;
import springfox.documentation.spring.web.readers.operation.OperationModelsProvider;

import java.util.List;

import static springfox.documentation.schema.ResolvedTypes.resolvedTypeSignature;

/**
 * 模型解析器 参考springfox.documentation.spring.web.readers.operation.OperationModelsProvider
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/2/12
 */
public class CustomizationModelsProvider implements OperationModelsProviderPlugin {

    private static final Logger LOG = LoggerFactory.getLogger(OperationModelsProvider.class);

    @Override
    public void apply(RequestMappingContext context) {
        collectFromReturnType(context);
        collectParameters(context);
        collectGlobalModels(context);
    }

    private void collectGlobalModels(RequestMappingContext context) {
        for (ResolvedType each : context.getAdditionalModels()) {
            TypeResolver resolver = new TypeResolver();

            // 如果直接对外的controller响应（不包含rpc的）不是框架响应，则使用框架响应对controller响应进行包装
            if (!each.isInstanceOf(Response.class)) {
                each = resolver.resolve(Response.class, each);
            }
            context.operationModelsBuilder().addInputParam(each);
            context.operationModelsBuilder().addReturn(each);
        }
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return true;
    }

    private void collectFromReturnType(RequestMappingContext context) {
        ResolvedType modelType = context.getReturnType();
        modelType = context.alternateFor(modelType);
        TypeResolver resolver = new TypeResolver();

        // 如果直接对外的controller响应（不包含rpc的）不是框架响应，则使用框架响应对controller响应进行包装
        if (!modelType.isInstanceOf(Response.class)) {
            modelType = resolver.resolve(Response.class, modelType);
        }
        LOG.debug("Adding return parameter of type {}", resolvedTypeSignature(modelType).or("<null>"));
        context.operationModelsBuilder().addReturn(modelType);
    }

    private void collectParameters(RequestMappingContext context) {


        LOG.debug("Reading parameters models for handlerMethod |{}|", context.getName());

        List<ResolvedMethodParameter> parameterTypes = context.getParameters();
        for (ResolvedMethodParameter parameterType : parameterTypes) {
            if (parameterType.hasParameterAnnotation(RequestBody.class)
                    || parameterType.hasParameterAnnotation(RequestPart.class)) {
                ResolvedType modelType = context.alternateFor(parameterType.getParameterType());
                LOG.debug("Adding input parameter of type {}", resolvedTypeSignature(modelType).or("<null>"));
                context.operationModelsBuilder().addInputParam(modelType);
            }
        }
        LOG.debug("Finished reading parameters models for handlerMethod |{}|", context.getName());
    }
}
