package com.flow.framework.api.doc.properties;

import lombok.Data;

/**
 * api文档相关配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/2/25
 */
@Data
public class FrameworkApiDocConfigProperties {

    private boolean enableApiDoc = false;
}
