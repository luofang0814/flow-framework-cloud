package com.flow.framework.config.system.listener;

import com.flow.framework.core.system.initialization.ApplicationContextHelper;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import org.springframework.cloud.endpoint.event.RefreshEvent;
import org.springframework.context.ApplicationListener;

import java.util.Collection;

/**
 * 系统刷新监听器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/5
 */
public class SystemRefreshListener implements ApplicationListener<RefreshEvent> {
    @Override
    public void onApplicationEvent(RefreshEvent event) {
        Collection<ISystemLifecycleListener> beans = ApplicationContextHelper.getBeans(ISystemLifecycleListener.class);
        for (ISystemLifecycleListener listener : beans) {
            listener.onRefresh();
        }
    }
}
