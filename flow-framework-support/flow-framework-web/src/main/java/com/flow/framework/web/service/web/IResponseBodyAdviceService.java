package com.flow.framework.web.service.web;

/**
 * 响应body处理服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/25
 */
public interface IResponseBodyAdviceService {

    /**
     * 是否匹配
     *
     * @param contextPath contextPath
     * @param method      method
     * @param uriPath     uriPath
     * @param body        body
     * @return
     */
    boolean isMatch(String contextPath, String method, String uriPath, Object body);

    /**
     * 获取响应body
     *
     * @param body body
     * @return
     */
    Object getResponseBody(Object body);
}