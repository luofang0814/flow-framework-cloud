package com.flow.framework.web.constant;

/**
 * web模块的常量定义
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/24
 */
public final class FrameworkWebConstant {

    /**
     * 包含RPC调用的URI特征
     */
    public static final String RPC_URI_CHARACTERISTIC = "/rpc/";

    /**
     * 默认出错时请求转发的PATH
     */
    public static final String DEFAULT_ERROR_PATH = "/error";

    /**
     * server.servlet.context-path表达式
     */
    public static final String CONTEXT_PATH_EXPRESSION = "${server.servlet.context-path}";
}
