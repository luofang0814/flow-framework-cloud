package com.flow.framework.mq.constant;

/**
 * MQ常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
public final class FrameworkMqConstant {

    /**
     * mq消息发送时间
     */
    public static final String MQ_SEND_DATE_TIME_KEY = "customization_mq_send_date_time";

    /**
     * 消息队列相关组合分割线
     */
    public static final String MQ_COMBINATION_SPLIT_LINE = "-";
}
