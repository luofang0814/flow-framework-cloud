package com.flow.framework.mq.config;

import com.flow.framework.core.pojo.dto.base.notify.BaseNotifyDto;
import com.flow.framework.core.properties.FrameworkCoreConfigProperties;
import com.flow.framework.core.toolkit.IdentifierGeneratorSequence;
import com.flow.framework.facade.mq.module.service.IMqFrameworkModuleService;
import com.flow.framework.mq.consumer.IConsumer;
import com.flow.framework.mq.producer.MessageQueueClient;
import com.flow.framework.mq.service.system.health.impl.QueueHealthCheckServiceImpl;
import com.flow.framework.mq.system.listener.lifecycle.MessageQueueLifecycleListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.amqp.rabbit.retry.ImmediateRequeueMessageRecoverer;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 框架消息队列配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/20
 */
@Slf4j
@Configuration
public class FrameworkMqConfig {

    @Bean
    @ConditionalOnMissingBean
    MessageRecoverer messageRecoverer() {
        return new ImmediateRequeueMessageRecoverer();
    }

    @Bean
    @ConditionalOnMissingBean
    MessageQueueClient messageQueueClient(RabbitTemplate rabbitTemplate, IdentifierGeneratorSequence identifierGeneratorSequence) {
        return new MessageQueueClient(rabbitTemplate, identifierGeneratorSequence);
    }

    @Bean
    @ConditionalOnMissingBean
    QueueHealthCheckServiceImpl queueHealthCheckService(RabbitTemplate rabbitTemplate) {
        return new QueueHealthCheckServiceImpl(rabbitTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    MessageQueueLifecycleListener messageQueueLifecycleListener(@Autowired(required = false) List<IConsumer<? extends BaseNotifyDto>> consumers,
                                                                @Autowired(required = false) ConnectionFactory connectionFactory,
                                                                @Autowired(required = false) RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry,
                                                                @Autowired(required = false) RabbitListenerContainerFactory<?> factory,
                                                                @Autowired(required = false) FrameworkCoreConfigProperties frameworkCoreConfigProperties,
                                                                IMqFrameworkModuleService mqFrameworkModuleService) {
        return new MessageQueueLifecycleListener(consumers, connectionFactory, rabbitListenerEndpointRegistry, factory,
                frameworkCoreConfigProperties, mqFrameworkModuleService);
    }
}
