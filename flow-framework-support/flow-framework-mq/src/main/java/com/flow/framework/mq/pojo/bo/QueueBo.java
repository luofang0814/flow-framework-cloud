package com.flow.framework.mq.pojo.bo;

import lombok.Getter;

/**
 * 消息队列信息
 * 针对不同的MQ使用不同的bizCode和handleMsgServiceCode组合，基本可以满足当前市场上的MQ需要的参数
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/26
 */
@Getter
public class QueueBo {

    /**
     * 业务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为业务类型编码，如：toleration:experiment:computer_apply
     */
    private String bizCode;

    /**
     * 处理消息的服务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为接收方处理消息的服务编码，如：toleration:experiment:computer_apply_approval_notify
     */
    private String handleMsgServiceCode;

    /**
     * 构造器
     *
     * @param bizCode              业务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为业务类型编码，如：toleration:experiment:computer_apply
     * @param handleMsgServiceCode 处理消息的服务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为接收方处理消息的服务编码，如：toleration:experiment:computer_apply_approval_notify
     */
    private QueueBo(String bizCode, String handleMsgServiceCode) {
        this.bizCode = bizCode;
        this.handleMsgServiceCode = handleMsgServiceCode;
    }

    /**
     * 构造队列信息
     *
     * @param bizCode              业务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为业务类型编码，如：toleration:experiment:computer_apply
     * @param handleMsgServiceCode 处理消息的服务编码，第一段为接收消息的模块名，第二段为发送消息的模块名，第三段为接收方处理消息的服务编码，如：toleration:experiment:computer_apply_approval_notify
     */
    public static QueueBo build(String bizCode, String handleMsgServiceCode) {
        return new QueueBo(bizCode, handleMsgServiceCode);
    }
}