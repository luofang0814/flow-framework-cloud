package com.flow.framework.mq.consumer;

import com.flow.framework.common.type.TypeReference;
import com.flow.framework.core.pojo.dto.base.notify.BaseNotifyDto;
import com.flow.framework.mq.pojo.bo.QueueBo;

import java.util.Map;

/**
 * MQ消息消费端
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/26
 */
public interface IConsumer<T extends BaseNotifyDto> {

    /**
     * 当收到消息时会调用该方法
     *
     * @param messageId 消息id
     * @param headers   headers
     * @param dto       消息内容
     */
    void onMessage(String messageId, Map<String, Object> headers, T dto);

    /**
     * 获取队列信息
     *
     * @return 队列信息
     */
    QueueBo getQueueBo();

    /**
     * 获取消息类型
     *
     * @return 消息类型引用
     */
    TypeReference<T> getMessageType();
}
