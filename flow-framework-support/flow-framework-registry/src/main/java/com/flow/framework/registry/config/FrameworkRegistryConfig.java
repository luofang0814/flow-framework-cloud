package com.flow.framework.registry.config;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.registry.NacosAutoServiceRegistration;
import com.flow.framework.registry.system.listener.lifecycle.NacosRegistryLifecycleListener;
import com.flow.framework.registry.service.system.health.impl.NacosRegistryCenterHealthCheckServiceImpl;
import com.flow.framework.registry.service.system.health.impl.NacosRegistryClientHealthCheckServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 框架注册中心模块配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/5
 */
@Configuration
public class FrameworkRegistryConfig {

    @Bean
    @ConditionalOnMissingBean
    NacosRegistryCenterHealthCheckServiceImpl nacosRegistryCenterHealthCheckService(NacosDiscoveryProperties nacosDiscoveryProperties) {
        return new NacosRegistryCenterHealthCheckServiceImpl(nacosDiscoveryProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    NacosRegistryClientHealthCheckServiceImpl nacosRegistryClientHealthCheckService(NacosAutoServiceRegistration nacosAutoServiceRegistration) {
        return new NacosRegistryClientHealthCheckServiceImpl(nacosAutoServiceRegistration);
    }

    @Bean
    @ConditionalOnMissingBean
    NacosRegistryLifecycleListener nacosRegistryLifecycleListener(@Autowired(required = false) NacosAutoServiceRegistration nacosAutoServiceRegistration) {
        return new NacosRegistryLifecycleListener(nacosAutoServiceRegistration);
    }
}
