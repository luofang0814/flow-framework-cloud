package com.flow.framework.registry.system.listener.lifecycle;

import com.alibaba.cloud.nacos.registry.NacosAutoServiceRegistration;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * nacos客户端生命周期监听器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/5
 */
@Slf4j
@RequiredArgsConstructor
public class NacosRegistryLifecycleListener implements ISystemLifecycleListener {

    private final NacosAutoServiceRegistration nacosAutoServiceRegistration;

    /**
     * @inheritDoc
     */
    @Override
    public void beforeShutdown() {
        if (VerifyUtil.isEmpty(nacosAutoServiceRegistration)) {
            log.info("registry service is empty.");
            throw new CheckedException(SystemErrorCode.OBJECT_NOT_FOUND_ERROR);
        }
        nacosAutoServiceRegistration.stop();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void manualOnline() {
        if (VerifyUtil.isEmpty(nacosAutoServiceRegistration)) {
            log.info("registry service is empty.");
            throw new CheckedException(SystemErrorCode.OBJECT_NOT_FOUND_ERROR);
        }
        nacosAutoServiceRegistration.start();
    }
}
