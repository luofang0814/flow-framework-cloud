package com.flow.framework.module.call.constant;

/**
 * 模块调用常量类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
public final class FrameworkModuleCallConstant {

    /**
     * 内部请求标识，通过分布式事务代理后，feign.Request#requestTemplate可能为空
     */
    public static final String INNER_REQUEST_KEY = "customization_inner_module_call_request";
}
