package com.flow.framework.module.call.rpc.properties;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 框架远程调用配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
public class FrameworkRpcConfigProperties {

    /**
     * 内部请求的ip或者域名
     */
    private List<String> innerRequestHosts = new ArrayList<>();
}
