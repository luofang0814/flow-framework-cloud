package com.flow.framework.module.call.aspect;

import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.clazz.ClazzUtil;
import com.flow.framework.common.util.verify.VerifyUtil;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * 禁止分布式事务切面
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
@Slf4j
@Aspect
public class ForbidDistributedTransactionAspect {

    @Pointcut("@annotation(com.flow.framework.module.call.annotation.ForbidDistributedTransaction)")
    public void pointcut() {
    }

    @Before("pointcut()")
    public void before(JoinPoint joinPoint) {
        String xid = RootContext.getXID();
        if (!VerifyUtil.isEmpty(xid)) {
            Signature signature = joinPoint.getSignature();
            String method = signature.toString();
            String typeName = signature.getDeclaringTypeName();
            String shortMethod = method.replace(typeName, ClazzUtil.getShortByClazzName(typeName));
            log.error("method can't support distributed transaction, method : {}", shortMethod);
            throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
        }
    }
}