package com.flow.framework.module.call.rpc.proto;

/**
 * 将feign代理的方法的void返回值替换为自定CustomizationVoid类型，因为void时feign不会调用序列化
 * 方便在com.flow.framework.rpc.decoder.CustomizationRpcSuccessDecoder中对其他数据进行校验，如响应header中的框架响应
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
public final class CustomizationVoid {
}