package com.flow.framework.module.call.rpc.message;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.flow.framework.common.datetime.CommonDateTimeFormatter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * jackson 自定义时间转换器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
public class FeignObjectMessageConverter extends AbstractJackson2HttpMessageConverter {

    private static final Jackson2ObjectMapperBuilder OBJECT_MAPPER_BUILDER = Jackson2ObjectMapperBuilder.json()
            .serializerByType(LocalDateTime.class,
                    new LocalDateTimeSerializer(CommonDateTimeFormatter.COMMON_DATETIME_FORMATTER))
            .serializerByType(LocalDate.class,
                    new LocalDateSerializer(CommonDateTimeFormatter.COMMON_DATE_FORMATTER))
            .deserializerByType(LocalDateTime.class,
                    new LocalDateTimeDeserializer(CommonDateTimeFormatter.COMMON_DATETIME_FORMATTER))
            .deserializerByType(LocalDate.class,
                    new LocalDateDeserializer(CommonDateTimeFormatter.COMMON_DATE_FORMATTER));

    public FeignObjectMessageConverter() {
        super(OBJECT_MAPPER_BUILDER.build(), MediaType.APPLICATION_JSON);
    }
}
