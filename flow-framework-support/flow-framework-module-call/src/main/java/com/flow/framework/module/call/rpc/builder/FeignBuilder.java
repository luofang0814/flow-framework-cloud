package com.flow.framework.module.call.rpc.builder;

import com.flow.framework.module.call.rpc.manager.FeignManager;
import feign.Feign;
import feign.Target;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * feign的构建器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
@RequiredArgsConstructor
public class FeignBuilder extends Feign.Builder {

    private final FeignManager feignManager;

    @Override
    public <T> T target(Target<T> target) {
        FeignClient feignClient = target.type().getDeclaredAnnotation(FeignClient.class);
        feignManager.registerFeignClient(target, feignClient);
        return super.target(target);
    }
}