package com.flow.framework.module.call.properties;

import com.flow.framework.module.call.rpc.properties.FrameworkRpcConfigProperties;
import lombok.Data;

/**
 * 框架模块调用配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/4
 */
@Data
public class FrameworkModuleCallConfigProperties {

    /**
     * rpc配置
     */
    private FrameworkRpcConfigProperties rpc;
}
