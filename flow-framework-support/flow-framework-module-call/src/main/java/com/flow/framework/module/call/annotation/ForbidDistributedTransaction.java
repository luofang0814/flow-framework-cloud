package com.flow.framework.module.call.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 禁止分布式事务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/5
 */
@Documented
@Target({METHOD})
@Retention(RUNTIME)
public @interface ForbidDistributedTransaction {
}