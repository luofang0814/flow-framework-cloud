package com.flow.framework.module.call.rpc.encoder;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;

/**
 * RPC调用编码器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/2/19
 */
public class CustomizationRpcEncoder extends SpringEncoder {

    public CustomizationRpcEncoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        super(messageConverters);
    }
}
