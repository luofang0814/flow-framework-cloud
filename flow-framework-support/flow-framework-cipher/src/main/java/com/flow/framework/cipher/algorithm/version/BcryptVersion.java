package com.flow.framework.cipher.algorithm.version;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.enumeration.CodeEnum;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Bcrypt算法类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/16
 */
@Getter
@Slf4j
@AllArgsConstructor
@SuppressWarnings("SpellCheckingInspection")
public enum BcryptVersion implements CodeEnum {

    /**
     * BV1，枚举code必须是BV开头
     */
    BV1("BV1", "BV1", "BV1", BCryptPasswordEncoder.BCryptVersion.$2A),
    ;

    // 枚举code必须是BV开头
    static {
        BcryptVersion[] values = BcryptVersion.values();
        for (BcryptVersion bcryptVersion : values) {
            if (!bcryptVersion.getCode().startsWith(FrameworkCommonConstant.BV_CIPHER)) {
                log.error("cipher name error.");
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
        }
    }


    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String remark;

    /**
     * 版本
     */
    private BCryptPasswordEncoder.BCryptVersion version;
}
