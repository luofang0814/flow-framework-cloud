package com.flow.framework.cipher.algorithm.version;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.enumeration.CodeEnum;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自定义RSA类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/16
 */
@Getter
@Slf4j
@AllArgsConstructor
public enum RsaVersion implements CodeEnum {

    /**
     * RV1，枚举code必须是RV开头
     */
    RV1("RV1", "RV1", "RV1", 2048,
            "RSA/ECB/PKCS1Padding", "SHA256withRSA", 117, 256),

    /**
     * RV2，枚举code必须是RV开头
     */
    RV2("RV2", "RV2", "RV2", 2048,
            "RSA/ECB/PKCS1Padding", "SHA512withRSA", 117, 256),
    ;

    // 枚举名称必须是RV开头
    static {
        RsaVersion[] values = RsaVersion.values();
        for (RsaVersion rsaVersion : values) {
            if (!rsaVersion.getCode().startsWith(FrameworkCommonConstant.RV_CIPHER)) {
                log.error("cipher name error.");
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
        }
    }

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String remark;

    /**
     * 秘钥key的大小
     */
    private int keySize;

    /**
     * 密文算法
     */
    private String cipherAlgorithm;

    /**
     * 加签算法
     */
    private String signatureAlgorithm;

    /**
     * 加密数据处理量
     */
    private int encryptBlockSize;

    /**
     * 解密数据处理量
     */
    private int decryptBlockSize;
}
