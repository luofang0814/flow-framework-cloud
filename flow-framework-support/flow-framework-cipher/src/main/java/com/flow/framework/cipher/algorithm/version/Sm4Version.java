package com.flow.framework.cipher.algorithm.version;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.enumeration.CodeEnum;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 自定义SM4类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/16
 */
@Getter
@Slf4j
@AllArgsConstructor
public enum Sm4Version implements CodeEnum {
    /**
     * SV1，枚举code必须是SV开头
     */
    SV1("SV1", "SV1", "SV1", 128,
            "SM4/ECB/PKCS7Padding", 4096, 4112),
    ;

    // 枚举code必须是SV开头
    static {
        Sm4Version[] values = Sm4Version.values();
        for (Sm4Version sm4Version : values) {
            if (!sm4Version.getCode().startsWith(FrameworkCommonConstant.SV_CIPHER)) {
                log.error("cipher name error.");
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
        }
    }

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String remark;

    /**
     * 秘钥key的大小
     * 64-16位16进制；128-32位16进制；256-64位16进制
     */
    private int keySize;

    /**
     * 加密算法/分组加密模式/分组填充方式
     * PKCS5Padding-以8个字节为一组进行分组加密
     * 定义分组加密模式使用：PKCS5Padding
     */
    private String cipherAlgorithm;

    /**
     * 加密数据处理量
     */
    private int encryptBlockSize;

    /**
     * 解密数据处理量
     */
    private int decryptBlockSize;
}
