package com.flow.framework.cipher.pojo.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 加密秘钥对
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/16
 */
@Getter
@AllArgsConstructor
public class CipherKeyPairBo {

    private String publicKey;

    private String privateKey;
}
