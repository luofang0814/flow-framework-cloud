package com.flow.framework.base.properties.component;

import lombok.Data;

import java.util.List;

/**
 * 指定三方调用网络监控配置（基于TCP的监控）
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class AllocationRemoteConfigProperties {

    private List<Remote> remotes;

    @Data
    public static class Remote {

        /**
         * ip或者域名
         */
        private String host;

        /**
         * 端口
         */
        private int port;
    }
}
