package com.flow.framework.base.properties.component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

/**
 * cpu负载、内存利用率和磁盘空间检查配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class OperatingSystemConfigProperties {

    /**
     * CPU利用率一般不超过65%
     */
    private String cpuLoadDeadLinePercent = "0.65";

    /**
     * 内存利用率一般不超过75%
     */
    private String memoryUsageDeadLinePercent = "0.75";

    /**
     * 磁盘空间检查
     */
    private List<DiskUsage> diskUsage = Collections.singletonList(
            new DiskUsage("0.80", ".")
    );

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DiskUsage {

        /**
         * 磁盘使用率一般不超过80%
         */
        private String deadLinePercent = "0.80";

        /**
         * 磁盘路径
         */
        private String path;
    }
}
