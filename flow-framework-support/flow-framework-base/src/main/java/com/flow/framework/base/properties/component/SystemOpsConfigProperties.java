package com.flow.framework.base.properties.component;

import lombok.Data;

/**
 * 系统敏感操作安全配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class SystemOpsConfigProperties {

    /**
     * 入访秘钥在header中的key
     */
    private String accessHeaderKey;

    /**
     * 入访秘钥
     */
    private String accessSecret;

    /**
     * 是否采用签名模式，如果为签名模式，则校验方式为请求header中的请求时间加换行符再加accessSecret采用Hmac签名
     */
    private boolean signVerify = true;

    /**
     * 请求中传递请求时间的header key
     */
    private String requestTimeHeaderKey;

    /**
     * 签名时间有效期
     */
    private long duration = 30000;
}
