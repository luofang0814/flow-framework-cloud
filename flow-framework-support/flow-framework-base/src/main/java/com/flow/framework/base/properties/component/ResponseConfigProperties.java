package com.flow.framework.base.properties.component;

import lombok.Data;

/**
 * 响应相关配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class ResponseConfigProperties {


    /**
     * 加工响应body的最大值，默认512kb，如：验签，解密等
     */
    private int processMaxBodySize = 512 * 1024;

    /**
     * 系统编码，如果没有配置，则取微服务端口；起作用场景为：1、在国际化时会使用此标志区分国际化来源系统标识；2、在系统响应中会带上系统编码标识
     */
    private String systemCode;

    /**
     * 响应持久化记录的最大长度
     */
    private int recordPersistenceMaxLength = 10240;

    /**
     * 响应本地系统日志记录的最大长度
     */
    private int recordLocalMaxLength = 10240;

    /**
     * 国际化错误默认展示的提示信息
     */
    private String i18nErrorDefaultMsg = "system error. please contact your system carrier";
}
