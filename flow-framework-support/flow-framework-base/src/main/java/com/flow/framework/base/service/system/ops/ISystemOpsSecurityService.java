package com.flow.framework.base.service.system.ops;

import java.util.List;
import java.util.Map;

/**
 * 系统敏感操作安全服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
public interface ISystemOpsSecurityService {

    /**
     * uri是否匹配
     *
     * @param contextPath contextPath
     * @param method      method
     * @param uri         uri
     * @return
     */
    boolean isUriMatch(String contextPath, String method, String uri);

    /**
     * 认证
     *
     * @param headers headers
     * @return
     */
    boolean authentication(Map<String, List<String>> headers);
}