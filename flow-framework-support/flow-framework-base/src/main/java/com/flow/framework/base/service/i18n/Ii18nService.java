package com.flow.framework.base.service.i18n;

import java.util.Locale;

/**
 * 国际化服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
public interface Ii18nService {

    /**
     * 根据地区和国际化key展示语言
     *
     * @param serviceName serviceName
     * @param key     key
     * @param locale  locale
     * @return
     */
    String getInternationalValue(String serviceName, String key, Locale locale);

    /**
     * 记录未被国际化的key
     *
     * @param serviceName serviceName
     * @param key     key
     * @param locale  locale
     * @return
     */
    void recordUnInternationalKey(String serviceName, String key, Locale locale);
}
