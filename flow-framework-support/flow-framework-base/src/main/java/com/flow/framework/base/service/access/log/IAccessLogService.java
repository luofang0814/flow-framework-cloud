package com.flow.framework.base.service.access.log;

import com.flow.framework.facade.access.log.opt.annotation.OptLog;
import com.flow.framework.facade.access.log.pojo.dto.OptLogModuleDto;
import com.flow.framework.facade.access.log.pojo.dto.TraceLogModuleDto;
import org.springframework.lang.Nullable;

/**
 * 日志记录服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
public interface IAccessLogService {

    /**
     * 匹配操作日志的注解
     *
     * @param method 请求方法
     * @param uri    请求的uri
     * @return
     */
    @Nullable
    OptLog matchOptLogAnnotation(String method, String uri);

    /**
     * 异步记录操作日志
     *
     * @param optLogModuleDto optLogModuleDto
     */
    void asyncRecordOptLog(OptLogModuleDto optLogModuleDto);

    /**
     * 记录请求日志
     *
     * @param traceLogModuleDto 请求日志记录请求
     */
    void asyncRecordTraceLog(TraceLogModuleDto traceLogModuleDto);
}
