package com.flow.framework.base.constant;

/**
 * 常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
public final class FrameworkBaseConstant {

    /**
     * 去除context path的健康检查uri
     */
    public static final String NO_CONTEXT_PATH_HEALTH_CHECK_URI = "/system/ops/cloud/health/check";

    /**
     * 去除context path的终止前预处理uri
     */
    public static final String NO_CONTEXT_PATH_BEFORE_SHUTDOWN_URI = "/system/ops/cloud/before/shutdown";

    /**
     * 去除context path的上线uri
     */
    public static final String NO_CONTEXT_PATH_MANUAL_ONLINE_URI = "/system/ops/cloud/manual/online";

    /**
     * 自定义请求上下文
     */
    public static final String REQUEST_RESPONSE_CONTEXT_KEY = "customization_request_response_context";
}
