package com.flow.framework.base.service.system.health.impl;

import com.flow.framework.base.service.alarm.IAlarmService;
import com.flow.framework.common.health.ServiceHealthCheckCode;
import com.flow.framework.common.pojo.vo.system.ServiceHealthVo;
import com.flow.framework.common.service.system.health.IHealthCheckService;
import com.flow.framework.core.system.helper.SystemHealthHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;

/**
 * 告警服务健康检查
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/3/5
 */
@Slf4j
@RequiredArgsConstructor
public class AlarmHealthCheckServiceImpl implements IHealthCheckService {

    private final IAlarmService alarmService;

    @Override
    @SuppressWarnings("deprecation")
    public List<ServiceHealthVo> check() {
        try {
            boolean health = alarmService.getAndResetHealth();
            if (!health) {
                log.error("alarm service occurred error.");
                return Collections.singletonList(SystemHealthHelper.unhealthy(ServiceHealthCheckCode.BIZ_HEALTH_CHECK_CODE,
                        "alarm service occurred error", null));
            }
            return Collections.singletonList(SystemHealthHelper.healthy(ServiceHealthCheckCode.BIZ_HEALTH_CHECK_CODE));
        } catch (Exception e) {
            log.error("alarm service occurred error.", e);
            return Collections.singletonList(
                    SystemHealthHelper.unhealthy(ServiceHealthCheckCode.BIZ_HEALTH_CHECK_CODE, e.getMessage(), null)
            );
        }
    }
}
