package com.flow.framework.base.config;

import com.flow.framework.base.controller.SystemOpsController;
import com.flow.framework.base.helper.i18n.I18nHelper;
import com.flow.framework.base.properties.FrameworkBaseConfigProperties;
import com.flow.framework.base.service.access.log.IAccessLogService;
import com.flow.framework.base.service.access.log.impl.AccessLogServiceImpl;
import com.flow.framework.base.service.alarm.IAlarmService;
import com.flow.framework.base.service.alarm.impl.AlarmServiceImpl;
import com.flow.framework.base.service.i18n.Ii18nService;
import com.flow.framework.base.service.i18n.impl.I18nServiceImpl;
import com.flow.framework.base.service.response.impl.ResponseI18nServiceImpl;
import com.flow.framework.base.service.system.health.impl.AlarmHealthCheckServiceImpl;
import com.flow.framework.base.service.system.health.impl.OperatingSystemHealthCheckServiceImpl;
import com.flow.framework.base.service.system.ops.ISystemOpsSecurityService;
import com.flow.framework.base.service.system.ops.impl.SystemOpsSecurityServiceImpl;
import com.flow.framework.base.system.listener.lifecycle.NetworkStatusListenerLifecycleListener;
import com.flow.framework.common.service.system.health.IHealthCheckService;
import com.flow.framework.core.properties.FrameworkCoreConfigProperties;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.service.response.IResponseI18nService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import com.flow.framework.facade.access.log.module.service.IAccessLogFrameworkModuleService;
import com.flow.framework.facade.alarm.module.service.IAlarmFrameworkModuleService;
import com.flow.framework.facade.i18n.module.service.Ii18nFrameworkModuleService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.composite.CompositeDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * 框架基础模块配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Configuration
public class FrameworkBaseConfig {

    @Bean
    @ConditionalOnMissingBean
    @RefreshScope
    @ConfigurationProperties(prefix = "customization.framework.base")
    FrameworkBaseConfigProperties frameworkBaseConfigProperties() {
        return new FrameworkBaseConfigProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    @RefreshScope
    @ConfigurationProperties(prefix = "customization.framework.core")
    FrameworkCoreConfigProperties frameworkCoreProperties() {
        return new FrameworkCoreConfigProperties();
    }

    @Bean
    @ConditionalOnMissingBean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @ConditionalOnMissingBean
    Ii18nService i18nService(Ii18nFrameworkModuleService i18nFrameworkModuleService) {
        return new I18nServiceImpl(i18nFrameworkModuleService);
    }

    @Bean
    @ConditionalOnMissingBean
    IAccessLogService accessLogService(IAccessLogFrameworkModuleService traceLogFrameworkModuleService,
                                       FrameworkBaseConfigProperties frameworkBaseConfigProperties,
                                       ISystemConfigPropertiesService systemConfigPropertiesService) {
        return new AccessLogServiceImpl(traceLogFrameworkModuleService, frameworkBaseConfigProperties, systemConfigPropertiesService);
    }

    @Bean
    @ConditionalOnMissingBean
    ISystemOpsSecurityService systemOpsSecurityService(FrameworkBaseConfigProperties frameworkBaseConfigProperties) {
        return new SystemOpsSecurityServiceImpl(frameworkBaseConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    IResponseI18nService responseService(ISystemConfigPropertiesService systemConfigPropertiesService,
                                         FrameworkBaseConfigProperties frameworkBaseConfigProperties) {
        return new ResponseI18nServiceImpl(systemConfigPropertiesService, frameworkBaseConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    NetworkStatusListenerLifecycleListener systemStatusListenerLifecycleListener(CompositeDiscoveryClient discoveryClient,
                                                                                 Environment environment,
                                                                                 FrameworkBaseConfigProperties frameworkBaseConfigProperties,
                                                                                 ISystemConfigPropertiesService systemConfigPropertiesService) {
        return new NetworkStatusListenerLifecycleListener(discoveryClient, environment, frameworkBaseConfigProperties, systemConfigPropertiesService);
    }

    @Bean
    @ConditionalOnMissingBean
    IAlarmService alarmService(IAlarmFrameworkModuleService alarmFrameworkModuleService,
                               FrameworkBaseConfigProperties frameworkBaseConfigProperties) {
        return new AlarmServiceImpl(alarmFrameworkModuleService, frameworkBaseConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    SystemOpsController systemOpsController(List<ISystemLifecycleListener> systemLifecycleListeners, List<IHealthCheckService> healthCheckServices) {
        return new SystemOpsController(systemLifecycleListeners, healthCheckServices);
    }

    @Bean
    @ConditionalOnMissingBean
    OperatingSystemHealthCheckServiceImpl operatingSystemHealthCheckService(FrameworkBaseConfigProperties frameworkBaseConfigProperties) {
        return new OperatingSystemHealthCheckServiceImpl(frameworkBaseConfigProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    AlarmHealthCheckServiceImpl alarmHealthCheckService(IAlarmService alarmService) {
        return new AlarmHealthCheckServiceImpl(alarmService);
    }

    @Bean
    @ConditionalOnMissingBean
    I18nHelper i18nHelper(ISystemConfigPropertiesService systemConfigPropertiesService) {
        return new I18nHelper(systemConfigPropertiesService);
    }
}
