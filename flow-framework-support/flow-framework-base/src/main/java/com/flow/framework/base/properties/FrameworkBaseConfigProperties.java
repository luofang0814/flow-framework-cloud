package com.flow.framework.base.properties;

import com.flow.framework.base.properties.component.*;
import lombok.Data;

/**
 * 框架基础模块配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class FrameworkBaseConfigProperties {

    /**
     * 是否检查基于注册中心发现的远程provider服务
     */
    private Boolean enableDiscoveryRemoteStatusCheck = false;

    /**
     * 是否开启告警服务
     */
    private Boolean enableAlarmService = false;

    /**
     * 指定三方调用网络监控配置（基于TCP的监控）
     */
    private AllocationRemoteConfigProperties allocation = new AllocationRemoteConfigProperties();

    /**
     * cpu负载、内存利用率和磁盘空间检查配置
     */
    private OperatingSystemConfigProperties operatingSystem = new OperatingSystemConfigProperties();

    /**
     * 请求相关配置
     */
    private RequestConfigProperties request = new RequestConfigProperties();

    /**
     * 响应相关配置
     */
    private ResponseConfigProperties response = new ResponseConfigProperties();

    /**
     * 系统敏感操作安全配置
     */
    private SystemOpsConfigProperties systemOps = new SystemOpsConfigProperties();
}
