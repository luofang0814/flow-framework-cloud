package com.flow.framework.base.service.alarm;

import com.flow.framework.facade.alarm.pojo.dto.AlarmClearModuleDto;
import com.flow.framework.facade.alarm.pojo.dto.AlarmReportModuleDto;

/**
 * 告警服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
public interface IAlarmService {

    /**
     * 获取告警健康状态并重置健康状态
     *
     * @return
     */
    @Deprecated
    boolean getAndResetHealth();

    /**
     * 上报告警
     *
     * @param alarmReportModuleDto reportAlarmModuleDto
     */
    void reportAlarm(AlarmReportModuleDto alarmReportModuleDto);

    /**
     * 清除告警
     *
     * @param alarmClearModuleDto clearAlarmModuleDto
     */
    void clearAlarm(AlarmClearModuleDto alarmClearModuleDto);
}
