package com.flow.framework.base.service.response.impl;

import com.flow.framework.base.helper.i18n.I18nHelper;
import com.flow.framework.base.properties.FrameworkBaseConfigProperties;
import com.flow.framework.base.properties.component.ResponseConfigProperties;
import com.flow.framework.core.constant.FrameworkCoreConstant;
import com.flow.framework.core.pojo.bo.InternationalBo;
import com.flow.framework.core.service.properties.ISystemConfigPropertiesService;
import com.flow.framework.core.service.response.IResponseI18nService;
import com.flow.framework.core.system.listener.lifecycle.ISystemLifecycleListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 响应处理服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
@Slf4j
@RequiredArgsConstructor
public class ResponseI18nServiceImpl implements IResponseI18nService, ISystemLifecycleListener {

    private final ISystemConfigPropertiesService systemConfigPropertiesService;

    private final FrameworkBaseConfigProperties frameworkBaseConfigProperties;

    private String systemCode;

    /**
     * @inheritDoc
     */
    @Override
    public InternationalBo i18n(String code, List<String> params) {
        String internationalKey = I18nHelper.getInternationalKey(code, params);
        return new InternationalBo(systemCode, systemCode + code, internationalKey);
    }

    /**
     * @inheritDoc
     */
    @Override
    public String getCurrentSystemCode() {
        return systemCode;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onStartUp() {
        ResponseConfigProperties responseConfig = frameworkBaseConfigProperties.getResponse();
        if (null == responseConfig) {
            this.systemCode = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_CLIENT_PORT_KEY);
            return;
        }
        String systemCode = responseConfig.getSystemCode();
        if (null != systemCode) {
            this.systemCode = systemCode;
        } else {
            this.systemCode = systemConfigPropertiesService.getConfigValue(FrameworkCoreConstant.SERVICE_CLIENT_PORT_KEY);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onRefresh() {
        onStartUp();
    }
}
