package com.flow.framework.base.service.i18n.impl;

import com.flow.framework.base.service.i18n.Ii18nService;
import com.flow.framework.facade.i18n.pojo.dto.RecordUnInternationalModuleDto;
import com.flow.framework.facade.i18n.module.service.Ii18nFrameworkModuleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Locale;

/**
 * i18n 服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/11
 */
@Slf4j
@RequiredArgsConstructor
public class I18nServiceImpl implements Ii18nService {

    private final Ii18nFrameworkModuleService i18nFrameworkModuleService;

    /**
     * @inheritDoc
     */
    @Override
    public String getInternationalValue(String serviceName, String key, Locale locale) {
        try {
            return i18nFrameworkModuleService.getInternationalValue(serviceName, key, locale);
        } catch (Exception e) {
            log.error("get i18n error. service name : {}, key: {}, locale: {}", serviceName, key, locale, e);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void recordUnInternationalKey(String serviceName, String key, Locale locale) {
        try {
            i18nFrameworkModuleService.recordUnInternationalKey(new RecordUnInternationalModuleDto(serviceName, key, locale));
        } catch (Exception e) {
            log.error("record un-international key error. service name: {}, key: {}, locale: {}", serviceName, key, locale, e);
        }
    }
}
