package com.flow.framework.base.properties.component;

import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * 请求相关配置
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/12/10
 */
@Data
public class RequestConfigProperties {

    /**
     * 加工请求body的最大值，默认512kb，如：验签，解密等
     */
    private int processMaxBodySize = 512 * 1024;

    /**
     * 是否持久化记录请求日志
     */
    private boolean enableRecordPersistence = false;

    /**
     * 持久化请求日志的最大长度
     */
    private int recordPersistenceMaxLength = 10240;

    /**
     * 是否将请求日志记录到本地系统日志中
     */
    private boolean enableRecordLocal = false;

    /**
     * 请求日志记录到本地系统日志中的最大长度
     */
    private int recordLocalMaxLength = 10240;

    /**
     * 需要忽略请求日志记录的uri，注意：只要包含uri，则不记录请求日志
     */
    private List<String> ignoreRecordTraceUris = Collections.singletonList("/rpc/");

    /**
     * 需要忽略请求方法
     */
    private List<String> ignoreRecordRequestMethods = Collections.emptyList();

    /**
     * 标记客户端IP的header名称
     */
    private String remoteHostHeaderKey = "X-Forwarded-For";
}
