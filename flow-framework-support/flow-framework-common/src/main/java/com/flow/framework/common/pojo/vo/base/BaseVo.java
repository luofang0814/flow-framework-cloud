package com.flow.framework.common.pojo.vo.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础view Object对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Data
@ToString
@EqualsAndHashCode
public abstract class BaseVo implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 创建时间，由数据库自动设置值，不可在业务代码中设置相关的值
     */
    private LocalDateTime createTime;

    /**
     * 更新时间，由数据库自动设置值，不可在业务代码中设置相关的值
     */
    private LocalDateTime updateTime;

    /**
     * 是否已删除
     */
    private Boolean isDeleted = false;
}
