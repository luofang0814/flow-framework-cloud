package com.flow.framework.common.datetime;

import java.time.format.DateTimeFormatter;

/**
 * 通用时间格式/格式转换器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
public class CommonDateTimeFormatter {

    /**
     * 年月格式：yyyy-MM
     */
    public static final String COMMON_MONTH_PATTERN = "yyyy-MM";

    /**
     * 年月格式 DateTimeFormatter：yyyy-MM
     */
    public static final DateTimeFormatter COMMON_MONTH_FORMATTER = DateTimeFormatter.ofPattern(COMMON_MONTH_PATTERN);


    /**
     * 简单年月格式：yyyyMM
     */
    public static final String SIMPLE_MONTH_PATTERN = "yyyyMM";
    /**
     * 简单年月格式 DateTimeFormatter：yyyyMM
     */
    public static final DateTimeFormatter SIMPLE_MONTH_FORMATTER = DateTimeFormatter.ofPattern(SIMPLE_MONTH_PATTERN);


    /**
     * 中文年月格式：yyyy年MM月
     */
    public static final String CHINESE_MONTH_PATTERN = "yyyy年MM月";
    /**
     * 中文年月格式 DateTimeFormatter：yyyy年MM月
     */
    public static final DateTimeFormatter CHINESE_MONTH_FORMATTER = DateTimeFormatter.ofPattern(CHINESE_MONTH_PATTERN);


    /**
     * 标准日期格式：yyyy-MM-dd
     */
    public static final String COMMON_DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 标准日期格式 DateTimeFormatter：yyyy-MM-dd
     */
    public static final DateTimeFormatter COMMON_DATE_FORMATTER = DateTimeFormatter.ofPattern(COMMON_DATE_PATTERN);


    /**
     * 简单日期格式：yyyyMMdd
     */
    public static final String SIMPLE_DATE_PATTERN = "yyyyMMdd";
    /**
     * 简单日期格式 DateTimeFormatter：yyyyMMdd
     */
    public static final DateTimeFormatter SIMPLE_DATE_FORMATTER = DateTimeFormatter.ofPattern(SIMPLE_DATE_PATTERN);


    /**
     * 中文日期格式：yyyy年MM月dd日
     */
    public static final String CHINESE_DATE_PATTERN = "yyyy年MM月dd日";
    /**
     * 中文日期格式 DateTimeFormatter：yyyy年MM月dd日
     */
    public static final DateTimeFormatter CHINESE_DATE_FORMATTER = DateTimeFormatter.ofPattern(CHINESE_DATE_PATTERN);


    /**
     * 标准时间格式：HH:mm:ss
     */
    public static final String COMMON_TIME_PATTERN = "HH:mm:ss";
    /**
     * 标准时间格式 DateTimeFormatter：HH:mm:ss
     */
    public static final DateTimeFormatter COMMON_TIME_FORMATTER = DateTimeFormatter.ofPattern(COMMON_TIME_PATTERN);


    /**
     * 标准日期时间格式，精确到秒：yyyy-MM-dd HH:mm:ss
     */
    public static final String COMMON_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 标准日期时间格式，精确到秒 DateTimeFormatter：yyyy-MM-dd HH:mm:ss
     */
    public static final DateTimeFormatter COMMON_DATETIME_FORMATTER = DateTimeFormatter.ofPattern(COMMON_DATETIME_PATTERN);


    /**
     * 简单日期时间格式，精确到秒：yyyyMMdd HH:mm:ss
     */
    public static final String SIMPLE_DATETIME_PATTERN = "yyyyMMdd HH:mm:ss";
    /**
     * 简单日期时间格式，精确到秒 DateTimeFormatter：yyyyMMdd HH:mm:ss
     */
    public static final DateTimeFormatter SIMPLE_DATETIME_FORMATTER = DateTimeFormatter.ofPattern(SIMPLE_DATETIME_PATTERN);


    /**
     * 中文日期格式：yyyy年MM月dd日 HH时mm分ss秒
     */
    public static final String CHINESE_DATE_TIME_PATTERN = "yyyy年MM月dd日HH时mm分ss秒";
    /**
     * 中文日期格式 DateTimeFormatter：yyyy年MM月dd日HH时mm分ss秒
     */
    public static final DateTimeFormatter CHINESE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(CHINESE_DATE_TIME_PATTERN);

    /**
     * 无分隔符格式
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static final String NO_SEPARATOR_DATE_TIME_PATTERN = "yyyyMMddHHmmss";

    /**
     * 无分隔符格式
     */
    public static final DateTimeFormatter NO_SEPARATOR_DATETIME_FORMATTER = DateTimeFormatter.ofPattern(NO_SEPARATOR_DATE_TIME_PATTERN);
}
