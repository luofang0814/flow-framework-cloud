package com.flow.framework.common.util.digest.version;

import com.flow.framework.common.constant.FrameworkCommonConstant;
import com.flow.framework.common.enumeration.CodeEnum;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * hmac版本
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Getter
@Slf4j
@AllArgsConstructor
public enum HmacVersion implements CodeEnum {

    /**
     * HMV1，枚举code必须是HMV开头
     */
    HMV1("HMV1", "HMV1", "HMV1", "HmacSHA512"),
    ;

    // 枚举code必须是HMV开头
    static {
        HmacVersion[] values = HmacVersion.values();
        for (HmacVersion hmacVersion : values) {
            if (!hmacVersion.getCode().startsWith(FrameworkCommonConstant.HMV_CIPHER)) {
                log.error("cipher name error.");
                throw new CheckedException(SystemErrorCode.SYSTEM_NOT_SUPPORT_ERROR);
            }
        }
    }

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    private String remark;

    /**
     * 算法
     */
    private String signatureAlgorithm;
}