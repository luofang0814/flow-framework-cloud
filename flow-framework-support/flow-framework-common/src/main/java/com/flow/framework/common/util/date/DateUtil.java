package com.flow.framework.common.util.date;

import com.flow.framework.common.datetime.CommonDateTimeFormatter;
import com.flow.framework.common.error.SystemErrorCode;
import com.flow.framework.common.exception.CheckedException;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * 时间工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Slf4j
public final class DateUtil {

    /**
     * 将毫秒数转化为时间
     *
     * @param millisecond millisecond
     * @return
     */
    public static LocalDateTime convertToLocalDateTime(long millisecond) {
        Instant instant = Instant.ofEpochMilli(millisecond);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    /**
     * 将毫秒数转化为指定时区时间
     *
     * @param millisecond millisecond
     * @param zoneId      zoneId
     * @return
     */
    public static LocalDateTime convertToLocalDateTime(long millisecond, ZoneId zoneId) {
        if (null == zoneId) {
            log.error("zone id is null");
            throw new CheckedException(SystemErrorCode.DATE_FORMAT_ERROR, "zone id is null", null);
        }
        Instant instant = Instant.ofEpochMilli(millisecond);
        return LocalDateTime.ofInstant(instant, zoneId);
    }

    /**
     * 将字符串转化为时间
     *
     * @param text text
     * @return
     */
    public static LocalDateTime parseLocalDateTime(CharSequence text) {
        try {
            return LocalDateTime.parse(text, CommonDateTimeFormatter.COMMON_DATETIME_FORMATTER);
        } catch (Exception e) {
            log.error("parse date time error.", e);
            throw new CheckedException(SystemErrorCode.DATE_FORMAT_ERROR, "parse date time error", e);
        }
    }

    /**
     * 将字符串转化为时间
     *
     * @param text text
     * @return
     */
    public static LocalDate parseLocalDate(CharSequence text) {
        if (null == text) {
            return null;
        }
        try {
            return LocalDate.parse(text, CommonDateTimeFormatter.COMMON_DATE_FORMATTER);
        } catch (Exception e) {
            log.error("parse date error.", e);
            throw new CheckedException(SystemErrorCode.DATE_FORMAT_ERROR, "parse date error", e);
        }
    }

    /**
     * 将时间转化为默认格式字符串
     *
     * @param localDateTime localDateTime
     * @return
     */
    public static String formatLocalDateTime(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        try {
            return localDateTime.format(CommonDateTimeFormatter.COMMON_DATETIME_FORMATTER);
        } catch (Exception e) {
            log.error("format date time error.", e);
            throw new CheckedException(SystemErrorCode.DATE_FORMAT_ERROR, "format date time error", e);
        }
    }

    /**
     * 将日期转换为指定格式的字符串
     *
     * @param localDate localDate
     * @return
     */
    public static String formatLocalDate(LocalDate localDate) {
        if (null == localDate) {
            return null;
        }
        try {
            return localDate.format(CommonDateTimeFormatter.COMMON_DATE_FORMATTER);
        } catch (Exception e) {
            log.error("format date time error.", e);
            throw new CheckedException(SystemErrorCode.DATE_FORMAT_ERROR, "format date error", e);
        }
    }
}
