package com.flow.framework.common.type;

/**
 * 泛型支持
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/2
 */
public abstract class TypeReference<T> extends com.fasterxml.jackson.core.type.TypeReference<T> {
}
