package com.flow.framework.common.tuple;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 简单键值对，如果不是通用场景下，不建议使用，建议自定义对象保存
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValueTuple<K, V> {

    /**
     * 键
     */
    private K key;

    /**
     * 值
     */
    private V value;
}
