
package com.flow.framework.common.error;

/**
 * 系统错误码定义
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
public interface SystemErrorCode {

    /**
     * 系统启动异常
     */
    long SYSTEM_START_ERROR = 1;

    /**
     * 参数错误
     */
    long PARAMS_ERROR = 2;

    /**
     * 未预期异常
     */
    long UNEXPECTED_ERROR = 3;

    /**
     * 加解密异常
     */
    long CIPHER_ERROR = 4;

    /**
     * 请求签名异常
     */
    long REQUEST_SIGN_ERROR = 5;

    /**
     * 序列化异常
     */
    long SERIALIZE_ERROR = 6;

    /**
     * 反序列化异常
     */
    long DESERIALIZATION_ERROR = 7;

    /**
     * 消息队列异常
     */
    long MQ_ERROR = 8;

    /**
     * 锁异常
     */
    long LOCK_ERROR = 9;

    /**
     * 对象不存在
     */
    long OBJECT_NOT_FOUND_ERROR = 10;

    /**
     * 对象已存在
     */
    long OBJECT_EXIST_ERROR = 11;

    /**
     * 未登录/登陆已过期
     */
    long UNAUTHORIZED_ERROR = 12;

    /**
     * 权限不足
     */
    long FORBIDDEN_ERROR = 13;

    /**
     * 时间转换错误
     */
    long DATE_FORMAT_ERROR = 14;

    /**
     * 远程调用错误
     */
    long REMOTE_PROCEDURE_CALL_ERROR = 15;

    /**
     * 数据类型错误
     */
    long DATA_TYPE_ERROR = 16;

    /**
     * 服务器错误
     */
    long API_SERVER_ERROR = 17;

    /**
     * 请求方式不支持
     */
    long API_REQUEST_METHOD_ERROR = 18;

    /**
     * 请求body错误
     */
    long API_REQUEST_BODY_ERROR = 19;

    /**
     * 参数错误
     */
    long API_UNKNOWN_PARAMS_ERROR = 20;

    /**
     * 系统不支持
     */
    long SYSTEM_NOT_SUPPORT_ERROR = 21;

    /**
     * 租户id错误
     */
    long TENANT_ID_ERROR = 22;

    /**
     * 微服务即将关闭错误
     */
    long SERVICE_WILL_SHUTDOWN_ERROR = 23;

    /**
     * 任务调度错误
     */
    long SCHEDULE_JOB_ERROR = 24;

    /**
     * 本地缓存错误
     */
    long LOCAL_CACHE_ERROR = 25;

    /**
     * 提交任务失败
     */
    long SUBMIT_TASK_ERROR = 26;

    /**
     * 获取数据超时
     */
    long GET_DATA_TIMEOUT_ERROR = 27;
}
