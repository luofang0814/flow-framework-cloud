package com.flow.framework.common.immutable.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 常量约束接口中的常量备注
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
@Documented
@Target({FIELD})
@Retention(RUNTIME)
public @interface ImmutableRemark {

    /**
     * 备注的内容
     *
     * @return 备注的内容
     */
    String value();
}