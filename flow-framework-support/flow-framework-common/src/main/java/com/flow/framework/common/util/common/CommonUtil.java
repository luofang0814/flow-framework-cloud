package com.flow.framework.common.util.common;

import javax.annotation.Nullable;

/**
 * 通用工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
public class CommonUtil {

    /**
     * 获取对象的hash code，对象可以为null
     *
     * @param o o
     * @return
     */
    public static String getHashCode(@Nullable Object o) {
        if (null == o) {
            return "null";
        }
        return String.valueOf(o.hashCode());
    }
}