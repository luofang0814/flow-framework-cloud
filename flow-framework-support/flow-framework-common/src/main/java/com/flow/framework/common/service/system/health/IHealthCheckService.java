package com.flow.framework.common.service.system.health;

import com.flow.framework.common.pojo.vo.system.ServiceHealthVo;

import java.util.List;

/**
 * 系统健康检查服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
public interface IHealthCheckService {

    /**
     * 健康检查
     *
     * @return
     */
    List<ServiceHealthVo> check();
}
