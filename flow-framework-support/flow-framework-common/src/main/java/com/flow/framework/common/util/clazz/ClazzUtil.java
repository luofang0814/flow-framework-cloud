package com.flow.framework.common.util.clazz;

import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.common.constant.FrameworkCommonConstant;

/**
 * class工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
public class ClazzUtil {

    /**
     * 将class的全局限定名转换为短class名称
     *
     * @param clazzName 类名字符串
     * @return
     */
    public static String getShortByClazzName(String clazzName) {
        if (VerifyUtil.isEmpty(clazzName)) {
            return FrameworkCommonConstant.EMPTY_STRING;
        }
        String[] splits = clazzName.split("\\.");
        StringBuilder builder = new StringBuilder();
        for (String temp : splits) {
            char firstChar = temp.charAt(0);
            if (Character.isUpperCase(firstChar)) {
                builder.append(temp);
            } else {
                builder.append(firstChar);
            }
            builder.append(".");
        }
        return builder.substring(0, builder.length() - 1);
    }

    /**
     * 将class的全局限定名转换为短class名称
     *
     * @param clazz 类
     * @return
     */
    public static <T> String getShortByClazz(Class<T> clazz) {
        if (VerifyUtil.isEmpty(clazz)) {
            return FrameworkCommonConstant.EMPTY_STRING;
        }
        return getShortByClazzName(clazz.getName());
    }
}