package com.flow.framework.common.pojo.vo.base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 基础view Object对象
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
public abstract class BaseBizVo extends BaseVo {

    /**
     * 租户id，由脚手架自动设置值，不可在业务代码中设置相关的值
     */
    private String tenantId;
}
