package com.flow.framework.common.stream.handler;

/**
 * 批量处理器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/2
 */
public interface BatchProcessHandler {

    /**
     * 最大32M
     */
    int MAX_BUFFER_SIZE = 33554432;

    /**
     * 默认8k
     */
    int DEFAULT_BUFFER_SIZE = 8192;

    /**
     * 批量大小，即每一批解码的数组buffer大小
     *
     * @return 批量大小
     */
    default int getBatchSize() {
        return DEFAULT_BUFFER_SIZE;
    }
}