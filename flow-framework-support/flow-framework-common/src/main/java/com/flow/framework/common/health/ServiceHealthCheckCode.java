package com.flow.framework.common.health;

/**
 * 健康检查项编码
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
public interface ServiceHealthCheckCode {
    /**
     * CPU负载编码
     */
    int SERVICE_CPU_LOAD_CODE = 100000000;

    /**
     * 线程池编码
     */
    int SERVICE_THREAD_POOL_CODE = 100000001;

    /**
     * 内存编码
     */
    int SERVICE_MEMORY_CODE = 100000002;

    /**
     * 存储编码
     */
    int SERVICE_STORAGE_CODE = 100000003;

    /**
     * 配置中心编码
     */
    int SERVICE_CONFIG_CENTER_CODE = 100000004;

    /**
     * 注册中心编码
     */
    int SERVICE_REGISTRY_CENTER_CODE = 100000005;

    /**
     * 注册客户端状态编码（即是否注册到注册中心）
     */
    int SERVICE_REGISTRY_CLIENT_CODE = 100000006;

    /**
     * 分布式缓存编码
     */
    int SERVICE_DISTRIBUTION_CACHE_CODE = 100000007;

    /**
     * 分布式锁编码
     */
    int SERVICE_DISTRIBUTION_LOCK_CODE = 100000008;

    /**
     * 数据库编码
     */
    int SERVICE_DATABASE_CODE = 100000009;

    /**
     * 消息队列编码
     */
    int SERVICE_MQ_CODE = 100000020;

    /**
     * 调度中心编码
     */
    int SERVICE_SCHEDULER_CODE = 100000021;

    /**
     * 内部服务网络状态编码
     */
    int SERVICE_INNER_NET_CODE = 100000022;

    /**
     * 三方服务网络状态编码
     */
    int SERVICE_OUTER_NET_CODE = 100000023;

    /**
     * 健康检查异常编码
     */
    int SERVICE_HEALTH_CHECK_CODE = 100000024;

    /**
     * 异步调度检查异常编码
     */
    int ASYNC_SCHEDULE_HEALTH_CHECK_CODE = 100000025;

    /**
     * 业务检查异常编码
     */
    int BIZ_HEALTH_CHECK_CODE = 100000026;
}