package com.flow.framework.common.stream.handler;

/**
 * 批量处理器
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/2
 */
public interface BatchVoidProcessHandler extends BatchProcessHandler {

    /**
     * 批量处理
     *
     * @param buffer 每一批处理的数组buffer，最后一次处理的buffer大小可能小于批量处理规定的buffer大小
     * @return
     */
    void process(byte[] buffer);
}