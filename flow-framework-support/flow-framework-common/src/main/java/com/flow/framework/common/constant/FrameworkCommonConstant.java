package com.flow.framework.common.constant;

/**
 * 框架通用模块常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
public final class FrameworkCommonConstant {
    /**
     * 当前支持的BV算法
     */
    public static final String BV_CIPHER = "BV";

    /**
     * 当前支持的RV算法
     */
    public static final String RV_CIPHER = "RV";

    /**
     * 当前支持的SV算法
     */
    public static final String SV_CIPHER = "SV";

    /**
     * 当前支持的HMV算法
     */
    public static final String HMV_CIPHER = "HMV";

    /**
     * 空字符串
     */
    public static final String EMPTY_STRING = "";

    /**
     * IO读取后返回长度的结束值
     */
    public static final int EOF = -1;

    /**
     * 默认IO字节数组大小
     */
    public static final int DEFAULT_IO_BUFFER_SIZE = 8192;

    /**
     * 数组最大索引数字比数组大小小1
     */
    public static final int ARRAY_INDEX_LESS_SIZE_VALUE = 1;

    /**
     * 只有一个元素的数组或容器的大小
     */
    public static final int ONE_ELEMENT_COLLECTION_SIZE = 1;

    /**
     * 日志调用链ID的KEY
     */
    public static final String GLOBAL_LOG_TRACE_KEY = "customization_trace_id";

    /**
     * 环境版本编码
     */
    public static final String SYSTEM_VERSION_KEY = "customization_system_version";

    /**
     * 左大括号
     */
    public static final String LEFT_BRACE = "{";

    /**
     * 右大括号
     */
    public static final String RIGHT_BRACE = "}";

    /**
     * 左中括号
     */
    public static final String LEFT_BRACKET = "[";

    /**
     * 右中括号
     */
    public static final String RIGHT_BRACKET = "]";
}
