package com.flow.framework.common.exception;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import java.util.List;

/**
 * 检查异常
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
@Getter
@ToString(callSuper = true)
@Slf4j
public class CheckedException extends RuntimeException {

    /**
     * 错误编码
     */
    private long code;

    /**
     * 自定义消息
     */
    private String msg;

    /**
     * 错误编码国际化对应的参数
     */
    private List<String> params;

    /**
     * 错误来源系统编码
     */
    private String srcSystem;

    /**
     * 错误来源系统的错误编码
     */
    private String srcCode;

    /**
     * 错误来源系统自定义消息
     */
    private String srcMsg;

    /**
     * 错误码来源系统自定义消息
     */
    private List<String> srcParams;

    /**
     * @param errorCode errorCode
     */
    public CheckedException(long errorCode) {
        this(errorCode, null, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     */
    public CheckedException(long errorCode, String message) {
        this(errorCode, message, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param e         e
     */
    public CheckedException(long errorCode, String message, Throwable e) {
        this(errorCode, message, null, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param e         e
     */
    public CheckedException(long errorCode, String message, @Nullable List<String> params, Throwable e) {
        this(errorCode, message, params, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     */
    public CheckedException(long errorCode, String srcSystem, String srcCode, String srcMsg, List<String> srcParams) {
        this(errorCode, null, null, srcSystem, srcCode, srcMsg, srcParams);
    }

    /**
     * @param errorCode errorCode
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     * @param e         e
     */
    public CheckedException(long errorCode, String srcSystem, String srcCode, String srcMsg, List<String> srcParams, Throwable e) {
        this(errorCode, null, null, srcSystem, srcCode, srcMsg, srcParams, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     */
    public CheckedException(long errorCode, String message, @Nullable List<String> params, String srcSystem, String srcCode,
                            String srcMsg, List<String> srcParams) {
        super(String.valueOf(errorCode));
        this.code = errorCode;
        this.msg = message;
        this.params = params;
        this.srcSystem = srcSystem;
        this.srcCode = srcCode;
        this.srcMsg = srcMsg;
        this.srcParams = srcParams;
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     * @param e         e
     */
    public CheckedException(long errorCode, String message, @Nullable List<String> params, String srcSystem, String srcCode,
                            String srcMsg, List<String> srcParams, Throwable e) {

        super(String.valueOf(errorCode), e);
        this.code = errorCode;
        this.msg = message;
        this.params = params;
        if (null == srcCode && e instanceof CheckedException) {
            CheckedException checkedException = (CheckedException) e;
            this.srcSystem = checkedException.getSrcSystem();
            this.srcCode = checkedException.getSrcCode();
            this.srcMsg = checkedException.getSrcMsg();
            this.srcParams = checkedException.getSrcParams();
            return;
        }
        this.srcSystem = srcSystem;
        this.srcCode = srcCode;
        this.srcMsg = srcMsg;
        this.srcParams = srcParams;
    }
}
