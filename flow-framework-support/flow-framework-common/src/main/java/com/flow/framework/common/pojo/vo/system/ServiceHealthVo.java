package com.flow.framework.common.pojo.vo.system;

import com.flow.framework.common.pojo.vo.base.BaseVo;
import lombok.*;

import java.util.List;

/**
 * 健康检查返回对象，对象构造参考com.flow.framework.core.helper.system.SystemHealthHelper
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ServiceHealthVo extends BaseVo {

    protected String serviceName;

    protected String servicePort;

    protected String workerName;

    protected String workerIp;

    protected String serviceHealthCode;

    protected boolean healthy;

    protected String msg;

    protected String extMsg;

    protected List<String> params;
}
