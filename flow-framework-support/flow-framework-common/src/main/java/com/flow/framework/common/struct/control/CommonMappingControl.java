package com.flow.framework.common.struct.control;

import org.mapstruct.control.MappingControl;

/**
 * 使用map struct做属性copy时，建议只使用直接属性转换，避免出现动态编译报错
 * 详情请查看 MappingControl.Use.DIRECT，使用规范请参考项目中的实际使用
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@MappingControl(MappingControl.Use.DIRECT)
@MappingControl(MappingControl.Use.MAPPING_METHOD)
public @interface CommonMappingControl {
}
