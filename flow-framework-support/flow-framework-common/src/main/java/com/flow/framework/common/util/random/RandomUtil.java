package com.flow.framework.common.util.random;

import com.flow.framework.common.constant.FrameworkCommonConstant;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机相关工具类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
public final class RandomUtil {

    private RandomUtil() {
    }

    /**
     * 随机获取长度为20的id，区分大小写
     *
     * @return
     */
    public static String random20LenId() {
        return random(20);
    }

    /**
     * 随机获取长度为14的id，区分大小写
     *
     * @return
     */
    public static String random14LenId() {
        return random(14);
    }

    /**
     * 随机获取指定长度的id，区分大小写
     *
     * @return
     */
    private static String random(int len) {
        StringBuilder id = new StringBuilder();
        //产生16位的强随机数
        Random rd = new SecureRandom();
        for (int i = 0; i < len; i++) {
            //产生0-2的3位随机数
            int type = rd.nextInt(3);
            switch (type) {
                case 0:
                    //0-9的随机数
                    id.append(rd.nextInt(10));
                    break;
                case 1:
                    //ASCII在65-90之间为大写,获取大写随机
                    id.append((char) (rd.nextInt(25) + 65));
                    break;
                default:
                    //ASCII在97-122之间为小写，获取小写随机
                    id.append((char) (rd.nextInt(25) + 97));
            }
        }
        return id.toString();
    }

    /**
     * 获取没有短横线的uuid
     *
     * @return String
     */
    public static String randomNoShortLineUuid() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return new UUID(random.nextLong(), random.nextLong()).toString().replace("-", FrameworkCommonConstant.EMPTY_STRING);
    }
}
