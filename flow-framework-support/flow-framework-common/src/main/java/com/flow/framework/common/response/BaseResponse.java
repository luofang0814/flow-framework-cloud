package com.flow.framework.common.response;

import lombok.Getter;

import java.util.List;

/**
 * 基础响应
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/3
 */
@Getter
public abstract class BaseResponse<T> {
    /**
     * 系统编码
     */
    protected String system;

    /**
     * 状态码/错误码
     */
    protected String code;

    /**
     * 自定义消息
     */
    protected String msg;

    /**
     * 状态码/错误码对应的国际化参数
     */
    protected List<String> params;

    /**
     * 错误来源系统编码
     */
    protected String srcSystem;

    /**
     * 错误来源系统的错误码
     */
    protected String srcCode;

    /**
     * 错误来源系统的自定义消息
     */
    protected String srcMsg;

    /**
     * 错误来源系统的错误码对应的国际化参数
     */
    protected List<String> srcParams;

    /**
     * 实际数据
     */
    protected T data;

    public String getSystem() {
        return system;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public final List<String> getParams() {
        return params;
    }

    public String getSrcSystem() {
        return srcSystem;
    }

    public String getSrcCode() {
        return srcCode;
    }

    public String getSrcMsg() {
        return srcMsg;
    }

    public List<String> getSrcParams() {
        return srcParams;
    }

    public T getData() {
        return data;
    }
}
