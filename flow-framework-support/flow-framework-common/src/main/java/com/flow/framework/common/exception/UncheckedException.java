package com.flow.framework.common.exception;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 未检查异常
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
@Getter
@ToString(callSuper = true)
@Slf4j
public class UncheckedException extends Exception {

    /**
     * 错误编码
     */
    private long code;

    /**
     * 自定义消息
     */
    private String msg;

    /**
     * 错误编码国际化对应的参数
     */
    private List<String> params;

    /**
     * 错误来源系统编码
     */
    private String srcSystem;

    /**
     * 错误来源系统的错误编码
     */
    private String srcCode;

    /**
     * 错误来源系统自定义消息
     */
    private String srcMsg;

    /**
     * 错误码来源系统自定义消息
     */
    private List<String> srcParams;

    /**
     * @param errorCode errorCode
     */
    public UncheckedException(long errorCode) {
        this(errorCode, null, null, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param e         e
     */
    public UncheckedException(long errorCode, Exception e) {
        this(errorCode, null, null, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     */
    public UncheckedException(long errorCode, String message) {
        this(errorCode, message, null, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param e         e
     */
    public UncheckedException(long errorCode, String message, Exception e) {
        this(errorCode, message, null, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param params    params
     */
    public UncheckedException(long errorCode, List<String> params) {
        this(errorCode, null, params, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param params    params
     * @param e         e
     */
    public UncheckedException(long errorCode, List<String> params, Throwable e) {
        this(errorCode, null, params, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     */
    public UncheckedException(long errorCode, String message, List<String> params) {
        this(errorCode, message, params, null, null, null, null, null);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param e         e
     */
    public UncheckedException(long errorCode, String message, List<String> params, Throwable e) {
        this(errorCode, message, params, null, null, null, null, e);
    }

    /**
     * @param errorCode errorCode
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     */
    public UncheckedException(long errorCode, String srcSystem, String srcCode, String srcMsg, List<String> srcParams) {
        this(errorCode, null, null, srcSystem, srcCode, srcMsg, srcParams, null);
    }

    /**
     * @param errorCode errorCode
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     * @param e         e
     */
    public UncheckedException(long errorCode, String srcSystem, String srcCode, String srcMsg, List<String> srcParams, Throwable e) {
        this(errorCode, null, null, srcSystem, srcCode, srcMsg, srcParams, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     */
    public UncheckedException(long errorCode, String message, String srcSystem, String srcCode, String srcMsg, List<String> srcParams) {
        this(errorCode, message, null, srcSystem, srcCode, srcMsg, srcParams, null);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     * @param e         e
     */
    public UncheckedException(long errorCode, String message, String srcSystem, String srcCode, String srcMsg, List<String> srcParams,
                              Exception e) {
        this(errorCode, message, null, srcSystem, srcCode, srcMsg, srcParams, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param srcSystem srcSystem
     * @param srcCode   srcCode
     * @param srcMsg    srcMsg
     * @param srcParams srcParams
     * @param e         e
     */
    public UncheckedException(long errorCode, String message, List<String> params, String srcSystem, String srcCode, String srcMsg,
                              List<String> srcParams, Throwable e) {
        super(String.valueOf(errorCode), e);
        this.code = errorCode;
        this.msg = message;
        this.params = params;
        this.srcSystem = srcSystem;
        this.srcCode = srcCode;
        this.srcMsg = srcMsg;
        this.srcParams = srcParams;
    }
}
