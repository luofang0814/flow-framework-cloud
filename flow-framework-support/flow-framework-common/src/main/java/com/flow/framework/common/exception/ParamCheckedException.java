package com.flow.framework.common.exception;

import java.util.List;

/**
 * 参数检查异常
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/1
 */
public class ParamCheckedException extends CheckedException {

    /**
     * @param errorCode errorCode
     */
    public ParamCheckedException(long errorCode) {
        super(errorCode);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     */
    public ParamCheckedException(long errorCode, String message) {
        super(errorCode, message);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param e         e
     */
    public ParamCheckedException(long errorCode, String message, Throwable e) {
        super(errorCode, message, e);
    }

    /**
     * @param errorCode errorCode
     * @param message   message
     * @param params    params
     * @param e         e
     */
    public ParamCheckedException(long errorCode, String message, List<String> params, Throwable e) {
        super(errorCode, message, params, e);
    }
}