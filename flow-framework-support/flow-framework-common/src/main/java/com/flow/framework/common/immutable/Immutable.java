package com.flow.framework.common.immutable;

/**
 * 常量约束标记接口，所有常量约束接口都必须集成该接口
 * <p>
 * 注意：常量约束接口只有在入参时才校验入参的值是否包含在常量中，使用方法可参考com.flow.framework.module.call中的README！
 * 但是在响应模型（Vo）中还是需要添加ImmutableCustomization注解，提示接收方该字段是使用不可变接口定义的
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/1/22
 */
public interface Immutable {
}