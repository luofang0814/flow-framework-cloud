CREATE TABLE `toleration_computer` (
  `id` varchar(20) NOT NULL COMMENT 'id',
  `computer_type` char(2) NOT NULL DEFAULT '01' COMMENT '电脑类型:01-台式；02-便携式',
  `computer_code` varchar(32) NOT NULL COMMENT '电脑编码',
  `computer_model` varchar(32) NOT NULL COMMENT '电脑型号',
  `hardware_config` varchar(32) NOT NULL COMMENT '电脑硬件配置',
  `os_type` char(2) NOT NULL COMMENT '操作系统类型：01-windows；01-centos',
  `os_version` varchar(16) NOT NULL COMMENT '操作系统版本',
  `computer_status` char(2) NOT NULL COMMENT '电脑状态：01-正常；02-未知故障；03-硬件故障；04-软件故障；05-回收出售',
  `computer_occupancy_status` char(2) NOT NULL COMMENT '电脑占用状态：01-申请中；02-使用中；03-空闲',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  `system_version` bigint(20) NOT NULL DEFAULT '0' COMMENT '处理数据的系统版本号',
  `tenant_id` varchar(20) NOT NULL DEFAULT '' COMMENT '租户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='电脑信息';