package com.flow.service.facade.toleration;

/**
 * toleration模块错误码
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/1
 */
public interface TolerationErrorCode {

    /**
     * 参数不能为空
     */
    long INPUT_OBJECT_EMPTY_ERROR = 60001;

    /**
     * 参数属性为空错误
     */
    long INPUT_PROPERTY_EMPTY_ERROR = 60002;

    /**
     * 电脑类型错误
     */
    long COMPUTER_TYPE_ERROR = 60003;

    /**
     * 操作系统类型错误
     */
    long OS_TYPE_ERROR = 60004;

    /**
     * 电脑状态错误
     */
    long COMPUTER_STATUS_ERROR = 60005;


    /**
     * 电脑编号已存在错误
     */
    long COMPUTER_CODE_EXIST_ERROR = 60006;

    /**
     * 电脑占用状态错误
     */
    long COMPUTER_OCCUPANCY_STATUS_ERROR = 60007;

    /**
     * 电脑信息不存在
     */
    long COMPUTER_NOT_EXIST_ERROR = 60008;

    /**
     * 电脑服务不存在
     */
    long COMPUTER_SERVICE_NOT_EXIST_ERROR = 60009;

    /**
     * 更新电脑占用状态错误
     */
    long COMPUTER_OCCUPANCY_STATUS_UPDATE_ERROR = 60010;
}
