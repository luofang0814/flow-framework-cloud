package com.flow.service.facade.toleration;

/**
 * toleration模块常量类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/1
 */
public interface TolerationModuleConstant {

    /**
     * URL调用前缀表达式
     */
    String URL_PREFIX_EXPRESSION = "${customization.service.facade.toleration.url:}";

    /**
     * application name表达式
     */
    String APPLICATION_NAME_EXPRESSION = "${customization.service.facade.computer.application-name:flow-service-test-biz}";

    /**
     * RPC调用前缀表达式
     */
    String RPC_PREFIX_EXPRESSION = "${customization.service.facade.toleration.rpc-prefix:/testBiz}";
}