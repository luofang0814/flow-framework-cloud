package com.flow.service.facade.toleration.computer.module.service;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import com.flow.service.facade.toleration.TolerationModuleConstant;
import com.flow.service.facade.toleration.computer.pojo.dto.ComputerModuleDto;
import com.flow.service.facade.toleration.computer.pojo.vo.ComputerModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 电脑信息模块调用服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/1
 */
@FeignClient(contextId = "service-facade-test-toleration-computerModuleService",
        url = TolerationModuleConstant.URL_PREFIX_EXPRESSION,
        name = TolerationModuleConstant.APPLICATION_NAME_EXPRESSION,
        path = TolerationModuleConstant.RPC_PREFIX_EXPRESSION + "/rpc/computerModuleService")
public interface IComputerModuleService {


    /**
     * 创建电脑信息
     *
     * @param dto dto
     * @return
     */
    @PostMapping("/create")
    ComputerModuleVo create(@RequestBody @Valid
                            @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                    ComputerModuleDto dto);

    /**
     * 根据id获取电脑信息，不推荐使用@RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull这种方式，
     * 避免提示信息不准确，比如需要提示某场景下的某资源不存在；但如果从研发效率角度出发，该方法可以小幅度提升研发效率
     *
     * @param computerId computerId
     * @param allowNull  allowNull
     * @return
     */
    @GetMapping("/getById")
    ComputerModuleVo getById(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                             @RequestParam("computerId") String computerId,
                             @RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull);

    /**
     * 根据编码获取电脑信息
     *
     * @param computerCode computerCode
     * @param allowNull    allowNull
     * @return
     */
    @GetMapping("/getByCode")
    ComputerModuleVo getByCode(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                               @RequestParam("computerCode") String computerCode,
                               @RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull);
}