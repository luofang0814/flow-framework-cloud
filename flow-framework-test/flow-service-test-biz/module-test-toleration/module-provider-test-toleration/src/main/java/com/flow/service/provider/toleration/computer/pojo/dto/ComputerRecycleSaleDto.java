package com.flow.service.provider.toleration.computer.pojo.dto;

import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 电脑回收出售
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@ApiModel(description = "<p> 电脑回收出售 </p>")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ComputerRecycleSaleDto extends BaseModuleDto {

    private static final long serialVersionUID = 1L;

    /**
     * 电脑id
     */
    @ApiModelProperty("电脑id")
    @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String id;
}
