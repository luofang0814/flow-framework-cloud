package com.flow.service.provider.toleration.computer.module.service.impl;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import com.flow.service.facade.toleration.computer.module.service.IComputerModuleService;
import com.flow.service.facade.toleration.computer.pojo.dto.ComputerModuleDto;
import com.flow.service.facade.toleration.computer.pojo.vo.ComputerModuleVo;
import com.flow.service.provider.toleration.computer.service.IComputerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 电脑模块调用服务实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@Slf4j
@RestController
@RequestMapping("/rpc/applyModuleService")
@Validated
@RequiredArgsConstructor
public class ComputerModuleServiceImpl implements IComputerModuleService {

    private final IComputerService computerService;

    /**
     * 创建电脑信息
     *
     * @param dto dto
     * @return
     */
    @Override
    public ComputerModuleVo create(@RequestBody @Valid
                                   @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                           ComputerModuleDto dto) {
        return computerService.create(dto);
    }

    /**
     * 根据id获取电脑信息
     *
     * @param computerId computerId
     * @param allowNull  allowNull
     * @return
     */
    @Override
    public ComputerModuleVo getById(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                            String computerId, boolean allowNull) {
        return computerService.getModuleVoById(computerId, allowNull);
    }

    /**
     * 根据编码获取电脑信息
     *
     * @param computerCode computerCode
     * @param allowNull    allowNull
     * @return
     */
    @Override
    public ComputerModuleVo getByCode(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                              String computerCode, boolean allowNull) {
        return computerService.getModuleVoByCode(computerCode, allowNull);
    }
}