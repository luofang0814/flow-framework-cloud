package com.flow.service.provider.toleration.computer.controller;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerDto;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerPageQueryDto;
import com.flow.service.provider.toleration.computer.pojo.vo.ComputerVo;
import com.flow.service.provider.toleration.computer.service.IComputerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 电脑管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@Api(tags = "电脑管理")
@Slf4j
@RestController
@RequestMapping("/computer")
@Validated
@RequiredArgsConstructor
public class ComputerController {

    private final IComputerService computerService;

    @ApiOperation(value = "创建电脑信息")
    @PostMapping("/create")
    public ComputerVo create(@RequestBody @Valid ComputerDto dto) {
        return computerService.create(dto);
    }

    @ApiOperation(value = "分页查询电脑信息")
    @GetMapping("/page")
    public PageVo<ComputerVo> page(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                           ComputerPageQueryDto dto) {
        return computerService.page(dto);
    }

    @ApiOperation(value = "分页查询电脑信息")
    @GetMapping("/getById")
    public void getById(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                              @RequestParam(name = "computerId") @ApiParam(value = "电脑id", required = true)
                                      String computerId) {
//        return computerService.getVoById(computerId, true);
//        return null;
    }
}