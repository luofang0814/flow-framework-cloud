package com.flow.service.provider.toleration.apply.service;

import com.flow.framework.core.system.proxy.IProxy;
import com.flow.framework.lock.enumeration.ILockKeyEnum;

/**
 * 资源申请服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
public interface IResourceApplyService extends IProxy<String> {

    /**
     * 检查资源是否可以被申请
     *
     * @param type       申请类型
     * @param resourceId 资源id
     */
    void check(String type, String resourceId);

    /**
     * 获取分布式锁枚举，用于资源修改时加锁
     *
     * @param type 申请类型
     * @return
     */
    ILockKeyEnum getLockEnum(String type);

    /**
     * 修改资源状态为申请中
     *
     * @param type       申请类型
     * @param resourceId resourceId
     */
    void updateResourceApplying(String type, String resourceId);

    /**
     * 申请结果回调
     *
     * @param type       申请类型
     * @param success    是否申请成功
     * @param resourceId resourceId
     */
    void applyResultCallback(String type, boolean success, String resourceId);
}