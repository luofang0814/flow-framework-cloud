package com.flow.service.provider.toleration.apply.pojo.dto;

import com.flow.common.facade.experiment.apply.immutable.ApplyTypeImmutable;
import com.flow.framework.core.pojo.dto.base.BaseDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 申请
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
@ApiModel(description = "<p> 申请 </p>")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ApplyDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    /**
     * 申请no
     */
    @ApiModelProperty("申请no")
    @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String applyNo;

    /**
     * 申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请
     */
    @ApiModelProperty("申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请")
    @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_TYPE_ERROR, immutableInterface = ApplyTypeImmutable.class)
    private String applyType;

    /**
     * 申请资源的id
     */
    @ApiModelProperty("申请资源的id")
    @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String resourceId;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String userId;
}
