package com.flow.service.provider.toleration.computer.convert;

import com.flow.service.facade.toleration.computer.pojo.dto.ComputerModuleDto;
import com.flow.service.facade.toleration.computer.pojo.vo.ComputerModuleVo;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerDto;
import com.flow.service.provider.toleration.computer.pojo.po.Computer;
import com.flow.service.provider.toleration.computer.pojo.vo.ComputerVo;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public class ComputerConverter {

    private static ComputerConvert convert = ComputerConvert.INSTANCE;

    public static Computer convertToPo(ComputerDto o) {
        return convert.convertToPo(o);
    }

    public static Computer convertToPo(ComputerModuleDto o) {
        return convert.convertToPo(o);
    }

    public static ComputerModuleVo convertToModuleVo(Computer o) {
        return convert.convertToModuleVo(o);
    }

    public static ComputerVo convertToVo(Computer o) {
        return convert.convertToVo(o);
    }
}