package com.flow.service.provider.toleration.computer.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.flow.common.facade.toleration.computer.immutable.ComputerOccupancyStatusImmutable;
import com.flow.common.facade.toleration.computer.immutable.ComputerStatusImmutable;
import com.flow.common.facade.toleration.computer.immutable.ComputerTypeImmutable;
import com.flow.common.facade.toleration.computer.immutable.OsTypeImmutable;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.persistence.pojo.po.base.BaseBizPo;
import com.flow.service.facade.toleration.TolerationErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 电脑信息
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "toleration_computer", autoResultMap = true)
public class Computer extends BaseBizPo {

    private static final long serialVersionUID = 1L;

    /**
     * 电脑类型:01-台式；02-便携式
     */
    @ImmutableCustomization(errorCode = TolerationErrorCode.COMPUTER_TYPE_ERROR, immutableInterface = ComputerTypeImmutable.class)
    private String computerType;

    /**
     * 电脑编码
     */
    private String computerCode;

    /**
     * 电脑型号
     */
    private String computerModel;

    /**
     * 电脑硬件配置
     */
    private String hardwareConfig;

    /**
     * 操作系统类型：01-windows；01-centos
     */
    @ImmutableCustomization(errorCode = TolerationErrorCode.OS_TYPE_ERROR, immutableInterface = OsTypeImmutable.class)
    private String osType;

    /**
     * 操作系统版本
     */
    private String osVersion;

    /**
     * 电脑状态：01-正常；02-未知故障；03-硬件故障；04-软件故障；05-回收出售
     */
    @ImmutableCustomization(errorCode = TolerationErrorCode.COMPUTER_STATUS_ERROR, immutableInterface = ComputerStatusImmutable.class)
    private String computerStatus;

    /**
     * 电脑占用状态：01-申请中；02-使用中；03-空闲
     */
    @ImmutableCustomization(errorCode = TolerationErrorCode.COMPUTER_OCCUPANCY_STATUS_ERROR, immutableInterface = ComputerOccupancyStatusImmutable.class)
    private String computerOccupancyStatus;

    /**
     * 备注
     */
    private String remark;


}
