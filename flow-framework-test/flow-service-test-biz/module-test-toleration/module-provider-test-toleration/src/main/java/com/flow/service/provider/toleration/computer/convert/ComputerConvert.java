package com.flow.service.provider.toleration.computer.convert;

import com.flow.framework.common.struct.control.CommonMappingControl;
import com.flow.service.facade.toleration.computer.pojo.dto.ComputerModuleDto;
import com.flow.service.facade.toleration.computer.pojo.vo.ComputerModuleVo;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerDto;
import com.flow.service.provider.toleration.computer.pojo.po.Computer;
import com.flow.service.provider.toleration.computer.pojo.vo.ComputerVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@Mapper(mappingControl = CommonMappingControl.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface ComputerConvert {

    ComputerConvert INSTANCE = Mappers.getMapper(ComputerConvert.class);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    Computer convertToPo(ComputerDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    Computer convertToPo(ComputerModuleDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ComputerModuleVo convertToModuleVo(Computer o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ComputerVo convertToVo(Computer o);
}