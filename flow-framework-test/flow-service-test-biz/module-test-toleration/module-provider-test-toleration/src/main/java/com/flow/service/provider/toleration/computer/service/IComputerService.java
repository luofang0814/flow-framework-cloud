package com.flow.service.provider.toleration.computer.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.facade.toleration.computer.pojo.dto.ComputerModuleDto;
import com.flow.service.facade.toleration.computer.pojo.vo.ComputerModuleVo;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerDto;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerPageQueryDto;
import com.flow.service.provider.toleration.computer.pojo.vo.ComputerVo;

/**
 * 电脑业务服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface IComputerService {

    /**
     * 创建用户
     *
     * @param dto dto
     * @return
     */
    ComputerVo create(ComputerDto dto);

    /**
     * 创建用户
     *
     * @param dto dto
     * @return
     */
    ComputerModuleVo create(ComputerModuleDto dto);

    /**
     * 根据id获取电脑信息
     *
     * @param computerId computerId
     * @param allowNull  allowNull
     * @return
     */
    ComputerModuleVo getModuleVoById(String computerId, boolean allowNull);

    /**
     * 根据id获取电脑信息
     *
     * @param computerId computerId
     * @param allowNull  allowNull
     * @return
     */
    ComputerVo getVoById(String computerId, boolean allowNull);

    /**
     * 根据编码获取电脑信息
     *
     * @param computerCode computerCode
     * @param allowNull    allowNull
     * @return
     */
    ComputerModuleVo getModuleVoByCode(String computerCode, boolean allowNull);

    /**
     * 电脑信息分页查询
     *
     * @param dto dto
     * @return
     */
    PageVo<ComputerVo> page(ComputerPageQueryDto dto);
}