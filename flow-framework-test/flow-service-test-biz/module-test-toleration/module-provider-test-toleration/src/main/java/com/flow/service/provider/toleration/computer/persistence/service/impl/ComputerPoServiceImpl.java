package com.flow.service.provider.toleration.computer.persistence.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flow.common.facade.toleration.computer.immutable.ComputerOccupancyStatusImmutable;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.persistence.persistence.page.Page;
import com.flow.framework.persistence.persistence.service.impl.PersistenceServiceImpl;
import com.flow.service.facade.toleration.TolerationErrorCode;
import com.flow.service.provider.toleration.computer.persistence.mapper.ComputerMapper;
import com.flow.service.provider.toleration.computer.persistence.service.IComputerPoService;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerPageQueryDto;
import com.flow.service.provider.toleration.computer.pojo.po.Computer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 电脑信息 服务实现类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@Slf4j
@Service
public class ComputerPoServiceImpl extends PersistenceServiceImpl<ComputerMapper, Computer> implements IComputerPoService {

    @Override
    public boolean save(Computer entity) {
        return service().save(entity);
    }

    @Override
    public boolean saveBatch(Collection<Computer> entityList) {
        return service().saveBatch(entityList);
    }

    @Override
    public boolean saveBatch(Collection<Computer> entityList, int batchSize) {
        return service().saveBatch(entityList, batchSize);
    }

    @Override
    public boolean saveOrUpdate(Computer entity) {
        return service().saveOrUpdate(entity);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Computer> entityList) {
        return service().saveOrUpdateBatch(entityList);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Computer> entityList, int batchSize) {
        return service().saveOrUpdateBatch(entityList, batchSize);
    }

    @Override
    public Computer getById(String id, boolean allowNull) {
        Computer computer = service().getById(id);
        if (!allowNull && null == computer) {
            log.error("can't find po. id : {}", id);
            throw new CheckedException(TolerationErrorCode.COMPUTER_NOT_EXIST_ERROR);
        }
        return computer;
    }

    @Override
    public List<Computer> listByIds(Collection<String> idList, boolean allowNotExist) {
        List<Computer> pos = service().listByIds(idList);
        if (!allowNotExist && pos.size() != idList.size()) {
            log.error("some pos can't be found. ids : {}", idList);
            throw new CheckedException(TolerationErrorCode.COMPUTER_NOT_EXIST_ERROR);
        }
        return pos;
    }

    @Override
    public boolean removeById(String id) {
        return service().removeById(id);
    }

    @Override
    public boolean removeByIds(Collection<String> idList) {
        return service().removeByIds(idList);
    }

    @Override
    public boolean updateById(Computer entity) {
        return service().updateById(entity);
    }

    @Override
    public boolean updateBatchById(Collection<Computer> entityList) {
        return service().updateBatchById(entityList);
    }

    @Override
    public boolean updateBatchById(Collection<Computer> entityList, int batchSize) {
        return service().updateBatchById(entityList, batchSize);
    }

    /**
     * 检查电脑编号是否存在
     *
     * @param computerCode computerCode
     * @return
     */
    @Override
    public boolean isComputerCodeExist(String computerCode) {
        return service().lambdaQuery().eq(Computer::getComputerCode, computerCode).count() > 0;
    }

    /**
     * 根据电脑编号获取电脑信息
     *
     * @param computerCode computerCode
     * @return
     */
    @Override
    public Computer getByCode(String computerCode, boolean allowNull) {
        Computer computer = service()
                .getOne(new LambdaQueryWrapper<Computer>().eq(Computer::getComputerCode, computerCode), false);
        if (!allowNull && null == computer) {
            log.error("can't find computer. computer code : {}", computerCode);
            throw new CheckedException(TolerationErrorCode.COMPUTER_NOT_EXIST_ERROR);
        }
        return computer;
    }

    /**
     * 分页查询电脑信息
     *
     * @param dto dto
     * @return
     */
    @Override
    public Page<Computer> page(ComputerPageQueryDto dto) {
        return service().lambdaQuery()
                .eq(!VerifyUtil.isEmpty(dto.getComputerStatus()), Computer::getComputerStatus, dto.getComputerStatus())
                .eq(!VerifyUtil.isEmpty(dto.getComputerCode()), Computer::getComputerCode, dto.getComputerCode())
                .eq(!VerifyUtil.isEmpty(dto.getComputerType()), Computer::getComputerType, dto.getComputerType())
                .eq(!VerifyUtil.isEmpty(dto.getHardwareConfig()), Computer::getHardwareConfig, dto.getHardwareConfig())
                .eq(!VerifyUtil.isEmpty(dto.getOsVersion()), Computer::getOsVersion, dto.getOsVersion())
                .likeRight(!VerifyUtil.isEmpty(dto.getOsType()), Computer::getOsType, dto.getOsType())
                .likeRight(!VerifyUtil.isEmpty(dto.getComputerModel()), Computer::getComputerModel, dto.getComputerModel())
                .page(Page.of(dto));
    }

    /**
     * 修改资源状态为申请中
     *
     * @param resourceId resourceId
     */
    @Override
    public void updateResourceApplying(String resourceId) {
        boolean success = service().lambdaUpdate().set(Computer::getComputerOccupancyStatus, ComputerOccupancyStatusImmutable.APPLYING)
                .eq(Computer::getId, resourceId)
                .eq(Computer::getComputerOccupancyStatus, ComputerOccupancyStatusImmutable.IDLE)
                .update();
        if (!success) {
            log.error("update computer occupancy status error. id : {}", resourceId);
            throw new CheckedException(TolerationErrorCode.COMPUTER_OCCUPANCY_STATUS_UPDATE_ERROR);
        }
    }
}
