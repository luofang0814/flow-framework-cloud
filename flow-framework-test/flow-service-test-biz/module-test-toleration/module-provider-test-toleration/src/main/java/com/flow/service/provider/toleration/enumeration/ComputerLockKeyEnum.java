package com.flow.service.provider.toleration.enumeration;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 电脑信息分布式锁
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@AllArgsConstructor
@Getter
public enum ComputerLockKeyEnum implements ILockKeyEnum {

    /**
     * 电脑编码检查
     */
    COMPUTER_CODE_CHECK_LOCK_KEY(1, "电脑编码检查"),

    /**
     * 电脑占用状态检查
     */
    COMPUTER_OCCUPANCY_STATUS_CHECK_LOCK_KEY(1, "电脑占用状态检查"),

    /**
     * 电脑状态检查
     */
    COMPUTER_STATUS_CHECK_LOCK_KEY(1, "电脑状态检查"),
    ;

    private int paramsSize;

    private String remark;
}
