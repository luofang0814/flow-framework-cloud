package com.flow.service.provider.toleration.apply.convert;

import com.flow.framework.common.struct.control.CommonMappingControl;
import com.flow.service.provider.toleration.apply.pojo.dto.ApplyDto;
import com.flow.service.provider.toleration.apply.pojo.vo.ApplyVo;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
@Mapper(mappingControl = CommonMappingControl.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface ApplyConvert {

    ApplyConvert INSTANCE = Mappers.getMapper(ApplyConvert.class);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ApplyVo convertToVo(ApplyModuleVo o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ApplyModuleDto convertToModuleDto(ApplyDto o);
}