package com.flow.service.provider.toleration.apply.service;

import com.flow.service.provider.toleration.apply.pojo.dto.ApplyDto;
import com.flow.service.provider.toleration.apply.pojo.vo.ApplyVo;

/**
 * 申请业务服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
public interface IApplyService {

    /**
     * 创建申请
     *
     * @param dto dto
     * @return
     */
    ApplyVo create(ApplyDto dto);

    /**
     * 根据申请编号获取申请
     *
     * @param applyNo applyNo
     * @return
     */
    ApplyVo getByApplyNo(String applyNo);
}