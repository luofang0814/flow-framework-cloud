package com.flow.service.provider.toleration.computer.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.flow.service.provider.toleration.computer.pojo.po.Computer;

/**
 * <p>
 * 电脑信息 Mapper 接口
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface ComputerMapper extends BaseMapper<Computer> {

}
