package com.flow.service.provider.toleration.computer.persistence.service;

import com.flow.framework.persistence.persistence.page.Page;
import com.flow.service.provider.toleration.computer.pojo.dto.ComputerPageQueryDto;
import com.flow.service.provider.toleration.computer.pojo.po.Computer;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 电脑信息 服务类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface IComputerPoService {

    /**
     * 保存
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean save(Computer entity);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveBatch(Collection<Computer> entityList);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveBatch(Collection<Computer> entityList, int batchSize);

    /**
     * 保存或更新，根据id是否存在判断
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean saveOrUpdate(Computer entity);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Computer> entityList);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Computer> entityList, int batchSize);

    /**
     * 根据id获取实体
     *
     * @param id        id
     * @param allowNull allowNull
     * @return 实体
     */
    Computer getById(String id, boolean allowNull);

    /**
     * 批量根据id获取实体
     *
     * @param idList        idList
     * @param allowNotExist allowNotExist
     * @return 实体集合
     */
    List<Computer> listByIds(Collection<String> idList, boolean allowNotExist);

    /**
     * 根据id删除实体
     *
     * @param id id
     * @return 是否成功
     */
    boolean removeById(String id);

    /**
     * 批量根据id删除实体
     *
     * @param idList idList
     * @return 是否成功
     */
    boolean removeByIds(Collection<String> idList);

    /**
     * 根据id更新实体
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean updateById(Computer entity);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Computer> entityList);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Computer> entityList, int batchSize);

    /**
     * 检查电脑编号是否存在
     *
     * @param computerCode computerCode
     * @return
     */
    boolean isComputerCodeExist(String computerCode);

    /**
     * 根据电脑编号获取电脑信息
     *
     * @param computerCode computerCode
     * @param allowNull    allowNull
     * @return
     */
    Computer getByCode(String computerCode, boolean allowNull);

    /**
     * 分页查询电脑信息
     *
     * @param dto dto
     * @return
     */
    Page<Computer> page(ComputerPageQueryDto dto);

    /**
     * 修改资源状态为申请中
     *
     * @param resourceId resourceId
     */
    void updateResourceApplying(String resourceId);
}
