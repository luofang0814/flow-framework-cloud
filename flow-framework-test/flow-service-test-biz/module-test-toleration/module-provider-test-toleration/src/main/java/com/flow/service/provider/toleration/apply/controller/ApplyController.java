package com.flow.service.provider.toleration.apply.controller;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import com.flow.service.provider.toleration.apply.pojo.dto.ApplyDto;
import com.flow.service.provider.toleration.apply.pojo.vo.ApplyVo;
import com.flow.service.provider.toleration.apply.service.IApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 电脑管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
@Api(tags = "申请管理")
@Slf4j
@RestController
@RequestMapping("/apply")
@Validated
@RequiredArgsConstructor
public class ApplyController {

    private final IApplyService applyService;

    @ApiOperation(value = "创建申请")
    @PostMapping("/create")
    public ApplyVo create(@RequestBody @Valid ApplyDto dto) {
        return applyService.create(dto);
    }

    @ApiOperation(value = "根据申请编号获取申请")
    @GetMapping("/getByApplyNo")
    public ApplyVo getByApplyNo(@Valid @NotEmptyCustomization(errorCode = TolerationErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                @RequestParam(name = "applyNo") @ApiParam(value = "申请编号", required = true)
                                        String applyNo) {
        return applyService.getByApplyNo(applyNo);
    }
}