package com.flow.service.provider.toleration.apply.convert;

import com.flow.service.provider.toleration.apply.pojo.dto.ApplyDto;
import com.flow.service.provider.toleration.apply.pojo.vo.ApplyVo;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/13
 */
public class ApplyConverter {

    private static ApplyConvert convert = ApplyConvert.INSTANCE;

    public static ApplyVo convertToVo(ApplyModuleVo o) {
        return convert.convertToVo(o);
    }

    public static ApplyModuleDto convertToModuleDto(ApplyDto o) {
        return convert.convertToModuleDto(o);
    }
}