package com.flow.service.provider.toleration.computer.pojo.dto;

import com.flow.common.facade.toleration.computer.immutable.ComputerStatusImmutable;
import com.flow.common.facade.toleration.computer.immutable.ComputerTypeImmutable;
import com.flow.common.facade.toleration.computer.immutable.OsTypeImmutable;
import com.flow.framework.core.pojo.dto.base.BasePageQueryDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.toleration.TolerationErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 电脑信息分页查询
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
@ApiModel(description = "电脑信息分页查询")
@Data
@EqualsAndHashCode(callSuper = true)
public class ComputerPageQueryDto extends BasePageQueryDto {

    /**
     * 电脑类型:01-台式；02-便携式
     */
    @ApiModelProperty("电脑类型:01-台式；02-便携式")
    @ImmutableCustomization(errorCode = TolerationErrorCode.COMPUTER_TYPE_ERROR, immutableInterface = ComputerTypeImmutable.class)
    private String computerType;

    /**
     * 电脑编码
     */
    @ApiModelProperty("电脑编码")
    private String computerCode;

    /**
     * 电脑型号
     */
    @ApiModelProperty("电脑型号")
    private String computerModel;

    /**
     * 电脑硬件配置
     */
    @ApiModelProperty("电脑硬件配置")
    private String hardwareConfig;

    /**
     * 操作系统类型：01-windows；01-centos
     */
    @ApiModelProperty("操作系统类型：01-windows；01-centos")
    @ImmutableCustomization(errorCode = TolerationErrorCode.OS_TYPE_ERROR, immutableInterface = OsTypeImmutable.class)
    private String osType;

    /**
     * 操作系统版本
     */
    @ApiModelProperty("操作系统版本")
    private String osVersion;

    /**
     * 电脑状态：01-正常；02-未知故障；03-硬件故障；04-软件故障；05-回收出售
     */
    @ApiModelProperty("电脑状态：01-正常；02-未知故障；03-硬件故障；04-软件故障；05-回收出售")
    @ImmutableCustomization(errorCode = TolerationErrorCode.COMPUTER_STATUS_ERROR, immutableInterface = ComputerStatusImmutable.class)
    private String computerStatus;
}