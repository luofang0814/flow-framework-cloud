package com.flow.common.facade.check.role.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 角色类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface RoleTypeImmutable extends Immutable {

    @ImmutableRemark("外部")
    String OUTER = "01";

    @ImmutableRemark("内部")
    String INNER = "02";
}