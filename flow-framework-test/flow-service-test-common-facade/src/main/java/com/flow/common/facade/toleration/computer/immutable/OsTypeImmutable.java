package com.flow.common.facade.toleration.computer.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 操作系统类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface OsTypeImmutable extends Immutable {

    /**
     * windows操作系统
     */
    @ImmutableRemark("windows操作系统")
    String WINDOWS = "01";

    /**
     * CentOS操作系统
     */
    @ImmutableRemark("CentOS操作系统")
    String CENT_OS = "02";
}