package com.flow.common.facade.experiment.apply.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 申请类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ApplyTypeImmutable extends Immutable {

    @ImmutableRemark("电脑使用申请")
    String COMPUTER_USE_APPLY = "01";

    @ImmutableRemark("电脑归还申请")
    String COMPUTER_BACK_APPLY = "02";

    @ImmutableRemark("电脑回收出售申请")
    String COMPUTER_RECYCLE_SALE_APPLY = "03";
}