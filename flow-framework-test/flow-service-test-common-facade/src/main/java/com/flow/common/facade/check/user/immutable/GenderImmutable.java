package com.flow.common.facade.check.user.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 性别
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface GenderImmutable extends Immutable {

    @ImmutableRemark("男")
    String MAN = "1";

    @ImmutableRemark("女")
    String WOMAN = "2";
}