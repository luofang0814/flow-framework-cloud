package com.flow.common.facade.toleration.computer.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 电脑状态
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface ComputerStatusImmutable extends Immutable {

    /**
     * 正常
     */
    @ImmutableRemark("正常")
    String NORMAL = "01";

    /**
     * 未知故障
     */
    @ImmutableRemark("未知故障")
    String UNKNOWN_FAILURE = "02";

    /**
     * 硬件故障
     */
    @ImmutableRemark("硬件故障")
    String HARDWARE_FAILURE = "03";

    /**
     * 软件故障
     */
    @ImmutableRemark("软件故障")
    String SOFTWARE_FAILURE = "04";

    /**
     * 回收出售
     */
    @ImmutableRemark("回收出售")
    String RECYCLE_SOLD = "05";
}