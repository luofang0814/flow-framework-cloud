package com.flow.common.facade.check.user.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 用户类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface UserTypeImmutable extends Immutable {

    @ImmutableRemark("个人")
    String INDIVIDUAL = "01";

    @ImmutableRemark("公司")
    String COMPANY = "02";
}