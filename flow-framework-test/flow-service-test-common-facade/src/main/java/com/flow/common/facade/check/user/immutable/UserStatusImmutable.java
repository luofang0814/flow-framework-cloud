package com.flow.common.facade.check.user.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 用户状态
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface UserStatusImmutable extends Immutable {

    @ImmutableRemark("正常")
    String NORMAL = "01";

    @ImmutableRemark("冻结")
    String FREEZE = "02";
}