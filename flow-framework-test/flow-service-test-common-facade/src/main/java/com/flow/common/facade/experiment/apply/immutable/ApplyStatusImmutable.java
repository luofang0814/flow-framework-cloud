package com.flow.common.facade.experiment.apply.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 申请状态
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ApplyStatusImmutable extends Immutable {

    @ImmutableRemark("待审批")
    String PENDING = "01";

    @ImmutableRemark("通过")
    String PASS = "02";

    @ImmutableRemark("拒绝")
    String REFUSE = "03";
}