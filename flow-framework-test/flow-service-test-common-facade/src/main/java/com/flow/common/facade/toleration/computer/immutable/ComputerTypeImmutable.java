package com.flow.common.facade.toleration.computer.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 电脑类型
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface ComputerTypeImmutable extends Immutable {

    /**
     * 台式电脑
     */
    @ImmutableRemark("台式电脑")
    String DESKTOP_COMPUTER = "01";

    /**
     * 便携式电脑
     */
    @ImmutableRemark("便携式电脑")
    String NOTEBOOK_COMPUTER = "02";
}