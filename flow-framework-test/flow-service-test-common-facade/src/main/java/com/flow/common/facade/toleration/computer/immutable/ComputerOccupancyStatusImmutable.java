package com.flow.common.facade.toleration.computer.immutable;

import com.flow.framework.common.immutable.Immutable;
import com.flow.framework.common.immutable.annotation.ImmutableRemark;

/**
 * 电脑占用状态
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/5/2
 */
public interface ComputerOccupancyStatusImmutable extends Immutable {

    /**
     * 申请中
     */
    @ImmutableRemark("申请中")
    String APPLYING = "01";

    /**
     * 使用中
     */
    @ImmutableRemark("使用中")
    String IN_USE = "02";

    /**
     * 空闲
     */
    @ImmutableRemark("空闲")
    String IDLE = "03";
}