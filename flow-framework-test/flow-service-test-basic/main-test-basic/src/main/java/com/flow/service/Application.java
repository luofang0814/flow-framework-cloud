package com.flow.service;

import com.flow.framework.module.call.rpc.annotation.EnableCustomizationFeignClients;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 服务启动类
 * 初始化feign client时需要排除当前项目自己的feign client
 * 本项目自己调用自己的feign client时，只需要在feign client类上加@Validated并且在方法上加@Valid（也可以是自定义注解）
 * 此时spring同样会像controller一样去校验相关参数
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/8
 */
@SpringBootApplication
@EnableCustomizationFeignClients(basePackages = {"com.flow"}, excludePackages = {
        "com.flow.service.facade.check",
        "com.flow.service.facade.experiment"
})
@MapperScan({"com.flow.**.mapper"})
@ComponentScan({"com.flow"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
