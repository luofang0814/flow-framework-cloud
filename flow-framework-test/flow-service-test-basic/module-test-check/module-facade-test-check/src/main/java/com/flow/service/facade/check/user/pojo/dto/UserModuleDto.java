package com.flow.service.facade.check.user.pojo.dto;

import com.flow.common.facade.check.user.immutable.GenderImmutable;
import com.flow.common.facade.check.user.immutable.UserStatusImmutable;
import com.flow.common.facade.check.user.immutable.UserTypeImmutable;
import com.flow.framework.core.pojo.dto.base.module.BaseModuleDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserModuleDto extends BaseModuleDto {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编号
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String roleCode;

    /**
     * 用户姓名
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String userName;

    /**
     * 登录账号
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String loginAccount;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String mobilePhone;

    /**
     * 花名
     */
    private String nickName;

    /**
     * 用户类型:01-个人；02-公司
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_TYPE_ERROR, immutableInterface = UserTypeImmutable.class)
    private String userType;

    /**
     * 是否超级管理员
     */
    private Boolean isAdmin;

    /**
     * 性别：1-男；2-女
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = CheckErrorCode.GENDER_ERROR, immutableInterface = GenderImmutable.class)
    private String gender;

    /**
     * 状态:01-正常；02-冻结
     */
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_STATUS_ERROR, immutableInterface = UserStatusImmutable.class)
    private String status;
}
