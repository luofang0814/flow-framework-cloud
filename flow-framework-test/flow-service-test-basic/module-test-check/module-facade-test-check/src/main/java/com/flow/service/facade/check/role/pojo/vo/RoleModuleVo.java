package com.flow.service.facade.check.role.pojo.vo;

import com.flow.common.facade.check.role.immutable.RoleTypeImmutable;
import com.flow.framework.core.pojo.vo.base.BaseBizModuleVo;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@ApiModel(description = "<p> 角色 </p>")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RoleModuleVo extends BaseBizModuleVo {

    private static final long serialVersionUID = 1L;


    /**
     * 角色类型:01-外部；02-内部
     */
    @ImmutableCustomization(errorCode = CheckErrorCode.ROLE_TYPE_ERROR, immutableInterface = RoleTypeImmutable.class)
    private String roleType;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 扩展字段，json结构
     */
    private String extraInfo;


}
