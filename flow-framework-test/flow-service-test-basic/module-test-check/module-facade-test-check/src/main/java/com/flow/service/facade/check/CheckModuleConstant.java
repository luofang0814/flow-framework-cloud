package com.flow.service.facade.check;

/**
 * check模块常量类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface CheckModuleConstant {

    /**
     * URL调用前缀表达式
     */
    String URL_PREFIX_EXPRESSION = "${customization.service.facade.check.url:}";

    /**
     * application name表达式
     */
    String APPLICATION_NAME_EXPRESSION = "${customization.service.facade.check.application-name:flow-service-test-common}";

    /**
     * RPC调用前缀表达式
     */
    String RPC_PREFIX_EXPRESSION = "${customization.service.facade.check.rpc-prefix:/testCommon}";
}