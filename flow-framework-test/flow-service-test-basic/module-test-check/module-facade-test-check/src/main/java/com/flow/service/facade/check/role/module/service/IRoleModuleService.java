package com.flow.service.facade.check.role.module.service;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.facade.check.CheckModuleConstant;
import com.flow.service.facade.check.role.pojo.vo.RoleModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * 角色模块调用服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@FeignClient(contextId = "service-facade-test-check-roleModuleService",
        url = CheckModuleConstant.URL_PREFIX_EXPRESSION,
        name = CheckModuleConstant.APPLICATION_NAME_EXPRESSION,
        path = CheckModuleConstant.RPC_PREFIX_EXPRESSION + "/rpc/roleModuleService")
public interface IRoleModuleService {


    /**
     * 根据角色编码获取角色信息
     *
     * @param roleCode roleCode
     * @return
     */
    @GetMapping("/getRoleByCode")
    RoleModuleVo getRoleByCode(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                               @RequestParam("roleCode") String roleCode,
                               @RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull);
}