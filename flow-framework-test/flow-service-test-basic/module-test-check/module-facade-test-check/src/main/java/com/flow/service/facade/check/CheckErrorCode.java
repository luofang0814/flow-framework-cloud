package com.flow.service.facade.check;

/**
 * check模块错误码类
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface CheckErrorCode {

    /**
     * 参数不能为空
     */
    long INPUT_OBJECT_EMPTY_ERROR = 20001;

    /**
     * 参数属性为空错误
     */
    long INPUT_PROPERTY_EMPTY_ERROR = 20002;

    /**
     * 角色类型错误
     */
    long ROLE_TYPE_ERROR = 20003;

    /**
     * 用户类型错误
     */
    long USER_TYPE_ERROR = 20004;

    /**
     * 性别错误
     */
    long GENDER_ERROR = 20005;

    /**
     * 用户状态错误
     */
    long USER_STATUS_ERROR = 20006;

    /**
     * 用户状态错误
     */
    long ROLE_CODE_ERROR = 20007;

    /**
     * 角色编码已存在
     */
    long ROLE_CODE_EXIST_ERROR = 20008;

    /**
     * 用户不存在错误
     */
    long USER_NOT_EXIST_ERROR = 20009;

    /**
     * 角色不存在错误
     */
    long ROLE_NOT_EXIST_ERROR = 20010;
}
