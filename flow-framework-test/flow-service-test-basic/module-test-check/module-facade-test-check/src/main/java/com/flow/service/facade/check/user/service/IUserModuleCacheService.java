package com.flow.service.facade.check.user.service;

import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;

/**
 * 用户模块缓存服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface IUserModuleCacheService {

    /**
     * 根据id获取用户
     *
     * @param userId    userId
     * @param allowNull allowNull
     * @return
     */
    UserModuleVo getById(String userId, boolean allowNull);
}