package com.flow.service.facade.check.user.module.service;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.facade.check.CheckModuleConstant;
import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户模块调用服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@FeignClient(contextId = "service-facade-test-check-userModuleService",
        url = CheckModuleConstant.URL_PREFIX_EXPRESSION,
        name = CheckModuleConstant.APPLICATION_NAME_EXPRESSION,
        path = CheckModuleConstant.RPC_PREFIX_EXPRESSION + "/rpc/userModuleService")
public interface IUserModuleService {


    /**
     * 创建用户
     *
     * @param dto userCreateModuleDto
     * @return
     */
    @PostMapping("/create")
    UserModuleVo create(@RequestBody @Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                UserModuleDto dto);

    /**
     * 根据id获取用户
     *
     * @param userId    userId
     * @param allowNull allowNull
     * @return
     */
    @Deprecated
    @GetMapping("/getById")
    UserModuleVo getById(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                         @RequestParam("userId") String userId,
                         @RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull);
}