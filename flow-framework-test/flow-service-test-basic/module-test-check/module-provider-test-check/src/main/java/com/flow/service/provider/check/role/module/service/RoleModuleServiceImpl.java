package com.flow.service.provider.check.role.module.service;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.facade.check.role.module.service.IRoleModuleService;
import com.flow.service.facade.check.role.pojo.vo.RoleModuleVo;
import com.flow.service.provider.check.role.service.IRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 角色模块服务实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
@Slf4j
@RestController
@RequestMapping("/rpc/roleModuleService")
@Validated
@RequiredArgsConstructor
public class RoleModuleServiceImpl implements IRoleModuleService {

    private final IRoleService roleService;

    /**
     * 根据角色编码获取角色信息
     *
     * @param roleCode roleCode
     * @return
     */
    @Override
    public RoleModuleVo getRoleByCode(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                      @RequestParam("roleCode") String roleCode, boolean allowNull) {
        return roleService.getRoleByCode(roleCode, allowNull);
    }
}