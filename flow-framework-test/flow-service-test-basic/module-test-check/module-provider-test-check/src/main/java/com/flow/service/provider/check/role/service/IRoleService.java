package com.flow.service.provider.check.role.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.facade.check.role.pojo.vo.RoleModuleVo;
import com.flow.service.provider.check.role.pojo.dto.RoleCreateDto;
import com.flow.service.provider.check.role.pojo.dto.RolePageQueryDto;
import com.flow.service.provider.check.role.pojo.vo.RoleVo;

/**
 * 角色服务业务服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
public interface IRoleService {

    /**
     * 根据角色编码获取角色信息
     *
     * @param roleCode  roleCode
     * @param allowNull allowNull
     * @return
     */
    RoleModuleVo getRoleByCode(String roleCode, boolean allowNull);

    /**
     * 创建角色
     *
     * @param dto dto
     * @return
     */
    RoleVo create(RoleCreateDto dto);

    /**
     * 分页查询角色
     *
     * @param dto dto
     * @return
     */
    PageVo<RoleVo> page(RolePageQueryDto dto);
}