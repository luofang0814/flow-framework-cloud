package com.flow.service.provider.check.role.convert;

import com.flow.service.facade.check.role.pojo.vo.RoleModuleVo;
import com.flow.service.provider.check.role.pojo.dto.RoleCreateDto;
import com.flow.service.provider.check.role.pojo.po.Role;
import com.flow.service.provider.check.role.pojo.vo.RoleVo;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
public class RoleConverter {

    private static RoleConvert convert = RoleConvert.INSTANCE;

    public static RoleModuleVo convertToModuleVo(Role o) {
        return convert.convertToModuleVo(o);
    }

    public static Role convertToPo(RoleCreateDto o) {
        return convert.convertToPo(o);
    }

    public static RoleVo convertToVo(Role o) {
        return convert.convertToVo(o);
    }
}