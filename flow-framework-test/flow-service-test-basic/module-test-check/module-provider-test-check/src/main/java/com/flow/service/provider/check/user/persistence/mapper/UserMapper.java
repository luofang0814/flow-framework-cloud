package com.flow.service.provider.check.user.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 包含角色信息的用户用户分页查询
     *
     * @param page page
     * @param pageQueryDto pageQueryDto
     * @return
     */
    IPage<UserRoleVo> page(IPage page, @Param("pageQueryDto") UserRolePageQueryDto pageQueryDto);
}
