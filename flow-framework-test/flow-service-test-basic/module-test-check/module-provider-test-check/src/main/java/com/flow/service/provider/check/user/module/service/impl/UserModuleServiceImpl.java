package com.flow.service.provider.check.user.module.service.impl;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.facade.check.user.module.service.IUserModuleService;
import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import com.flow.service.provider.check.user.service.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户模块调用服务实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@Slf4j
@RestController
@RequestMapping("/rpc/userModuleService")
@Validated
@RequiredArgsConstructor
public class UserModuleServiceImpl implements IUserModuleService {

    private final IUserService userService;

    /**
     * 创建用户
     *
     * @param dto userCreateModuleDto
     * @return
     */
    @Override
    public UserModuleVo create(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR) UserModuleDto dto) {
        return userService.create(dto);
    }

    /**
     * 根据id获取用户
     *
     * @param userId    userId
     * @param allowNull allowNull
     * @return
     */
    @Override
    @SuppressWarnings("deprecation")
    public UserModuleVo getById(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                        String userId, boolean allowNull) {
        return userService.getById(userId, allowNull);
    }
}