package com.flow.service.provider.check.role.convert;

import com.flow.framework.common.struct.control.CommonMappingControl;
import com.flow.service.facade.check.role.pojo.vo.RoleModuleVo;
import com.flow.service.provider.check.role.pojo.dto.RoleCreateDto;
import com.flow.service.provider.check.role.pojo.po.Role;
import com.flow.service.provider.check.role.pojo.vo.RoleVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
@Mapper(mappingControl = CommonMappingControl.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface RoleConvert {

    RoleConvert INSTANCE = Mappers.getMapper(RoleConvert.class);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    RoleModuleVo convertToModuleVo(Role o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    Role convertToPo(RoleCreateDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    RoleVo convertToVo(Role o);
}