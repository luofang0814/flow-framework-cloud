package com.flow.service.provider.check.user.pojo.dto;

import com.flow.common.facade.check.role.immutable.RoleTypeImmutable;
import com.flow.common.facade.check.user.immutable.GenderImmutable;
import com.flow.common.facade.check.user.immutable.UserStatusImmutable;
import com.flow.common.facade.check.user.immutable.UserTypeImmutable;
import com.flow.framework.core.pojo.dto.base.BasePageQueryDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户角色分页查询dto
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserRolePageQueryDto extends BasePageQueryDto {
    /**
     * 角色编号
     */
    @ApiModelProperty("角色编号")
    private String roleCode;

    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String userName;

    /**
     * 登录账号
     */
    @ApiModelProperty("登录账号")
    private String loginAccount;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 手机
     */
    @ApiModelProperty("手机")
    private String mobilePhone;

    /**
     * 花名
     */
    @ApiModelProperty("花名")
    private String nickName;

    /**
     * 用户类型:01-个人；02-公司
     */
    @ApiModelProperty("用户类型:01-个人；02-公司")
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_TYPE_ERROR, immutableInterface = UserTypeImmutable.class)
    private String userType;

    /**
     * 是否超级管理员
     */
    @ApiModelProperty("是否超级管理员")
    private Boolean isAdmin;

    /**
     * 性别：1-男；2-女
     */
    @ApiModelProperty("性别：1-男；2-女")
    @ImmutableCustomization(errorCode = CheckErrorCode.GENDER_ERROR, immutableInterface = GenderImmutable.class)
    private String gender;

    /**
     * 状态:01-正常；02-冻结
     */
    @ApiModelProperty("状态:01-正常；02-冻结")
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_STATUS_ERROR, immutableInterface = UserStatusImmutable.class)
    private String status;


    /**
     * 角色类型:01-外部；02-内部
     */
    @ApiModelProperty("角色类型:01-外部；02-内部")
    @ImmutableCustomization(errorCode = CheckErrorCode.ROLE_TYPE_ERROR, immutableInterface = RoleTypeImmutable.class)
    private String roleType;


    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;
}