package com.flow.service.provider.check.role.persistence.service.impl;

import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.persistence.persistence.page.Page;
import com.flow.framework.persistence.persistence.service.impl.PersistenceServiceImpl;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.provider.check.role.persistence.mapper.RoleMapper;
import com.flow.service.provider.check.role.persistence.service.IRolePoService;
import com.flow.service.provider.check.role.pojo.dto.RolePageQueryDto;
import com.flow.service.provider.check.role.pojo.po.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@Slf4j
@Service
public class RolePoServiceImpl extends PersistenceServiceImpl<RoleMapper, Role> implements IRolePoService {

    @Override
    public boolean save(Role entity) {
        return service().save(entity);
    }

    @Override
    public boolean saveBatch(Collection<Role> entityList) {
        return service().saveBatch(entityList);
    }

    @Override
    public boolean saveBatch(Collection<Role> entityList, int batchSize) {
        return service().saveBatch(entityList, batchSize);
    }

    @Override
    public boolean saveOrUpdate(Role entity) {
        return service().saveOrUpdate(entity);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Role> entityList) {
        return service().saveOrUpdateBatch(entityList);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Role> entityList, int batchSize) {
        return service().saveOrUpdateBatch(entityList, batchSize);
    }

    @Override
    public Role getById(String id, boolean allowNull) {
        Role po = service().getById(id);
        if (!allowNull && null == po) {
            log.error("can't find po. id : {}", id);
            throw new CheckedException(CheckErrorCode.ROLE_NOT_EXIST_ERROR);
        }
        return po;
    }

    @Override
    public List<Role> listByIds(Collection<String> idList, boolean allowNotExist) {
        List<Role> pos = service().listByIds(idList);
        if (!allowNotExist && pos.size() != idList.size()) {
            log.error("some pos can't be found. ids : {}", idList);
            throw new CheckedException(CheckErrorCode.ROLE_NOT_EXIST_ERROR);
        }
        return pos;
    }

    @Override
    public boolean removeById(String id) {
        return service().removeById(id);
    }

    @Override
    public boolean removeByIds(Collection<String> idList) {
        return service().removeByIds(idList);
    }

    @Override
    public boolean updateById(Role entity) {
        return service().updateById(entity);
    }

    @Override
    public boolean updateBatchById(Collection<Role> entityList) {
        return service().updateBatchById(entityList);
    }

    @Override
    public boolean updateBatchById(Collection<Role> entityList, int batchSize) {
        return service().updateBatchById(entityList, batchSize);
    }

    @Override
    public Role getRoleByCode(String roleCode, boolean allowNull) {
        Role po = service().getOne(service().lambdaQuery().eq(Role::getRoleCode, roleCode), false);
        if (!allowNull && null == po) {
            log.error("can't find role. role code : {}", roleCode);
            throw new CheckedException(CheckErrorCode.ROLE_NOT_EXIST_ERROR);
        }
        return po;
    }

    /**
     * 检查对象编码是否存在
     *
     * @param roleCode roleCode
     * @return
     */
    @Override
    public boolean isRoleCodeExist(String roleCode) {
        return service().lambdaQuery().eq(Role::getRoleCode, roleCode).count() > 0;
    }

    /**
     * 分页查询角色
     *
     * @param dto dto
     * @return
     */
    @Override
    public Page<Role> page(RolePageQueryDto dto) {
        return service().lambdaQuery()
                .eq(!VerifyUtil.isEmpty(dto.getRoleType()), Role::getRoleType, dto.getRoleType())
                .likeRight(!VerifyUtil.isEmpty(dto.getRoleName()), Role::getRoleName, dto.getRoleName())
                .page(Page.of(dto));
    }
}
