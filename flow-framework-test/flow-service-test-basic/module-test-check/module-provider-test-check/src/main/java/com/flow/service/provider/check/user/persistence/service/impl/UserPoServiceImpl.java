package com.flow.service.provider.check.user.persistence.service.impl;

import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.persistence.persistence.page.Page;
import com.flow.framework.persistence.persistence.service.impl.PersistenceServiceImpl;
import com.flow.framework.persistence.util.PagePoUtil;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.provider.check.user.persistence.mapper.UserMapper;
import com.flow.service.provider.check.user.persistence.service.IUserPoService;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@Slf4j
@Service
public class UserPoServiceImpl extends PersistenceServiceImpl<UserMapper, User> implements IUserPoService {

    @Override
    public boolean save(User entity) {
        return service().save(entity);
    }

    @Override
    public boolean saveBatch(Collection<User> entityList) {
        return service().saveBatch(entityList);
    }

    @Override
    public boolean saveBatch(Collection<User> entityList, int batchSize) {
        return service().saveBatch(entityList, batchSize);
    }

    @Override
    public boolean saveOrUpdate(User entity) {
        return service().saveOrUpdate(entity);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<User> entityList) {
        return service().saveOrUpdateBatch(entityList);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<User> entityList, int batchSize) {
        return service().saveOrUpdateBatch(entityList, batchSize);
    }

    @Override
    public User getById(String id, boolean allowNull) {
        User po = service().getById(id);
        if (!allowNull && null == po) {
            log.error("can't find po. id : {}", id);
            throw new CheckedException(CheckErrorCode.USER_NOT_EXIST_ERROR);
        }
        return po;
    }

    @Override
    public List<User> listByIds(Collection<String> idList, boolean allowNotExist) {
        List<User> pos = service().listByIds(idList);
        if (!allowNotExist && pos.size() != idList.size()) {
            log.error("some pos can't be found. ids : {}", idList);
            throw new CheckedException(CheckErrorCode.USER_NOT_EXIST_ERROR);
        }
        return pos;
    }

    @Override
    public boolean removeById(String id) {
        return service().removeById(id);
    }

    @Override
    public boolean removeByIds(Collection<String> idList) {
        return service().removeByIds(idList);
    }

    @Override
    public boolean updateById(User entity) {
        return service().updateById(entity);
    }

    @Override
    public boolean updateBatchById(Collection<User> entityList) {
        return service().updateBatchById(entityList);
    }

    @Override
    public boolean updateBatchById(Collection<User> entityList, int batchSize) {
        return service().updateBatchById(entityList, batchSize);
    }

    /**
     * 包含角色信息的用户用户分页查询
     *
     * @param pageQueryDto pageQueryDto
     * @return
     */
    @Override
    public PageVo<UserRoleVo> page(UserRolePageQueryDto pageQueryDto) {
        return PagePoUtil.convertToPageVo(mapper().page(Page.of(pageQueryDto), pageQueryDto));
    }
}
