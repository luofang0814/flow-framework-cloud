package com.flow.service.provider.check.user.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import com.flow.service.provider.check.user.pojo.dto.UserDto;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import com.flow.service.provider.check.user.pojo.vo.UserVo;

/**
 * 用户业务服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
public interface IUserService {

    /**
     * 创建用户
     *
     * @param dto userCreateModuleDto
     * @return
     */
    UserModuleVo create(UserModuleDto dto);

    /**
     * 查询用户
     *
     * @param userId    userId
     * @param allowNull allowNull
     * @return
     */
    UserModuleVo getById(String userId, boolean allowNull);

    /**
     * 包含角色信息的用户用户分页查询
     *
     * @param userRolePageQueryDto userRolePageQueryDto
     * @return
     */
    PageVo<UserRoleVo> page(UserRolePageQueryDto userRolePageQueryDto);

    /**
     * 更新用户信息
     *
     * @param dto dto
     * @return
     */
    UserVo update(UserDto dto);
}