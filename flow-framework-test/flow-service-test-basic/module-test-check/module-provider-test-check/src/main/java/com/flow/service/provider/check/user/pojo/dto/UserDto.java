package com.flow.service.provider.check.user.pojo.dto;

import com.flow.common.facade.check.user.immutable.GenderImmutable;
import com.flow.common.facade.check.user.immutable.UserStatusImmutable;
import com.flow.framework.core.pojo.dto.base.BaseDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@ApiModel(description = "更新用户")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String id;

    /**
     * 角色编号
     */
    @ApiModelProperty("角色编号")
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String roleCode;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 手机
     */
    @ApiModelProperty("手机")
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String mobilePhone;

    /**
     * 花名
     */
    @ApiModelProperty("花名")
    private String nickName;

    /**
     * 是否超级管理员
     */
    @ApiModelProperty("是否超级管理员")
    private Boolean isAdmin;

    /**
     * 性别：1-男；2-女
     */
    @ApiModelProperty("性别：1-男；2-女")
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = CheckErrorCode.GENDER_ERROR, immutableInterface = GenderImmutable.class)
    private String gender;

    /**
     * 状态:01-正常；02-冻结
     */
    @ApiModelProperty("状态:01-正常；02-冻结")
    @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_STATUS_ERROR, immutableInterface = UserStatusImmutable.class)
    private String status;
}
