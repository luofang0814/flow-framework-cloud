package com.flow.service.provider.check.user.service.impl;

import com.flow.framework.cache.constant.FrameworkCacheConstant;
import com.flow.framework.cache.manager.PojoCacheManager;
import com.flow.framework.common.type.TypeReference;
import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.facade.check.CheckModuleConstant;
import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import com.flow.service.provider.check.user.conver.UserConverter;
import com.flow.service.provider.check.user.persistence.service.IUserPoService;
import com.flow.service.provider.check.user.pojo.dto.UserDto;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import com.flow.service.provider.check.user.pojo.vo.UserVo;
import com.flow.service.provider.check.user.service.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 用户业务服务实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService {

    private static final PojoCacheManager POJO_CACHE_MANAGER = PojoCacheManager.getByPlaceholder(CheckModuleConstant.APPLICATION_NAME_EXPRESSION);

    private static final int CACHE_TIME = 60 * 60 * 1000;

    private final IUserPoService userPoService;

    /**
     * 创建用户
     *
     * @param dto userCreateModuleDto
     * @return
     */
    @Override
    public UserModuleVo create(UserModuleDto dto) {
        User user = UserConverter.convertToPo(dto);
        userPoService.save(user);
        User userInDb = userPoService.getById(user.getId(), false);
        return UserConverter.convertToModuleVo(userInDb);
    }

    /**
     * 查询用户
     *
     * @param userId    userId
     * @param allowNull allowNull
     * @return
     */
    @Override
    public UserModuleVo getById(String userId, boolean allowNull) {
        return POJO_CACHE_MANAGER.bigGet(0, UserModuleVo.class.getName()
                        + FrameworkCacheConstant.CACHE_VALUE_ID_KEY + userId,
                new TypeReference<UserModuleVo>() {
                }, CACHE_TIME, TimeUnit.MILLISECONDS,
                () -> {
                    User userInDb = userPoService.getById(userId, allowNull);
                    return UserConverter.convertToModuleVo(userInDb);
                });
    }

    /**
     * 包含角色信息的用户用户分页查询
     *
     * @param pageQueryDto pageQueryDto
     * @return
     */
    @Override
    public PageVo<UserRoleVo> page(UserRolePageQueryDto pageQueryDto) {
        return userPoService.page(pageQueryDto);
    }

    /**
     * 更新用户信息
     *
     * @param dto dto
     * @return
     */
    @Override
    public UserVo update(UserDto dto) {
        User user = UserConverter.convertToPo(dto);
        userPoService.updateById(user);
        String id = user.getId();
        POJO_CACHE_MANAGER.bigDelete(0, UserModuleVo.class.getName()
                + FrameworkCacheConstant.CACHE_VALUE_ID_KEY + id);

        // 更新时不用更新缓存，等实际使用时再更新
        User userInDb = userPoService.getById(id, false);
        return UserConverter.convertToVo(userInDb);
    }
}