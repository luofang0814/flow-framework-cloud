package com.flow.service.provider.check.user.persistence.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface IUserPoService {

    /**
     * 保存
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean save(User entity);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveBatch(Collection<User> entityList);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveBatch(Collection<User> entityList, int batchSize);

    /**
     * 保存或更新，根据id是否存在判断
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean saveOrUpdate(User entity);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<User> entityList);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<User> entityList, int batchSize);

    /**
     * 根据id获取实体
     *
     * @param id        id
     * @param allowNull allowNull
     * @return 实体
     */
    User getById(String id, boolean allowNull);

    /**
     * 批量根据id获取实体
     *
     * @param idList        idList
     * @param allowNotExist allowNotExist
     * @return 实体集合
     */
    List<User> listByIds(Collection<String> idList, boolean allowNotExist);

    /**
     * 根据id删除实体
     *
     * @param id id
     * @return 是否成功
     */
    boolean removeById(String id);

    /**
     * 批量根据id删除实体
     *
     * @param idList idList
     * @return 是否成功
     */
    boolean removeByIds(Collection<String> idList);

    /**
     * 根据id更新实体
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean updateById(User entity);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean updateBatchById(Collection<User> entityList);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean updateBatchById(Collection<User> entityList, int batchSize);

    /**
     * 包含角色信息的用户用户分页查询
     *
     * @param pageQueryDto pageQueryDto
     * @return
     */
    PageVo<UserRoleVo> page(UserRolePageQueryDto pageQueryDto);
}
