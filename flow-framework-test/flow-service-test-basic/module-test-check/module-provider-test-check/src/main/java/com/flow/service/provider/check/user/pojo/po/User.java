package com.flow.service.provider.check.user.pojo.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.flow.common.facade.check.user.immutable.GenderImmutable;
import com.flow.common.facade.check.user.immutable.UserStatusImmutable;
import com.flow.common.facade.check.user.immutable.UserTypeImmutable;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.persistence.handler.CustomizationSecurityHandler;
import com.flow.framework.persistence.pojo.po.base.BaseBizPo;
import com.flow.service.facade.check.CheckErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "check_user", autoResultMap = true)
public class User extends BaseBizPo {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编号
     */
    private String roleCode;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 登录账号
     */
    private String loginAccount;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    @TableField(typeHandler = CustomizationSecurityHandler.class)
    private String mobilePhone;

    /**
     * 花名
     */
    private String nickName;

    /**
     * 用户类型:01-个人；02-公司
     */
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_TYPE_ERROR, immutableInterface = UserTypeImmutable.class)
    private String userType;

    /**
     * 是否超级管理员
     */
    private Boolean isAdmin;

    /**
     * 性别：1-男；2-女
     */
    @ImmutableCustomization(errorCode = CheckErrorCode.GENDER_ERROR, immutableInterface = GenderImmutable.class)
    private String gender;

    /**
     * 状态:01-正常；02-冻结
     */
    @ImmutableCustomization(errorCode = CheckErrorCode.USER_STATUS_ERROR, immutableInterface = UserStatusImmutable.class)
    private String status;


}
