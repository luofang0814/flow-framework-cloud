package com.flow.service.provider.check.role.persistence.mapper;

import com.flow.service.provider.check.role.pojo.po.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface RoleMapper extends BaseMapper<Role> {

}
