package com.flow.service.provider.check.role.persistence.service;

import com.flow.framework.persistence.persistence.page.Page;
import com.flow.service.provider.check.role.pojo.dto.RolePageQueryDto;
import com.flow.service.provider.check.role.pojo.po.Role;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/9
 */
public interface IRolePoService {

    /**
     * 保存
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean save(Role entity);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveBatch(Collection<Role> entityList);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveBatch(Collection<Role> entityList, int batchSize);

    /**
     * 保存或更新，根据id是否存在判断
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean saveOrUpdate(Role entity);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Role> entityList);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Role> entityList, int batchSize);

    /**
     * 根据id获取实体
     *
     * @param id        id
     * @param allowNull allowNull
     * @return 实体
     */
    Role getById(String id, boolean allowNull);

    /**
     * 批量根据id获取实体
     *
     * @param idList        idList
     * @param allowNotExist allowNotExist
     * @return 实体集合
     */
    List<Role> listByIds(Collection<String> idList, boolean allowNotExist);

    /**
     * 根据id删除实体
     *
     * @param id id
     * @return 是否成功
     */
    boolean removeById(String id);

    /**
     * 批量根据id删除实体
     *
     * @param idList idList
     * @return 是否成功
     */
    boolean removeByIds(Collection<String> idList);

    /**
     * 根据id更新实体
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean updateById(Role entity);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Role> entityList);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Role> entityList, int batchSize);

    /**
     * 根据角色编码获取角色信息
     *
     * @param roleCode  roleCode
     * @param allowNull allowNull
     * @return
     */
    Role getRoleByCode(String roleCode, boolean allowNull);

    /**
     * 检查对象编码是否存在
     *
     * @param roleCode roleCode
     * @return
     */
    boolean isRoleCodeExist(String roleCode);

    /**
     * 分页查询角色
     *
     * @param dto dto
     * @return
     */
    Page<Role> page(RolePageQueryDto dto);
}
