package com.flow.service.provider.check.user.conver;

import com.flow.framework.common.struct.control.CommonMappingControl;
import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import com.flow.service.provider.check.user.pojo.dto.UserDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import com.flow.service.provider.check.user.pojo.vo.UserVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@Mapper(mappingControl = CommonMappingControl.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface UserConvert {

    UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    User convertToPo(UserModuleDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    User convertToPo(UserDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    UserModuleVo convertToModuleVo(User o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    UserVo convertToVo(User o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    UserRoleVo convertToUserRoleVo(User o);
}