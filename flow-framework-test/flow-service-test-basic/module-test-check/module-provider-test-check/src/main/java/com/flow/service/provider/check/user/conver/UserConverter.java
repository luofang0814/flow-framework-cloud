package com.flow.service.provider.check.user.conver;

import com.flow.service.facade.check.user.pojo.dto.UserModuleDto;
import com.flow.service.facade.check.user.pojo.vo.UserModuleVo;
import com.flow.service.provider.check.user.pojo.dto.UserDto;
import com.flow.service.provider.check.user.pojo.po.User;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import com.flow.service.provider.check.user.pojo.vo.UserVo;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
public class UserConverter {

    private static UserConvert convert = UserConvert.INSTANCE;

    public static User convertToPo(UserModuleDto o) {
        return convert.convertToPo(o);
    }

    public static User convertToPo(UserDto o) {
        return convert.convertToPo(o);
    }

    public static UserModuleVo convertToModuleVo(User o) {
        return convert.convertToModuleVo(o);
    }

    public static UserRoleVo convertToUserRoleVo(User o) {
        return convert.convertToUserRoleVo(o);
    }

    public static UserVo convertToVo(User o) {
        return convert.convertToVo(o);
    }
}