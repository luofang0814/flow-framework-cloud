package com.flow.service.provider.check.enumeration;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 角色分布式锁
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
@AllArgsConstructor
@Getter
public enum RoleLockKeyEnum implements ILockKeyEnum {

    /**
     * 角色编码检查
     */
    ROLE_CODE_CHECK_LOCK_KEY(1, "角色编码检查"),
    ;

    private int paramsSize;

    private String remark;
}
