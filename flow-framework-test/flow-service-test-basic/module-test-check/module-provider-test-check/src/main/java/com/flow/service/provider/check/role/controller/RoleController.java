package com.flow.service.provider.check.role.controller;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.provider.check.role.pojo.dto.RoleCreateDto;
import com.flow.service.provider.check.role.pojo.dto.RolePageQueryDto;
import com.flow.service.provider.check.role.pojo.vo.RoleVo;
import com.flow.service.provider.check.role.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 角色管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
@Api(tags = "角色管理")
@Slf4j
@RestController
@RequestMapping("/role")
@Validated
@RequiredArgsConstructor
public class RoleController {

    private final IRoleService roleService;

    @ApiOperation(value = "创建角色")
    @PostMapping("/create")
    public RoleVo create(@RequestBody @Valid RoleCreateDto dto){
        return roleService.create(dto);
    }

    @ApiOperation(value = "分页查询角色")
    @GetMapping("/page")
    public PageVo<RoleVo> page(RolePageQueryDto rolePageQueryDto){
        return roleService.page(rolePageQueryDto);
    }
}