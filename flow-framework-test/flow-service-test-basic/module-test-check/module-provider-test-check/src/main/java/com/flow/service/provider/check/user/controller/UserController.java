package com.flow.service.provider.check.user.controller;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import com.flow.service.provider.check.user.pojo.dto.UserDto;
import com.flow.service.provider.check.user.pojo.dto.UserRolePageQueryDto;
import com.flow.service.provider.check.user.pojo.vo.UserRoleVo;
import com.flow.service.provider.check.user.pojo.vo.UserVo;
import com.flow.service.provider.check.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/29
 */
@Api(tags = "用户管理")
@Slf4j
@RestController
@RequestMapping("/user")
@Validated
@RequiredArgsConstructor
public class UserController {

    private final IUserService userService;

    @ApiOperation(value = "分页查询用户角色详情")
    @GetMapping("/page")
    public PageVo<UserRoleVo> page(@Valid @NotEmptyCustomization(errorCode = CheckErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                           UserRolePageQueryDto pageQueryDto) {
        return userService.page(pageQueryDto);
    }

    @ApiOperation(value = "更新用户信息")
    @PostMapping("/update")
    public UserVo update(@RequestBody @Valid UserDto dto){
        return userService.update(dto);
    }
}