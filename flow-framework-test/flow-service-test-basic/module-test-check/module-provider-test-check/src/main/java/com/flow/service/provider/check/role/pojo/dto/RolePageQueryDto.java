package com.flow.service.provider.check.role.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.flow.common.facade.check.role.immutable.RoleTypeImmutable;
import com.flow.framework.core.pojo.dto.base.BasePageQueryDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.check.CheckErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/16
 */
@ApiModel(description = "<p> 角色 </p>")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("check_role")
public class RolePageQueryDto extends BasePageQueryDto {

    private static final long serialVersionUID = 1L;

    /**
     * 角色类型:01-外部；02-内部
     */
    @ApiModelProperty("角色类型:01-外部；02-内部")
    @ImmutableCustomization(errorCode = CheckErrorCode.ROLE_TYPE_ERROR, immutableInterface = RoleTypeImmutable.class)
    private String roleType;

    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;
}
