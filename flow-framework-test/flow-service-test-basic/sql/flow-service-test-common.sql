CREATE TABLE `check_role` (
  `id` varchar(20) NOT NULL COMMENT 'id',
  `role_type` char(2) NOT NULL DEFAULT '01' COMMENT '角色类型:01-外部；02-内部',
  `role_name` varchar(55) NOT NULL COMMENT '角色名称',
  `role_code` varchar(32) NOT NULL COMMENT '角色编码',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  `extra_info` varchar(300) NOT NULL DEFAULT '{}' COMMENT '扩展字段，json结构',
  `system_version` bigint(20) NOT NULL DEFAULT '0' COMMENT '处理数据的系统版本号',
  `tenant_id` varchar(20) NOT NULL DEFAULT '' COMMENT '租户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色';

CREATE TABLE `check_user` (
  `id` varchar(20) NOT NULL COMMENT 'id',
  `role_code` varchar(20) NOT NULL COMMENT '角色编号',
  `user_name` varchar(45) NOT NULL COMMENT '用户姓名',
  `login_account` varchar(32) NOT NULL COMMENT '登录账号',
  `email` varchar(320) NOT NULL DEFAULT '' COMMENT '邮箱',
  `mobile_phone` varchar(15) NOT NULL DEFAULT '' COMMENT '手机',
  `nick_name` varchar(45) NOT NULL DEFAULT '' COMMENT '花名',
  `user_type` char(2) NOT NULL DEFAULT '01' COMMENT '用户类型:01-个人；02-公司',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `gender` char(1) NOT NULL DEFAULT '1' COMMENT '性别：1-男；2-女',
  `status` char(2) NOT NULL DEFAULT '01' COMMENT '状态:01-正常；02-冻结',
  `system_version` bigint(20) NOT NULL DEFAULT '0' COMMENT '处理数据的系统版本号',
  `tenant_id` varchar(20) NOT NULL DEFAULT '' COMMENT '租户id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_role_code` (`role_code`) USING BTREE,
  KEY `index_login_account` (`login_account`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户';

CREATE TABLE `experiment_apply` (
  `id` varchar(20) NOT NULL COMMENT 'id',
  `apply_no` varchar(32) NOT NULL COMMENT '申请no',
  `apply_type` char(2) NOT NULL COMMENT '申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请',
  `apply_status` char(2) NOT NULL DEFAULT '01' COMMENT '申请状态：01-待审批；02-通过；03-拒绝',
  `resource_id` varchar(20) NOT NULL COMMENT '申请资源的id',
  `user_id` varchar(20) NOT NULL COMMENT '用户ID',
  `user_name` varchar(64) NOT NULL COMMENT '用户姓名',
  `is_notified` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已发送消息队列通知业务侧',
  `system_version` bigint(20) NOT NULL DEFAULT '0' COMMENT '处理数据的系统版本号',
  `tenant_id` varchar(20) NOT NULL DEFAULT '' COMMENT '租户id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_user_id` (`user_id`) USING BTREE,
  KEY `index_apply_no` (`apply_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='申请';