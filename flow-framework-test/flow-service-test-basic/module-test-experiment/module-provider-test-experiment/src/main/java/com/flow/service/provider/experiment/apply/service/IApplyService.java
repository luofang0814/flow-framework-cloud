package com.flow.service.provider.experiment.apply.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.core.system.thread.pool.policy.RejectedPolicy;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyApprovalDto;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyPageQueryDto;
import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;

/**
 * 申请业务服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
public interface IApplyService {

    /**
     * 创建申请
     *
     * @param dto dto
     * @return
     */
    ApplyModuleVo create(ApplyModuleDto dto);

    /**
     * 根据申请编号获取申请
     *
     * @param applyNo    applyNo
     * @param allowNull allowNull
     * @return
     */
    ApplyModuleVo getByApplyNo(String applyNo, boolean allowNull);

    /**
     * 申请分页查询
     *
     * @param dto dto
     * @return
     */
    PageVo<ApplyVo> page(ApplyPageQueryDto dto);

    /**
     * 申请审批
     *
     * @param dto dto
     * @return
     */
    ApplyVo applyApproval(ApplyApprovalDto dto);

    /**
     * 申请审批消息通知
     *
     * @param apply          apply
     * @param rejectedPolicy rejectedPolicy
     */
    void applyApprovalNotify(Apply apply, RejectedPolicy rejectedPolicy);
}