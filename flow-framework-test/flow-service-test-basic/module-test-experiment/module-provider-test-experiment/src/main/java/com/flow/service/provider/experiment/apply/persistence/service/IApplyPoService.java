package com.flow.service.provider.experiment.apply.persistence.service;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyPageQueryDto;
import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 申请 服务类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface IApplyPoService {

    /**
     * 保存
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean save(Apply entity);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveBatch(Collection<Apply> entityList);

    /**
     * 批量保存
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveBatch(Collection<Apply> entityList, int batchSize);

    /**
     * 保存或更新，根据id是否存在判断
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean saveOrUpdate(Apply entity);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Apply> entityList);

    /**
     * 批量保存或更新，根据id是否存在判断
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean saveOrUpdateBatch(Collection<Apply> entityList, int batchSize);

    /**
     * 根据id获取实体
     *
     * @param id        id
     * @param allowNull allowNull
     * @return 实体
     */
    Apply getById(String id, boolean allowNull);

    /**
     * 批量根据id获取实体
     *
     * @param idList        idList
     * @param allowNotExist allowNotExist
     * @return 实体集合
     */
    List<Apply> listByIds(Collection<String> idList, boolean allowNotExist);

    /**
     * 根据id删除实体
     *
     * @param id id
     * @return 是否成功
     */
    boolean removeById(String id);

    /**
     * 批量根据id删除实体
     *
     * @param idList idList
     * @return 是否成功
     */
    boolean removeByIds(Collection<String> idList);

    /**
     * 根据id更新实体
     *
     * @param entity entity
     * @return 是否成功
     */
    boolean updateById(Apply entity);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Apply> entityList);

    /**
     * 批量根据id更新实体
     *
     * @param entityList entityList
     * @param batchSize  batchSize
     * @return 是否成功
     */
    boolean updateBatchById(Collection<Apply> entityList, int batchSize);

    /**
     * 根据申请编号查询申请
     *
     * @param applyNo   applyNo
     * @param allowNull 返回值是否允许为空
     * @return
     */
    Apply getByApplyNo(String applyNo, boolean allowNull);

    /**
     * 申请分页查询
     *
     * @param applyPageQueryDto applyPageQueryDto
     * @return
     */
    PageVo<ApplyVo> page(ApplyPageQueryDto applyPageQueryDto);

    /**
     * 检查申请编号是否存在
     *
     * @param applyNo applyNo
     * @return
     */
    boolean isApplyNoExist(String applyNo);
}
