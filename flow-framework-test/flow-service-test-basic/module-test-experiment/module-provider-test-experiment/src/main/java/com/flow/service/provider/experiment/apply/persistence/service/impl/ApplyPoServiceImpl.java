package com.flow.service.provider.experiment.apply.persistence.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.flow.framework.common.exception.CheckedException;
import com.flow.framework.common.util.verify.VerifyUtil;
import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.persistence.persistence.service.impl.PersistenceServiceImpl;
import com.flow.framework.persistence.util.PagePoUtil;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import com.flow.service.provider.experiment.apply.convert.ApplyConverter;
import com.flow.service.provider.experiment.apply.persistence.mapper.ApplyMapper;
import com.flow.service.provider.experiment.apply.persistence.service.IApplyPoService;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyPageQueryDto;
import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 申请 服务实现类
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
@Slf4j
@Service
public class ApplyPoServiceImpl extends PersistenceServiceImpl<ApplyMapper, Apply> implements IApplyPoService {

    @Override
    public boolean save(Apply entity) {
        return service().save(entity);
    }

    @Override
    public boolean saveBatch(Collection<Apply> entityList) {
        return service().saveBatch(entityList);
    }

    @Override
    public boolean saveBatch(Collection<Apply> entityList, int batchSize) {
        return service().saveBatch(entityList, batchSize);
    }

    @Override
    public boolean saveOrUpdate(Apply entity) {
        return service().saveOrUpdate(entity);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Apply> entityList) {
        return service().saveOrUpdateBatch(entityList);
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<Apply> entityList, int batchSize) {
        return service().saveOrUpdateBatch(entityList, batchSize);
    }

    @Override
    public Apply getById(String id, boolean allowNull) {
        Apply po = service().getById(id);
        if (!allowNull && null == po) {
            log.error("can't find po. id : {}", id);
            throw new CheckedException(ExperimentErrorCode.APPLY_NOT_EXIST_ERROR);
        }
        return po;
    }

    @Override
    public List<Apply> listByIds(Collection<String> idList, boolean allowNotExist) {
        List<Apply> pos = service().listByIds(idList);
        if (!allowNotExist && pos.size() != idList.size()) {
            log.error("some pos can't be found. ids : {}", idList);
            throw new CheckedException(ExperimentErrorCode.APPLY_NOT_EXIST_ERROR);
        }
        return pos;
    }

    @Override
    public boolean removeById(String id) {
        return service().removeById(id);
    }

    @Override
    public boolean removeByIds(Collection<String> idList) {
        return service().removeByIds(idList);
    }

    @Override
    public boolean updateById(Apply entity) {
        return service().updateById(entity);
    }

    @Override
    public boolean updateBatchById(Collection<Apply> entityList) {
        return service().updateBatchById(entityList);
    }

    @Override
    public boolean updateBatchById(Collection<Apply> entityList, int batchSize) {
        return service().updateBatchById(entityList, batchSize);
    }

    /**
     * 根据申请编号查询申请
     *
     * @param applyNo   applyNo
     * @param allowNull 返回值是否允许为空
     * @return
     */
    @Override
    public Apply getByApplyNo(String applyNo, boolean allowNull) {
        LambdaQueryWrapper<Apply> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Apply::getApplyNo, applyNo);
        Apply apply = service().getOne(lambdaQueryWrapper, false);
        if (!allowNull && null == apply) {
            log.error("can't find apply. apply no : {}", applyNo);
            throw new CheckedException(ExperimentErrorCode.APPLY_NOT_EXIST_ERROR);
        }
        return apply;
    }

    /**
     * 申请分页查询
     *
     * @param applyPageQueryDto applyPageQueryDto
     * @return
     */
    @Override
    public PageVo<ApplyVo> page(ApplyPageQueryDto applyPageQueryDto) {
        Page<Apply> page = service().lambdaQuery()
                .eq(!VerifyUtil.isEmpty(applyPageQueryDto.getApplyStatus()), Apply::getApplyStatus, applyPageQueryDto.getApplyStatus())
                .eq(!VerifyUtil.isEmpty(applyPageQueryDto.getUserId()), Apply::getUserId, applyPageQueryDto.getUserId())
                .like(!VerifyUtil.isEmpty(applyPageQueryDto.getUserName()), Apply::getUserName, applyPageQueryDto.getUserName())
                .page(new Page<>(applyPageQueryDto.getCurrent(), applyPageQueryDto.getSize()));
        return PagePoUtil.convertToPageVo(page, ApplyConverter::convertToVo);
    }

    /**
     * 检查申请编号是否存在
     *
     * @param applyNo applyNo
     * @return
     */
    @Override
    public boolean isApplyNoExist(String applyNo) {
        return service().lambdaQuery().eq(Apply::getApplyNo, applyNo).count() > 0;
    }
}
