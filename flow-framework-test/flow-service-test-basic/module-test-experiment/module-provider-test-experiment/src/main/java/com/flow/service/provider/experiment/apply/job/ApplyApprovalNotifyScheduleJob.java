package com.flow.service.provider.experiment.apply.job;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.flow.common.facade.experiment.apply.immutable.ApplyStatusImmutable;
import com.flow.framework.core.system.thread.pool.policy.RejectedPolicy;
import com.flow.framework.schedule.job.BaseBizScheduleJob;
import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.service.IApplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 申请通过通知调度任务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ApplyApprovalNotifyScheduleJob extends BaseBizScheduleJob<Apply> {

    private final IApplyService applyService;

    /**
     * 构造查询条件
     *
     * @param params      调度参数
     * @param lambdaQuery lambda查询
     */
    @Override
    public void buildLambdaQuery(String params, LambdaQueryWrapper<Apply> lambdaQuery) {
        lambdaQuery.in(Apply::getApplyStatus, Arrays.asList(ApplyStatusImmutable.PASS, ApplyStatusImmutable.REFUSE))
                .eq(Apply::getIsNotified, false);
    }

    /**
     * 分页处理
     *
     * @param record      record
     * @param currentPage 当前页数
     * @param pageSize    每页数据量
     * @param totalCount  数据总条数
     * @param params      调度参数
     */
    @Override
    public void process(List<Apply> record, long currentPage, long pageSize, long totalCount, String params) {
        record.forEach(apply -> applyService.applyApprovalNotify(apply, RejectedPolicy.CALLER_RUN_POLICY));
    }
}