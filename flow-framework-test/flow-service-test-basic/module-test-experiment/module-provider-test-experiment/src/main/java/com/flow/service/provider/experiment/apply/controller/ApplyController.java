package com.flow.service.provider.experiment.apply.controller;

import com.flow.framework.core.pojo.vo.PageVo;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyApprovalDto;
import com.flow.service.provider.experiment.apply.pojo.dto.ApplyPageQueryDto;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;
import com.flow.service.provider.experiment.apply.service.IApplyService;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 申请管理
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Api(tags = "申请管理")
@Slf4j
@RestController
@RequestMapping("/apply")
@Validated
@RequiredArgsConstructor
public class ApplyController {

    private final IApplyService applyService;

    @ApiOperation(value = "申请信息分页查询")
    @GetMapping("/page")
    public PageVo<ApplyVo> page(@Valid @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                        ApplyPageQueryDto dto) {
        return applyService.page(dto);
    }

    @ApiOperation(value = "申请审批")
    @PutMapping("/applyApproval")
    public ApplyVo applyApproval(@RequestBody @Valid ApplyApprovalDto dto) {
        return applyService.applyApproval(dto);
    }
}