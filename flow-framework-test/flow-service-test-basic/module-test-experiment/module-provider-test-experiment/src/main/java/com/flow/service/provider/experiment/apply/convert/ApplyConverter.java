package com.flow.service.provider.experiment.apply.convert;

import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
public class ApplyConverter {

    private static ApplyConvert convert = ApplyConvert.INSTANCE;

    public static Apply convertToPo(ApplyModuleDto o) {
        return convert.convertToPo(o);
    }

    public static ApplyModuleVo convertToModuleVo(Apply o) {
        return convert.convertToModuleVo(o);
    }

    public static ApplyVo convertToVo(Apply o) {
        return convert.convertToVo(o);
    }
}