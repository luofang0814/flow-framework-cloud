package com.flow.service.provider.experiment.constant;

/**
 * experiment模块告警常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ExperimentAlarmConstant {

    String APPLY_RESULT_NOTIFY_FAILED = "apply_result_notify_failed";
}