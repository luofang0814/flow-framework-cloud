package com.flow.service.provider.experiment.apply.pojo.dto;

import com.flow.common.facade.experiment.apply.immutable.ApplyStatusImmutable;
import com.flow.framework.core.pojo.dto.base.BasePageQueryDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 申请分页查询dto
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Data
@ApiModel(description = "申请分页查询")
@EqualsAndHashCode(callSuper = true)
public class ApplyPageQueryDto extends BasePageQueryDto {

    /**
     * 申请状态：01-待审批；02-通过；03-拒绝
     */
    @ApiModelProperty("申请状态：01-待审批；02-通过；03-拒绝")
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_STATUS_ERROR, immutableInterface = ApplyStatusImmutable.class)
    private String applyStatus;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private String userId;

    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String userName;
}