package com.flow.service.provider.experiment.apply.module.service.impl;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import com.flow.service.facade.experiment.apply.module.service.IApplyModuleService;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;
import com.flow.service.provider.experiment.apply.service.IApplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 申请模块服务调用实现
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Slf4j
@RestController
@RequestMapping("/rpc/applyModuleService")
@Validated
@RequiredArgsConstructor
public class ApplyModuleServiceImpl implements IApplyModuleService {

    private final IApplyService applyService;

    /**
     * 创建申请
     *
     * @param applyModuleDto applyModuleDto
     * @return
     */
    @Override
    public ApplyModuleVo create(@Valid @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                        ApplyModuleDto applyModuleDto) {
        return applyService.create(applyModuleDto);
    }

    /**
     * 根据申请编号查询申请
     *
     * @param applyNo   applyNo
     * @param allowNull 返回值是否允许为空
     * @return
     */
    @Override
    public ApplyModuleVo getByApplyNo(@Valid @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                              String applyNo, boolean allowNull) {
        return applyService.getByApplyNo(applyNo, allowNull);
    }
}