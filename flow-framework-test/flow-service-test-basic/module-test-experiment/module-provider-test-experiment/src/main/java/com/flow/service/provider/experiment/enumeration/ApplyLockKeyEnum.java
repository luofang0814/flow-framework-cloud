package com.flow.service.provider.experiment.enumeration;

import com.flow.framework.lock.enumeration.ILockKeyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 申请分布式锁
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2022/4/9
 */
@AllArgsConstructor
@Getter
public enum ApplyLockKeyEnum implements ILockKeyEnum {

    /**
     * 申请编号检查
     */
    APPLY_NO_CHECK_LOCK_KEY(1, "申请编号检查"),

    /**
     * 申请审批检查
     */
    APPLY_APPROVAL_CHECK_LOCK_KEY(1, "申请审批检查"),

    /**
     * 申请通过消息通知
     */
    APPLY_APPROVAL_NOTIFY_LOCK_KEY(1, "申请通过消息通知"),
    ;

    private int paramsSize;

    private String remark;
}
