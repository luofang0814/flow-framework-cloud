package com.flow.service.provider.experiment.apply.convert;

import com.flow.framework.common.struct.control.CommonMappingControl;
import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.flow.service.provider.experiment.apply.pojo.vo.ApplyVo;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 转换
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Mapper(mappingControl = CommonMappingControl.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface ApplyConvert {

    ApplyConvert INSTANCE = Mappers.getMapper(ApplyConvert.class);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    Apply convertToPo(ApplyModuleDto o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ApplyModuleVo convertToModuleVo(Apply o);

    /**
     * 转换
     *
     * @param o o
     * @return
     */
    ApplyVo convertToVo(Apply o);
}