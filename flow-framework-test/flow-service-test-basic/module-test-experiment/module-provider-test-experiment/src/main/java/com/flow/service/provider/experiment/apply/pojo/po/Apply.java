package com.flow.service.provider.experiment.apply.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.flow.common.facade.experiment.apply.immutable.ApplyStatusImmutable;
import com.flow.common.facade.experiment.apply.immutable.ApplyTypeImmutable;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.framework.persistence.pojo.po.base.BaseBizPo;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 申请
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "experiment_apply", autoResultMap = true)
public class Apply extends BaseBizPo {

    private static final long serialVersionUID = 1L;

    /**
     * 申请no
     */
    private String applyNo;

    /**
     * 申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请
     */
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_TYPE_ERROR, immutableInterface = ApplyTypeImmutable.class)
    private String applyType;

    /**
     * 申请状态：01-待审批；02-通过；03-拒绝
     */
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_STATUS_ERROR, immutableInterface = ApplyStatusImmutable.class)
    private String applyStatus;

    /**
     * 申请资源的id
     */
    private String resourceId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 是否已发送消息队列通知业务侧
     */
    private Boolean isNotified;
}
