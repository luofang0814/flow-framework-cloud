package com.flow.service.provider.experiment.apply.pojo.dto;

import com.flow.framework.core.pojo.dto.base.BaseDto;
import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.framework.core.validation.annotation.NotNullCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 申请审批
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@ApiModel(description = "<p> 申请审批 </p>")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ApplyApprovalDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    /**
     * 申请no
     */
    @ApiModelProperty("申请id")
    @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private String id;

    /**
     * 是否同意
     */
    @ApiModelProperty("是否同意")
    @NotNullCustomization(errorCode = ExperimentErrorCode.INPUT_PROPERTY_EMPTY_ERROR)
    private Boolean isPass;

}
