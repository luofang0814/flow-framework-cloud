package com.flow.service.provider.experiment.apply.persistence.mapper;

import com.flow.service.provider.experiment.apply.pojo.po.Apply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 申请 Mapper 接口
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ApplyMapper extends BaseMapper<Apply> {

}
