package com.flow.service.facade.experiment;

/**
 * experiment模块常量
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ExperimentModuleConstant {

    String MODULE_NAME = "module-test-experiment";

    /**
     * URL调用前缀表达式
     */
    String URL_PREFIX_EXPRESSION = "${customization.service.facade.experiment.url:}";

    /**
     * application name表达式
     */
    String APPLICATION_NAME_EXPRESSION = "${customization.service.facade.experiment.application-name:flow-service-test-common}";

    /**
     * RPC调用前缀表达式
     */
    String RPC_PREFIX_EXPRESSION = "${customization.service.facade.experiment.rpc-prefix:/testCommon}";
}