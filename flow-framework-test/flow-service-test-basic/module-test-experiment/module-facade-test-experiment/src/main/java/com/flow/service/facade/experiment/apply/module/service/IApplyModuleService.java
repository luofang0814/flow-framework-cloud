package com.flow.service.facade.experiment.apply.module.service;

import com.flow.framework.core.validation.annotation.NotEmptyCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import com.flow.service.facade.experiment.ExperimentModuleConstant;
import com.flow.service.facade.experiment.apply.pojo.dto.ApplyModuleDto;
import com.flow.service.facade.experiment.apply.pojo.vo.ApplyModuleVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 申请模块调用服务
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@FeignClient(contextId = "service-facade-test-experiment-applyModuleService",
        url = ExperimentModuleConstant.URL_PREFIX_EXPRESSION,
        name = ExperimentModuleConstant.APPLICATION_NAME_EXPRESSION,
        path = ExperimentModuleConstant.RPC_PREFIX_EXPRESSION + "/rpc/applyModuleService")
public interface IApplyModuleService {

    /**
     * 创建申请
     *
     * @param applyModuleDto applyModuleDto
     * @return
     */
    @PostMapping("/create")
    ApplyModuleVo create(@RequestBody @Valid
                         @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                                 ApplyModuleDto applyModuleDto);

    /**
     * 根据申请编号查询申请
     *
     * @param applyNo   applyNo
     * @param allowNull 返回值是否允许为空
     * @return
     */
    @GetMapping("/getByApplyNo")
    ApplyModuleVo getByApplyNo(@Valid @NotEmptyCustomization(errorCode = ExperimentErrorCode.INPUT_OBJECT_EMPTY_ERROR)
                               @RequestParam("applyNo") String applyNo,
                               @RequestHeader(name = "allowNull", defaultValue = "true") boolean allowNull);
}