package com.flow.service.facade.experiment.apply.pojo.dto.notify;

import com.flow.common.facade.experiment.apply.immutable.ApplyTypeImmutable;
import com.flow.framework.core.pojo.dto.base.notify.BaseNotifyDto;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 申请审批通过通知
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ApplyApprovalNotifyDto extends BaseNotifyDto {

    /**
     * 申请编号
     */
    private String applyNo;

    /**
     * 申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请
     */
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_TYPE_ERROR, immutableInterface = ApplyTypeImmutable.class)
    private String applyType;

    /**
     * 审批是否通过
     */
    private boolean isPass;

    /**
     * 申请资源id
     */
    private String resourceId;
}