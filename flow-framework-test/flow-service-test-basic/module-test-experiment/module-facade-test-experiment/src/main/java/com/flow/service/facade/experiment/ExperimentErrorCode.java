package com.flow.service.facade.experiment;

/**
 * experiment模块错误码
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/15
 */
public interface ExperimentErrorCode {


    /**
     * 参数不能为空
     */
    long INPUT_OBJECT_EMPTY_ERROR = 40001;

    /**
     * 参数属性为空错误
     */
    long INPUT_PROPERTY_EMPTY_ERROR = 40002;

    /**
     * 申请类型错误
     */
    long APPLY_TYPE_ERROR = 40003;

    /**
     * 申请编号已存在
     */
    long APPLY_NO_EXIST_ERROR = 40004;

    /**
     * 用户不存在
     */
    long USER_NOT_EXIST_ERROR = 40005;

    /**
     * 申请状态错误
     */
    long APPLY_STATUS_ERROR = 40006;

    /**
     * 申请不存在错误
     */
    long APPLY_NOT_EXIST_ERROR = 40006;

    /**
     * 申请审批消息通知配置错误
     */
    long APPLY_APPROVAL_NOTIFY_CONFIG_ERROR = 40007;

    /**
     * 提交申请失败
     */
    long SUBMIT_APPLY_ERROR = 40008;
}
