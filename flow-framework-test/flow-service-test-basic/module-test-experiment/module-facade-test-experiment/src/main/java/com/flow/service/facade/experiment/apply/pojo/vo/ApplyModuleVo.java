package com.flow.service.facade.experiment.apply.pojo.vo;

import com.flow.common.facade.experiment.apply.immutable.ApplyStatusImmutable;
import com.flow.common.facade.experiment.apply.immutable.ApplyTypeImmutable;
import com.flow.framework.core.pojo.vo.base.BaseBizModuleVo;
import com.flow.framework.core.validation.annotation.ImmutableCustomization;
import com.flow.service.facade.experiment.ExperimentErrorCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 申请
 * </p>
 *
 * @author luoguopiao
 * @version 0.0.1
 * @date 2023/4/30
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ApplyModuleVo extends BaseBizModuleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 申请no
     */
    private String applyNo;

    /**
     * 申请类型：01-电脑使用申请；02-电脑归还申请；03-电脑回收出售申请
     */
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_TYPE_ERROR, immutableInterface = ApplyTypeImmutable.class)
    private String applyType;

    /**
     * 申请状态：01-待审批；02-通过；03-拒绝
     */
    @ImmutableCustomization(errorCode = ExperimentErrorCode.APPLY_STATUS_ERROR, immutableInterface = ApplyStatusImmutable.class)
    private String applyStatus;

    /**
     * 申请资源的id
     */
    @ApiModelProperty("申请资源的id")
    private String resourceId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户姓名
     */
    private String userName;
}
